# Image Detect with Networking & Database 

The goal of this mini project is to gain an understanding and to implement the basics of a networked MySQL database server, as well as a web server to display stats scraped from the database. 
## Project Components

### Image Detection 
The excuse this project uses for "needing" a database is to collect and store image detection information that is collected on a raspberry pi. When a valid detection is made (e.g. human, dog, cat..) the detector will place a bounding a box around the detection and then record position of the detection, the type of detection, the detection lifetime, and date & time of the detection, as well as a raw image of the detection. This information will be written into a local SQLite database and then on some event trigger or schedule the image detection computer will trigger a call over the network to the MySQL database server and will push all local data to a more persistent database. This call will probably be to a REST API that will initiate communication over the network with the database server.

### HTTP Server 
HTTP is the the application layer that sits on top of TCP/IP and is used to make HTTP requests to a web server which in turn makes a HTTP reply to the client. HTTP has a handful of commands `POST`, `PUT`, `GET`, and `DELETE`. My current understanding of what HTTP is and how it works is that HTTP is specification for the type and format of the data that is being carried over the network via TCP/IP. This means that any HTTP client/server pair will be able to encode & decode (serialize & deserialize?) the messages contained in the HTTP message which is (to the best of my understanding) just a string of characters that TCP has delivered between two computers on a network. 

#### HTTP Message Serialization & Deserialization
My understanding of HTTP messages is that they are usually in the form of a string which is transmitted over the network as a stream of bytes. Once the bytes from one host have been transmitted to the other host. The next step is to reconstruct the byte stream into a single entity (e.g. file || string? ). I'm assuming this string and be used directly or that this string can deserialized into an programmatic object that the receiving host can use directly with code that is in the application layer. 

If I'm thinking about this correctly, this would allow would work like the following example. 

1. A program on the client side wants to use HTTP to make a request over the network to a server.
2. This program would serialize any data it wanted to transmit
3. Transmit this serialized HTTP message as a byte stream over TCP/IP
4. The server would reconstruct this byte stream via TCP
5. The reconstructed serialized message could then be deserialized.
6. A program on the server side can then take that deserialized object and perform normal operations on it. 
7. A result would be computed or obtained
8. The server would send a response message back to the client containing the HTTP status and any request information.  

### Asynchronous TCP/IP Socket Networking 
In order for multiple computers to communicate over the network they all need to implement a form of the HTTP/TPC/IP stack that will allow a client to make a request over the network to the server and for the server to process the request and reply over the network. 

#### Server-Side
The server side of this Networking layer needs to be able to able to asynchronously handle incoming TCP/IP connections and to establish messaging queues if connections are stacking. 

#### Client-Side
The client needs to be able to initiate the connection to the server on some predefined schedule or on an event trigger by invoking a REST API to the server.


### MySQL Database 
A MySQL database server is effectively a single process that runs the RDBMS SQL engine to interact with a database (either in RAM or disk). When desiring to add, change, or delete information from the database, an invocation to the MySQL-server is made specifying the database and information we want to alter. The RDBMS engine will then use the information that we have input to update the database. However, when interacting with a database from a programming language we do not have direct access to the `MySQL` command line utility. For this, we will use a library (in the programming language we are using) that allows us to programmatically invoke the RDBMS engine.


## Path Forward 

The path forward for this project involves the following steps... 

- [ ] Create a simple TCP/IP client that send requests to a specified TCP/IP endpoint
- [ ] Create simple TCP/IP server that can ingest client request, and respond appropriately. 
- [ ] Create standalone TCP class based implementations for a TCP server & client. 
- [ ] Write an HTTP server by wrapping the TCP namespace/classes with methods that enable the using of HTTP verbs (GET, POST, PUT, DELETE) and understands how to parse the results. 




/*
 * Simple TCP/IP Client Example
 */

#include "socket_headers.hpp"
#include <iostream> 

#define PORT 8080


int main()
{
    // Create Socket & return socket descriptor 
    // Socket Args: 1) Domain {Internet}, 2) Type {TCP}, 3) Protocol { Use default} 
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);   

    // Define Socket Address Struct
    struct sockaddr_in server_addr;

    // Popular Socket Struct
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    // server_addr.sin_addr.s_addr = inet_addr("142.250.217.142");    // Google Homepage
    server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");    // Google Homepage
    

    int connection_status = connect(socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr));

    if(connection_status < 0)
    {
        perror("Client Failed to 'connect' to server");
        return 1; 
    }
    else 
    {
        std::cout << "Client Connected to Server" << std::endl;
    }

    char message[25] = "GET / HTTP/1.1\r\n\r\n";
    int msg_len = sizeof(message);

    send(socket_fd, message, msg_len, 0);

    char rec_buf[4096];
    int n; 

    while((n = recv(socket_fd,rec_buf,sizeof(rec_buf), 0))>0)
    {
        std::cout<< rec_buf << std::endl;
    }

    if (n < 0) { return -1; }

    return 0; 
}


































// // Check Socket has been created correctly via file descriptor
// if(socket_fd == -1)
// {
//     std::cout << "Could Not Create Socket" << std::endl;
// }
// else
// {
//     std::cout << "Socket has been created successfully" << std::endl;
// }


// // Populate "socketadd_in" struct 
// server.sin_addr.s_addr = inet_addr("192.168.1.150");    // Set IP address in octet form 
// server.sin_family = AF_INET;                            // Tell socket to use Internet protocol
// server.sin_port = htons(80);                            // Use TCP port 80






// // Client attempts to make a connection to a server 
// int connection_status = connect(socket_fd, (struct sockaddr*)&server, sizeof(server));      // This connection is the clients attempt to establish a connection with an available server

// // Invokation to "connect" returns a status integer from the server 
// // Check server status and see if connection was successful 

// if(connection_status == -1 )
// {
//     std::cout << "Connection to server failed" << std::endl;
//     return 1; 
// }

// // ELSE - Connection was successful 

// // Define a response buffer that we can read in response to the server from. 
// char response_buf[256];

// // Now that connection has beeb










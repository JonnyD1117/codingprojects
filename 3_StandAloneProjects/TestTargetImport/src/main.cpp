#include <iostream>
#include "array.hpp"

int main()
{
	std::cout << "Main " << std::endl;

	DataStructures::Array<int, 10> array {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
	
	for(auto element : array)
	{
		std::cout << element << std::endl;
	}


	return 0; 
}


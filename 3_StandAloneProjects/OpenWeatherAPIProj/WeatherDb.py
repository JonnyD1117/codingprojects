import mariadb
import sys
import requests
from data.dummy_api_data import *


class OpenWeatherAPI:
    def __init__(self, default_locations=True,  api_key="83e0ee391dc1d9910e4f1417e5a48054"):
        self._location_dict = dict() 
        self._api_key = api_key
        self._results = list()

        self._test_api_reponse = fremont_json

        if default_locations:
            self._location_dict = self._default_locations()

    ###################################
    #       Private Methods 
    ###################################

    def _query_api(self, latitude, longitude ):
        api_request = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&units=metric&appid={self._api_key}"
        r = requests.get(url=api_request)
        return r.json()

    def _default_locations(self):
        locations = {
            "Fremont": {"LAT": 37.548, "LON": -121.98}, 
            "Tucson": {"LAT": 32.254, "LON": -110.97}, 
            "Salou": {"LAT": 41.077, "LON" : 1.13}, 
            "Lihue" : {"LAT": 21.97, "LON": -159.36},
            "Sydney" : {"LAT": -33.86, "LON": 151.20 }
            }
        return locations

    def _query2record(self, query):
        record = dict()

        # Yes I know that I am manually definining the record dict
        # Yes I know this is lazy
        # Deal with it.
        record["latitude"] = query["coord"]["lat"]
        record["longitude"] = query["coord"]["lon"]
        record["temperature"] = query["main"]["temp"]
        record["min_temp"] = query["main"]["temp_min"]
        record["max_temp"] = query["main"]["temp_max"]
        record["pressure"] = query["main"]["pressure"]
        record["humidity"] = query["main"]["humidity"]
        record["wind_speed"] = query["wind"]["speed"]
        record["wind_direction"] = query["wind"]["deg"]
        record["location_name"] = query["name"]
        record["timezone"] = query["timezone"]
        record["sunrise"] = query["sys"]["sunrise"]
        record["sunset"] = query["sys"]["sunset"]
        record["country"] = query["sys"]["country"]
        record["unix_time"] = query["dt"]
        record["visibility"] = query["visibility"]
    
        return record

    #####################################
    #       Public Methods
    #####################################

    def append_locations(self, name, latitude, longitude):
        self._location_dict[name] = {"LAT": latitude, "LON" : longitude}

    def aggregate_records(self, test_data_processing=False):

        if not test_data_processing:
            for key, val in self._location_dict.items():
                lat, lon = val["LAT"], val["LON"]
                response =self._query_api(latitude=lat, longitude=lon)
                self._results.append(self._query2record(response))
        else:
            self._results.append(self._query2record(self._test_api_reponse))
        return self._results


class WeatherDb:

    def __init__(self, password="boobs", database="my_weather", user="root", host="localhost"):
        self._connection = None 
        self._cursor = None 
        self._database_name = database
        self._user = user
        self._host = host
        self._password=password

        self._incoming_records = None 

    def __enter__(self):
        self._connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self._close()

    ##########################
    #   Private Methods
    ##########################
    def _query_weather_api(self, test_data=False):
        open_weather = OpenWeatherAPI()
        self._incoming_records = open_weather.aggregate_records(test_data_processing=test_data)
        return self._incoming_records

    def _connect(self):
        try:
            self._connection = mariadb.connect(
                    user=self._user,
                    password=self._password,
                    host=self._host,
                    database=self._database_name
                    )

        except mariadb.Error as e:
            print(e)
            sys.exit(1)

        self._cursor = self._connection.cursor()

    def _close(self):
        self._connection.close()

    def populate_db(self):

        records = self._query_weather_api()

        for record in records:
            cols = "("
            vals = "("

            len_dict = len(record.keys())

            for i, (key, value) in enumerate(record.items()):
            
                cols += str(key)
                
                if type(value) is str:
                    vals += f"'{value}'"
                else:
                    vals += str(value)


                if i < 15:
                    cols += ", "
                    vals += ", "
            cols += ")"
            vals += ")"
            query = f"INSERT INTO hourly_weather {cols} VALUES {vals}"
            self._cursor.execute(query)
        self._connection.commit()

    ##########################
    #   Public Methods
    ##########################
    
    def print(self, db_query="DESCRIBE hourly_weather"):
        self._cursor.execute(db_query)
        
        results = self._cursor.fetchall()
        for row in results:
            print(row)


def write_to_db():
    with WeatherDb() as db:
        db.populate_db()
        #db.print("SELECT * FROM hourly_weather")

if __name__ == '__main__':
    write_to_db()


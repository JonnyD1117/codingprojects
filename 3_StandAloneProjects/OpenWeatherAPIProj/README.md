# Open Weather API & MariaDB Project 

The goal of this project was to build a toy MariaDB database from scratch by populating the database with weather queries to the OpenWeatherAPI approximately once every hour. 

The stretch goal of this project would be to create a Dash board (e.g. Dash App) that could display the current and timeseries data contained within the Weather DB this application has set up on a locally hosted server. 

## Mariadb Python Connector 
This application uses the Mariadb Python connector to allow DB queries and entries to be made directly from Python. 

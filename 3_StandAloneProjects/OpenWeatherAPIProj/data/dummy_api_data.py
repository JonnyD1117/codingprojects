fremont_json = {
    "coord": {"lon": -121.98, "lat": 37.548},
    "weather": [
        {"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}
    ],
    "base": "stations",
    "main": {
        "temp": 17.3,
        "feels_like": 16.42,
        "temp_min": 13.9,
        "temp_max": 20.28,
        "pressure": 1019,
        "humidity": 51,
    },
    "visibility": 10000,
    "wind": {"speed": 2.06, "deg": 280},
    "clouds": {"all": 0},
    "dt": 1702679238,
    "sys": {
        "type": 2,
        "id": 2075054,
        "country": "US",
        "sunrise": 1702653329,
        "sunset": 1702687842,
    },
    "timezone": -28800,
    "id": 5350734,
    "name": "Fremont",
    "cod": 200,
}
tucson_json = {
    "coord": {"lon": -110.97, "lat": 32.254},
    "weather": [
        {"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}
    ],
    "base": "stations",
    "main": {
        "temp": 22.7,
        "feels_like": 21.39,
        "temp_min": 21.18,
        "temp_max": 24,
        "pressure": 1020,
        "humidity": 14,
    },
    "visibility": 10000,
    "wind": {"speed": 5.66, "deg": 110, "gust": 9.26},
    "clouds": {"all": 0},
    "dt": 1702679238,
    "sys": {
        "type": 2,
        "id": 2008134,
        "country": "US",
        "sunrise": 1702649855,
        "sunset": 1702686029,
    },
    "timezone": -25200,
    "id": 5294937,
    "name": "Flowing Wells",
    "cod": 200,
}
spain_json = {
    "coord": {"lon": 1.13, "lat": 41.077},
    "weather": [
        {"id": 800, "main": "Clear", "description": "clear sky", "icon": "01n"}
    ],
    "base": "stations",
    "main": {
        "temp": 7.45,
        "feels_like": 6.69,
        "temp_min": 7.42,
        "temp_max": 12.04,
        "pressure": 1033,
        "humidity": 76,
    },
    "visibility": 10000,
    "wind": {"speed": 1.54, "deg": 40},
    "clouds": {"all": 0},
    "dt": 1702679011,
    "sys": {
        "type": 1,
        "id": 6427,
        "country": "ES",
        "sunrise": 1702624399,
        "sunset": 1702657660,
    },
    "timezone": 3600,
    "id": 3110986,
    "name": "Salou",
    "cod": 200,
}
kauaii_json = {
    "coord": {"lon": -159.36, "lat": 21.97},
    "weather": [
        {"id": 500, "main": "Rain", "description": "light rain", "icon": "10d"}
    ],
    "base": "stations",
    "main": {
        "temp": 23.62,
        "feels_like": 23.55,
        "temp_min": 23.15,
        "temp_max": 24.69,
        "pressure": 1020,
        "humidity": 58,
    },
    "visibility": 10000,
    "wind": {"speed": 8.23, "deg": 50},
    "rain": {"1h": 0.25},
    "clouds": {"all": 0},
    "dt": 1702678983,
    "sys": {
        "type": 1,
        "id": 7873,
        "country": "US",
        "sunrise": 1702660121,
        "sunset": 1702698999,
    },
    "timezone": -36000,
    "id": 5850248,
    "name": "Lihue",
    "cod": 200,
}
sydney_au_json = {
    "coord": {"lon": 151.2, "lat": -33.86},
    "weather": [
        {"id": 800, "main": "Clear", "description": "clear sky", "icon": "01d"}
    ],
    "base": "stations",
    "main": {
        "temp": 26.75,
        "feels_like": 27.77,
        "temp_min": 25.11,
        "temp_max": 29.48,
        "pressure": 1005,
        "humidity": 60,
    },
    "visibility": 10000,
    "wind": {"speed": 3.6, "deg": 280},
    "clouds": {"all": 0},
    "dt": 1702678985,
    "sys": {
        "type": 2,
        "id": 2018875,
        "country": "AU",
        "sunrise": 1702665528,
        "sunset": 1702717330,
    },
    "timezone": 39600,
    "id": 2147714,
    "name": "Sydney",
    "cod": 200,
}

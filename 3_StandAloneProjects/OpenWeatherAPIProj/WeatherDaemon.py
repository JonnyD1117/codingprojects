from apscheduler.schedulers.blocking import BlockingScheduler
from WeatherDb import write_to_db



if __name__ == "__main__":

    scheduler = BlockingScheduler()
    scheduler.add_job(write_to_db, 'cron', hour='*', minute=0)

    try:
        # Start the scheduler
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        # Stop the scheduler on keyboard interrupt or system exit
        scheduler.shutdown()

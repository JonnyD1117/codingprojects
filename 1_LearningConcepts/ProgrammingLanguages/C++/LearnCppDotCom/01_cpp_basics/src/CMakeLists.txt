
add_executable(test_main main.cpp)

target_link_libraries(
    test_main 
    GTest::GTest 
    )

gtest_discover_tests(test_main)

set_target_properties(test_main PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${BIN_DIR})


#include <iostream>
#include <fstream>
#include <string> 

class DualBuffer : public std::streambuf
{
    public:
    DualBuffer(std::streambuf* buf1, std::streambuf * buf2) : buf1_(buf1) , buf2_(buf2) { }

    int overflow(int c) override
    {
        if (c != EOF) {
            if (buf1_) buf1_->sputc(c);  // Write to the original streambuf
            if (buf2_) buf2_->sputc(c);  // Write to the log streambuf
        }
        return c;
    }

    private:

    std::streambuf* buf1_;
    std::streambuf* buf2_; 
};


int main()
{   
    std::ofstream logFile("output.log");                            // Get the Ostream for log file 

    DualBuffer dualBuffer(std::cout.rdbuf(), logFile.rdbuf());      // Create the DUAL streambuf class that inherits from std::streambuf

    // std::ostream dualStream(&dualBuffer);                           // Create an Ostream using the DUAL streambuf

    // std::cout.rdbuf(dualStream.rdbuf());                            // Instuct std::cout to use dualStreams streambuf

    std::cout.rdbuf(&dualBuffer);

    std::cout << "Hello DualBuffering" << std::endl; 
    std::cout << "Directly" << std::endl; 
    std::cout << "Using Pointer of Custom std::streambuf" << std::endl; 
    return 0; 
}
use std::io;

fn main() {
    // Statements here are executed when the compiled binary is called.

    let mut input = String::new();

    let _val : i64 = 20;

    io::stdin().read_line(&mut input).expect("failed to read line");
    println!("{}", input);
}
fn main() {
    // Statements here are executed when the compiled binary is called.

    let x = 4.5;

    // Print text to the console.
    println!("Hello World! ==== {}", x);
}
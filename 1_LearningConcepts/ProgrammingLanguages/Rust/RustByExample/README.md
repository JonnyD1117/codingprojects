# Intro to Rust Programming Languages

My personal notes on the rust programming language.

## Basic Compilation 

Rust is a compiled language that requires running the compiler to generate an executable. 

### Manual Compilation 

```rust

user@host: ~/path/to/rust/program/$ rustc my_first_rust_file.rs 
```

This will manually compile the rust program and dump the exectuable in the same directory 

### Compilation using CARGO 
Inside of your directory where you would like to place your rust package... 

``` cpp
// Will create the new project in the current directory
cargo new <project-name>

// Will Compile your rust file 
cargo build 

// Will check that your rust file is compilable without errors 
//(NOTE: Not correct just compiles correctly)
cargo check

// Will build & run your rust executable 
cargo run
```


## Rust Basics 

### Main Entrypoint 

```rust
fn main()
{
    println!("Hello World");
}
```


cmake_minimum_required(VERSION 3.2)

project(fortran_from_cmake)

enable_language(Fortran)

add_executable(my_fortran_exe main.f90)

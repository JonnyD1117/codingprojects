# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.22

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Disable VCS-based implicit rules.
% : %,v

# Disable VCS-based implicit rules.
% : RCS/%

# Disable VCS-based implicit rules.
% : RCS/%,v

# Disable VCS-based implicit rules.
% : SCCS/s.%

# Disable VCS-based implicit rules.
% : s.%

.SUFFIXES: .hpux_make_needs_suffix_list

# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/build

# Include any dependencies generated for this target.
include CMakeFiles/my_fortran_exe.dir/depend.make
# Include any dependencies generated by the compiler for this target.
include CMakeFiles/my_fortran_exe.dir/compiler_depend.make

# Include the progress variables for this target.
include CMakeFiles/my_fortran_exe.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/my_fortran_exe.dir/flags.make

CMakeFiles/my_fortran_exe.dir/main.f90.o: CMakeFiles/my_fortran_exe.dir/flags.make
CMakeFiles/my_fortran_exe.dir/main.f90.o: ../main.f90
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building Fortran object CMakeFiles/my_fortran_exe.dir/main.f90.o"
	/usr/bin/gfortran $(Fortran_DEFINES) $(Fortran_INCLUDES) $(Fortran_FLAGS) -c /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/main.f90 -o CMakeFiles/my_fortran_exe.dir/main.f90.o

CMakeFiles/my_fortran_exe.dir/main.f90.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing Fortran source to CMakeFiles/my_fortran_exe.dir/main.f90.i"
	/usr/bin/gfortran $(Fortran_DEFINES) $(Fortran_INCLUDES) $(Fortran_FLAGS) -E /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/main.f90 > CMakeFiles/my_fortran_exe.dir/main.f90.i

CMakeFiles/my_fortran_exe.dir/main.f90.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling Fortran source to assembly CMakeFiles/my_fortran_exe.dir/main.f90.s"
	/usr/bin/gfortran $(Fortran_DEFINES) $(Fortran_INCLUDES) $(Fortran_FLAGS) -S /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/main.f90 -o CMakeFiles/my_fortran_exe.dir/main.f90.s

# Object files for target my_fortran_exe
my_fortran_exe_OBJECTS = \
"CMakeFiles/my_fortran_exe.dir/main.f90.o"

# External object files for target my_fortran_exe
my_fortran_exe_EXTERNAL_OBJECTS =

my_fortran_exe: CMakeFiles/my_fortran_exe.dir/main.f90.o
my_fortran_exe: CMakeFiles/my_fortran_exe.dir/build.make
my_fortran_exe: CMakeFiles/my_fortran_exe.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking Fortran executable my_fortran_exe"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/my_fortran_exe.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/my_fortran_exe.dir/build: my_fortran_exe
.PHONY : CMakeFiles/my_fortran_exe.dir/build

CMakeFiles/my_fortran_exe.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/my_fortran_exe.dir/cmake_clean.cmake
.PHONY : CMakeFiles/my_fortran_exe.dir/clean

CMakeFiles/my_fortran_exe.dir/depend:
	cd /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/build /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/build /home/indy/repos/CodingProjects/CourseWork/Fortran/1_cmake_compile/build/CMakeFiles/my_fortran_exe.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/my_fortran_exe.dir/depend


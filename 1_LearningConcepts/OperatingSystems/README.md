# Computer Operating Systems 

The object of projects in this directory are intended to learn about, explore, and even implement concepts in computer operating systems. While it is not too challenging to learn about operating system by reading a book it is often extremely difficult to implement any of the concepts and constructs presented in operating systems books from scratch. 



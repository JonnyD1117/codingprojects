;
;   Test Random Assembly Code in NASM
;

[org 0x7c00]
    mov bx, HELLO_MSG           ;
    call print_string
    jmp $

; Data 
HELLO_MSG:
    db 'Hello,World!', 0

GOODBYE_MSG:
    db 'Goodbye!', 0

%include "asm/print_string.asm"

times 510-($-$$) db 0
dw 0xaa55
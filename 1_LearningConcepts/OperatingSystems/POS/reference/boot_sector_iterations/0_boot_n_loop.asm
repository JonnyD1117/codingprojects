;
;   Simple boo sector program that loops forever
;

loop:                   ; Define a label, "loop" that will allow us to jump 
                        ; back to it, forver.
    
    jmp loop            ; Use a simple CPU instrction that jummps to a 
                        ; new memory address to continue executation 
                        ; In this case, jump to the address of the current
                        ; instruction. 

times 510-($-$$) db 0   ; Pad 510 bytes with zeros

dw 0xaa55               ; Write magic number to last 2 bytes of boot sector

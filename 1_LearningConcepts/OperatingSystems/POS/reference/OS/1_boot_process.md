# The Boot Process 

## BIOS - (Basic Input/Output Software)

Once the computer has been powered on, the BIOS is a collection of software routines are automatically placed into system memory, via hardware. The goal of this project is **NOT** to write my own BIOS. The BIOS is a tool provides limited (but essential) functionality for the computer to be able to start up everything required to run the operating system. 

It should be remembered that the BIOS is a VERY limited environment. It does not have a concept of files (e.g. a file system), processes, or any other constructs that we normally think about a bare bones computer having once it boots. This means that in order for the BIOS to be useful we need to directly manipulate the items we are interested in. 

In particular, the item that we are interested in is to find and read in a `boot device` that will begin the boot strapping process of starting the operating system software. 


## Boot Device 

The boot device is the media that encodes the startup process. The problem at this point is that that there might be several different types of media attached to the computer once it's powered up. 

### Boot Sector 
The boot sector is the first section (on any media/disk...etc) that the BIOS can read and identify as a `bootable` device, typically the first 512 bytes. There is no garuntee that *any* of the devices/media plugged into the computer at boot time are "valid" operating systems bootloaders, they could just be normal code or raw data. This presents the BIOS with the problem of figuring out which media and devices are suitable for attempting to boot from.  

This means that there needs to be some mechanism for the BIOS to reason about boot sector of every available device to determine whether or not it is `bootable`. This is accomplished with the use of a magic number. 

#### Magic Number

The magic number is a special number placed into the last two bytes from that 512 bytes that define the boot sector of the media which the BIOS is reading. 
These last two bytes are `0xaa55`. 

While unsophisticated, the use of the magic number means that the BIOS merely loops through all available device boot sectors and checks to see if the last two bytes of the boot sector are equal to the magic number. This lets the BIOS know that that current device is a bootable device that contains the software for the operating system.

Once the magic number has been confirmed, the BIOS will place the entire 512 bytes of the boot sector into the computers memory and instructs the computer to start executing the data in the now valid boot sector. 

#### Boot Sector Program 

The first three bytes of the 512 byte boot sector are `0xe9`, `0xfd`, and `0xff` while the last two bytes of the boot sector are `0x55`and `0xaa`. 

 - The first 3 bytes are instructions for an endless jump 
 - The last 2 bytes are the magic BIOS number 
 - The remaining bytes are padded zeros `0x00`

This is the most basic (and useless program that a computer can run)

#### Endian-ness 
It is important to know the Endian-ness of the computer you are using to understand how to write the data in a way that the computer will understand how to read it. 

On linux check using... 
```
lscpu | grep Endian
```


### Writing the Boot Sector in Assembly 

#### NASM 

To assembly the following code I will be using the `nasm` assembler 

```
nasm boot_sect.asm -f bin -o boot_sect.bin
```






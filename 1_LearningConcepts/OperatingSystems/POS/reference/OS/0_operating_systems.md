# Operating Systems

An operating system is a piece of software that abstracts the details of interacting with hardware, scheduling jobs, ...etc and presents users and developers with a simplified API that allows application level software to be agnostic about the complex details of hardware drivers or file systems or IO or Networking...etc. 

## What Starts the OS? 

The problem of starting a computer is a boot strapping problem that requires an iterative process of simple tasks enabling more complex tasks until the full operating system has been enabled. 

It is an important question then to ask, if the operating system is software that we use to run "our" software, how does the operating system software get run? 

For the case of general computers (e.g. not application specific embedded microcontrollers), the answer to this question is that a `bootloader` of some sort is started once electrical power has been turned on to the computer. 

### Bootloader 
The job of a bootloader is to be the first bit of a code run directly on the computer after its been powered up, such that the operating system software can be started. 

The job of the bootloader is VERY hardware dependent as the type and architecture of the computer being used will change the startup process that that machine must complete before booting into the operating system. 

## Setup & Environment
For this project, I will be using `AMD64` computer architecture commonly known as `x86_64`. Also instead of attempting to use an actual computer for this, I am going to use the `QEMU` cpu emulator. 
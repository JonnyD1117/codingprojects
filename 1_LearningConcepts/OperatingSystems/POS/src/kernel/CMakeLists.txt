
# Set Kernel Sources
set(kernel_sources kernel.c)

# Create kernel object file
add_library(kernel_target OBJECT ${kernel_sources})

set_target_properties(kernel_target PROPERTIES OUTPUT_NAME "kernel.o")




# Set Compiler Options for Kernel
target_compile_options(kernel_target PRIVATE "-ffreestanding")

# # Set Linker Flags for outputing kernel binary in correct format
# set_target_properties(kernel_target PROPERTIES 
#     LINK_FLAGS "-Ttext 0x1000 --oformat binary"
# )


set_target_properties(kernel_target PROPERTIES 
    RUNTIME_OUTPUT_DIRECTORY ${PROJ_BIN_DIR}
)


# Add a custom command to rename the object file to kernel.o
# Create a custom target to generate kernel.bin from kernel.o
add_custom_command(
    OUTPUT ${PROJ_BIN_DIR}/kernel.bin
    COMMAND ${CMAKE_LINKER} -o ${PROJ_BIN_DIR}/kernel.bin -Ttext 0x1000 $<TARGET_OBJECTS:kernel_target> --oformat binary
    DEPENDS kernel_target 
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
    COMMENT "Linking kernel_target to generate kernel.bin"
)

add_custom_target(generate_kernel_bin ALL DEPENDS ${PROJ_BIN_DIR}/kernel.bin)
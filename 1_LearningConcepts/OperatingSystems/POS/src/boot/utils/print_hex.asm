;
;   Print Hex Value (16bits max) as string to output
;   
;   NOTE: hex value should be loaded in `dx` before invoking function 
;
HEX_OUT: db "0x0000",0
len: equ $ - HEX_OUT

print_hex:
    mov bx, HEX_OUT 
    mov esi, 5
    jmp convert_loop            ; Loop over hex val to convert to str

convert_loop:
    mov cx, dx                  ; Store Current value of DX into CX
    and dx, 0xf                 ; Bitwise AND hex with mask
    add dx, '0'                 ; Convert numbers to letters in ASCII
    cmp dx, '9'                 ; Check to see if value is <9 
    jbe store_digit             ; IF true store value 
    add dx, 39                  ; ELSE offset to get a-f value
    jmp store_digit
    
store_digit:
    mov [ebx + esi], dl         ; Move masked digit into address contained in BX + offset esi
    mov dx, cx                  ; DX has been manipulated out the ass, reset its value from CX
    shr dx, 4                   ; Once DX has been reset bit shift to the rigth 4 bits
    dec esi                     ; De
    cmp esi, 1
    je end
    jmp convert_loop
end:
    mov bx, HEX_OUT
    call print_string
    ret

;
; Print String Function in assembly 
;
; NOTE: Assumes string label (reference?) is place in register bx
; 

print_string:   
 
    mov al, [bx]        ; Load char into AL register
    cmp al, 0           ; Determine if AL is 'null' terminator
    je end_loop         ; if 'null' terminator jump to end of function

    mov ah, 0x0e       ; Print via BIOS interupts
    int 0x10 

    inc bx              ; Increase BX register (go to next char) 
    jmp print_string    ; Loop unconditionally

end_loop:               ;
    ret                 ; Return function when 'null' terminator is encountered

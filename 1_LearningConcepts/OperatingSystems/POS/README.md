# POS - Operating System from Scratch

The goal of this project is to learn about the design and implementation of operating systems by writing a VERY basic one that can be run on a QEMU x86 emulated CPU. The operating system being built is intended to be as much of a Unix/Linux Clone as is possible without looking at source code. 

## Systems Language
One thing that I have always been interested in is the effect and design philosophies behind using C vs C++ as the systems language for operating systems. Since I find myself building small operating system, I would like to put this to the test by compiling two different version of the operating system. The first using only standard C while the second uses C++ and leans into OOP techniques and other C++ libraries.

## Current State:

I **did** have the OS running from the kernel; however, I am attempting to automate the build process completely using CMake which is getting a bit hairly when trying to assemble and build the operating system image using Cmake. Once this is accomplished the project will be ready for serious development.

After that the primary goals are kernel initialization 

1. Basic Memory Management 
2. Basic Process Management 
3. Minimal Device Drivers 
4. Function File Systems 
5. Basic SHELL & commands for interacting with the kernel (e.g ps, cd, mkdir, rm, ls, which ...etc)

Once these tasks are completed I can focus on creating something akin to `systemd` for managing user-land & background processes and understands how dependencies work. The final goal for this project is to actually be able to run compile and run a non-trivial project from within the operating system I made actually running on real hardware.

## Objectives 

- [x] Boot Strap the startup from the BIOS to the kernel (from assembly into kernel.c)
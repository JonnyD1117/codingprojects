section .data
    msg: db "Hello World", 10
    msglen: equ $ - msg
    HEX_IN: dd 0x1fb6
    HEX_OUT: db "0x0000",10
    str_len: equ $ - HEX_OUT

section .text
  global _start

_start:
mov dx, [HEX_IN]
mov ebx, HEX_OUT 
mov esi, 5

jmp convert_loop            ; Loop over hex val to convert to str

convert_loop:
    mov cx, dx
    and dx, 0xf                 ; Bitwise AND hex with mask
    add dx, '0'                 ; Convert numbers to letters in ASCII
    cmp dx, '9'                 ; Check to see if value is <9 
    jbe store_digit             ; IF true store value 
    add dx, 39                  ; ELSE offset to get a-f value
    jmp store_digit
    
store_digit:

    ; Move digit into address contained in BX
    mov [ebx + esi], dl
    mov dx, cx
    shr dx, 4
    dec esi

    cmp esi, 1
    je print
    
    jmp convert_loop


print:
  mov rax, 1        ; write(
  mov rdi, 1        ;   STDOUT_FILENO,
  mov rsi, HEX_OUT      ;   "Hello, world!\n",
  mov rdx, str_len   ;   sizeof("Hello, world!\n")
  syscall           ; );

  mov rax, 60       ; exit(
  mov rdi, 0        ;   EXIT_SUCCESS
  syscall           ; );

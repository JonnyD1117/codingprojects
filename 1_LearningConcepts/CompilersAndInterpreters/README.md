# Compiler & Interpreter Design
The mini-project contained in this directory are targeted at understanding the basics of how compilers and interpreters work and implementing projects that utilize some or all of the techniques common to these tasks. 

## Projects 
- [ ] JsonParser Libary 
- [ ] Simple C Compiler
- [ ] 6502 Simple C Compiler

# Simple C Compiler 

The goal of the simple C compiler is to compile a text file with basic C syntax ( functions, arrays, typed vars, include statements...etc) into 6502 assembly, with the long term goal of this project to be able to execute a C program in my 6502 CPU emulator which is a WIP. 

## 1. Lexer 
The goal of the lexer is to read in the text file and decompose into tokens that contain a symbol and an optional value. 


Input (Text file with .c/.cpp extension) -> list of tokens.

### 
 - [ ] Define a set of **legal** symbols (terminal & non-terminal) 
 - [ ] Read in a file char by char 
 - [ ] For each character in the 



___
#### Terminal Symbols 
1 
2
3
4
5
6
7
8
9
0
* 
** 
&
|
&& 
||
#
% 
(
)
{
}
+
- 
++
--
=
==
<=
>=
+=
-=
&=
|=
*=
/=
/*
*/
;
"
! 
!=

\n
\t
\r 

for
if 
else
while 
do 




WHITESPACE 
IDENTIFIER
INTEGER_LITERAL
STRING_LITERAL
OPERATOR
STRING_ESCAPE_SEQUENCE
COMMENT
____



#### Non-terminals 


#### Production Rules
<digit> ::= 0|1|2|3|4|5|6|7|8|9
<integer> ::= [-]<digit>| {<digit>}
<float> ::= [-]<integer>.<integer>

## 2. Parsing 

## 3. 6502 Code Generation 


## Features to Support 

1. Single Line Comments [// This is a comment]
2. Simple types (byte, int, float, char, double, long?, short?)
3. Structs 
4. arrays 
5. pointers (void pointers) [*, **, &var]
6. functions 
7. Maybe TYPEDEFS
8. Simple math ( -,+,*, /, <, >, <=, >= )
9. Bitwise math (&, |, &=, |=, ^=)
10. logical operators (||, &&)
11. by 
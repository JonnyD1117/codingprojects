# Json Parser Libary
The basic idea of a JSON libary is the need to deserialize raw JSON text into a structure that is directly able to interact with code at a high level. The reverse operation of serialization to take the Json code object and convert it into a raw string output is also required. 

The primary mechanisms behind these serialize/deserialize operations is the ability to `tokenize` a file stream of characters into a series of tokens which can get `parsed` into an intermediate representation from which the final coding artifact can be created and populated.

These structure is 100% identical to the implementation of a compiler compiling text files into machine code. The primary difference between GCC and a JsonParser is the reality that creating a Json object from text is much higher level task than GCC's need to convert C/C++ files into the raw machine code. 

## Objective 
The goal of this project is to provide a complete self-contained Json library in C++ that is capable of serialization & deserialization while providing a high-level API for interacting with Json Objects.  

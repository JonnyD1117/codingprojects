#include <gtest/gtest.h>
#include "binary_search_tree.hpp"


using namespace DataStructures;


TEST(TestBinarySearchTreeBasic, BstInsertNode)
{
    BinarySearchTree<int> bst; 

    bst.insert_node(22);
    bst.insert_node(15);
    bst.insert_node(13);
    bst.insert_node(5);
    bst.insert_node(1);
    bst.insert_node(47);
    // bst.insert_node(20);
    // bst.insert_node(10);
    // bst.insert_node(46);
    // bst.insert_node(38);

    bst.print_tree();

    bst.delete_node(5);
    
    bst.print_tree();

}

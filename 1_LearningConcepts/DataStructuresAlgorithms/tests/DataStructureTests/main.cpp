#include <iostream> 
#include "binary_search_tree.hpp"
#include "linked_list.hpp"

using namespace DataStructures;

int main()
{
    // LinkedList<int> my_array {22,15,13,5,1, 47,20,10,46,38,55,50,60} ;

    // std::cout << "Printed From my Array" << std::endl;
    // BinarySearchTree<int> bst(my_array.begin(), my_array.end() );


    BinarySearchTree<int> bst {22,15,13,5,1, 47,20,10,46,38,55,50,60}; 
    // bst.print_tree();

    // bst.insert_node(22);
    // bst.insert_node(15);
    // bst.insert_node(13);
    // bst.insert_node(5);
    // bst.insert_node(1);
    // bst.insert_node(47);
    // bst.insert_node(20);
    // bst.insert_node(10);
    // bst.insert_node(46);
    // bst.insert_node(38);
    // bst.insert_node(55);
    // bst.insert_node(50);
    // bst.insert_node(60);
    
    std::cout << "##############################################" << std::endl;
    bst.print_tree(1);
    std::cout << "##############################################" << std::endl;
    // // bst.delete_node(20);
    // // bst.delete_node(1);
    // // bst.delete_node(15);
    // bst.delete_node(55);
    // bst.print_tree(0);
    // std::cout << "##############################################" << std::endl;
  
    return 0; 
}
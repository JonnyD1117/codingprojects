# Data Structures & Algorithms
This project is my own personal implemention of C++ STL data structures & algorithms. This is for academic purposes only and NOT for production use. 

## Objective 
The objective of this project is to implement Data Structures & Algorithms that conform to the C++ STL API as close as possible. The implementation details will obviously be different and are intended as a learning exercise to make test my understanding of not only DS & A but also Modern C++ constructs. It provides a very well defined mini-project with quantifiable deliverables. (e.g. I should be able to swap out STL data structures for my own without noticing any appreciable difference.) 

Additionally, another important sub-goal that I have set is to try and fully test each implementation directly with a test suite designed to verify that each feature of the data structure conforms to the expected behavior as defined in the [C++ STL Reference](https://cplusplus.com/). These test are created via GoogleTest framework and while I do not consider them "required" to check a data structure as completed, implementing tests provides confidence as to the expected behavior under normal inputs one could expect to be given.

### Statement of Quality
None of these implementations are going to be as clean or polished as the STL. (That is why we use the STL). The implementation for each Data structure are in varying states of completion or undergoing different types of refactors as time goes by; however, by and large, if an implementation is checked as completed in the following section. I have deemed it "useable" (NOT perfect).

## Data Structures Implemented
- [x] Vector 
- [x] Array
- [x] Linked-List
- [ ] Ordered Map
    - [ ] Red/Black Tree
    - [ ] AVL Tree
        - [ ] Binary-Search-Tree
- [ ] Deque
- [ ] Stack 
- [ ] Queue 
    - [ ] Normal Queue 
    - [ ] Priority Queue
- [ ] Forward_list 
- [ ] Unordered Map (hash table)
- [ ] Set

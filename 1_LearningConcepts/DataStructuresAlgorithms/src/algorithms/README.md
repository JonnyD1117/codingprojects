# Algorithms

## Algorithm Progress 

## Sorting 
- [ ] Merge Sort
- [ ] Quick Sort
- [ ] Heap Sort
- [ ] Insertion Sort
- [ ] Bucket Sort
- [ ] Radix Sort


## Graph 
- [ ]  Shortest Path
  - [ ]  Dijkstra's
  - [ ]  A* 
  - [ ]  D*-Lite
- [ ]  Minimal Spanning Tree
- [ ]  Network Flow
- [ ]  Breadth Fist Search
- [ ]  Depth First Search
- [ ]  Topological Sort


## Dynamic Programming 

## Divide & Conquer 

## Back Tracking

## Greedy

## Randomized
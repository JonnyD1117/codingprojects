# Fundamental Computer Science Concepts 
This directory is home to the coding projects and concepts that I feel are fundamental to understand and implement at least once in the life of a CS/Software engineer. 

The reason this directory exists is to provide a single version controlled location where I can explore CS concepts and try my hand at implementing useful techniques and code. 

## Topics 
- Data Structures & Algorithms 
- Compiler & Intepreter Design 
- Regex & Finite State Machines ...etc
- Linear Algebra Library(ies) in C++ 
- Machine Learning 
- Computer Networking 
- Operating System Design & Implementation 
- Misc. Computing Language Practice
 

## Project Structures
Each mini-project in this directory hierarchy are designed to be standalone with the exception of possible external libraries required to implement that specific functionality. This is intentional. Keep the structure of each project as as uniform as possible and attempt to encourage code/lib reuse by using Cmake exported targets and header only C/C++ libs generated from the source contained in these projects

These targets or libraries enable other projects I have to neatly import the functionality and link to the appropriate cmake targets without having to retool a project to include specific functionality, avoids duplicating sources into other projects, and allows me to rapidly develop and improve the libraries that I am exporting in the event that my larger project catches an error in a dependency that I wrote and imported.  
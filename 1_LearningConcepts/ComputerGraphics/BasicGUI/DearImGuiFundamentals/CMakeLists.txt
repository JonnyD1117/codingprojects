cmake_minimum_required(VERSION 3.22)

project(DearImGui)


set(PROJ_ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR})
set(PROJ_BIN_DIR ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(PROJ_BUILD_DIR ${CMAKE_CURRENT_SOURCE_DIR}/build)
set(PROJ_EXTERNAL_DIR ${CMAKE_CURRENT_SOURCE_DIR}/external)


add_subdirectory(src)
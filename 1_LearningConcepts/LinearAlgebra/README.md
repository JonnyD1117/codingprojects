# Linear Algebra

As an engineer, the ability to use Linear Algrebra to solve problems is incredibly powerful in the world of linear systems. However, my whole life has been spent using MatLab as a crutch for understanding more common programming languages like C++ actually implement Matrix operations. 

Since in C++, matrices are not a fundamental data type, the complexity of linear algebra in languages like C/C++ lies in understanding and being comfortable with the ability to either write your own matrix abstraction from the fundamental data types C/C++ provides, or the use of external third party Matrix libraries (like Eigen) that have already done the hard work for you. 

## Objective
Projects in this directly are intended to be learning exercises in learning how to build a custom matrix library from scratch and how how to get comfortable with the abstraction and API that the Eigen Matrix Library has already built.
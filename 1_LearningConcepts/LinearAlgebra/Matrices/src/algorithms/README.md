# Linear Algebra Matrix Algorithms

## Objective 
The objective of this library is to integrate with the custom `Matrix` data type created in this project and to be able to take that matrix type and to apply a series of commonly use algorithms and methods to a matrix. 

## Algorithms  
- [ ] Gause-Jordan Elimination 
- [ ] Matrix Inversion & Psuedo-Inverse
- [ ] Singular Value Decomposition 
- [ ] QR Factorization 
- [ ] LU Factorization 
- [ ] Cholesky Factorization
- [ ] Partitioned Matrices 
- [ ] Eigenvalue Decomposition  
- [ ] Solving Linear Systems 
- [ ] Least Squares Optimization


# Linear Algrebra From Scratch

## Matrix Type 
In languages like MATLAB, the matrix is an important data structure for solving linear algebra problems. In languages like C++ where closest native data structure to a matrix is an array of arrays, building a matrix class provides its own challenges in designing an appealing API and fast and responsive implementation. 

## Objective
The objective of this project is to take implement my own matrix class and use that as the foundation behind a "simple" linear algrebra library. The purpose of this project is merely to understand the complexity behind existing implementation in other languages (e.g. MATLAB & Python's numpy) or existing libraries in C++ like Eigen. 

The second reason for wanting to implement this from scratch is to learn how to effectively implement super common Linear Algrebra algorithms. Many of these algorithms are iterative and utilize techniques like "vectorization" or "multi-threading" to achieve better performance. It seems like an appropriate way of learning about very well understood algorithms and their implementations in a low-ish level computing language. 


import torch
import torch.nn as nn
import torchvision.models as models


class BasicBlock(nn.Module):

    def __init__(self, in_chan=64, out_chan=64, down_sample=False):
        super().__init__()

        self.in_chan = in_chan
        self.out_chan = out_chan

        self.conv1 = nn.Conv2d(in_chan, out_chan, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.bn1 = nn.BatchNorm2d(out_chan, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(out_chan, out_chan, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1), bias=False)
        self.bn2 = nn.BatchNorm2d(out_chan, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)

        self.down_sample_flag = down_sample

    def forward(self, x):

        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.conv2(x)
        x_samp = self.bn2(x)

        if self.down_sample_flag is True:
            self.downsample = nn.Sequential()
            self.downsample.add_module('0', nn.Conv2d(self.in_chan, self.out_chan, kernel_size=(1, 1), stride=(2, 2), bias=False))
            self.downsample.add_module('1', nn.BatchNorm2d(self.out_chan, eps=1e-05, momentum=0.1, affine=True,
                                                           track_running_stats=True))

            x = self.downsample(x_samp)
        elif self.down_sample_flag is False:
            x = x_samp

        return x


class custom_resnet(nn.Module):

    def __init__(self):
        super().__init__()

        self.conv1 = nn.Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)
        self.bn1 = nn.BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1, dilation=1, ceil_mode=False)



        # Layer 1
        self.layer1 = nn.Sequential()
        self.layer1.add_module('0', BasicBlock(in_chan=64, out_chan=64))
        self.layer1.add_module('1', BasicBlock(in_chan=64, out_chan=64))

        # Layer 2
        self.layer2 = nn.Sequential()
        self.layer2.add_module('0', BasicBlock(in_chan=64, out_chan=128))
        self.layer2.add_module('1', BasicBlock(in_chan=128, out_chan=128, down_sample=True))

        # Layer 3
        self.layer3 = nn.Sequential()
        self.layer3.add_module('0', BasicBlock())
        self.layer3.add_module('1', BasicBlock())

        # Layer 4
        self.layer4 = nn.Sequential()
        self.layer4.add_module('0', BasicBlock())
        self.layer4.add_module('1', BasicBlock())

        self.avgpool = nn.AdaptiveAvgPool2d(output_size=(1, 1))
        self.fc = nn.Linear(in_features=512, out_features=1000, bias=True)


    def forward(self, x):
        x = self.conv1(x)
        x = self.bn1(x)
        x = self.relu(x)
        x = self.maxpool(x)


        x = self.layer1(x)


        x = self.layer2(x)

        x = self.layer3(x)


        x = self.layer4(x)

        x = self.avgpool(x)
        x = self.fc(x)
        return x


class ResNetUnet(nn.Module):

    def __init__(self):
        super().__init__()

        resnet_50 = models.resnet50(pretrained=True)
        # print(resnet_50)

        for child in resnet_50.named_children():

            print(child)









if __name__ == "__main__":

    nat = ResNetUnet()









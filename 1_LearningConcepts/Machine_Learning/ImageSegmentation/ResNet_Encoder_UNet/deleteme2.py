import torch
import torch.nn.functional as F




x = torch.randn((1,1,128,128))


output = F.softmax(x, dim=2)

print(output)

print(f"MIN: {torch.min(output)}, MAX: {torch.max(output)}")


import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.utils.data import DataLoader, Dataset, random_split
from torchvision import transforms as transforms
from torchvision.utils import make_grid
from pytorch_lightning.loggers import TensorBoardLogger
import pytorch_lightning as pl
from pytorch_lightning.metrics.functional.classification import dice_score, iou, average_precision, f1_score

import matplotlib.pyplot as plt
from UNet import UNet

import os
import re
from PIL import Image
import cv2
import albumentations as A
from albumentations.pytorch import ToTensorV2
import numpy as np

from UNet import UNet
from Training import Unet_segmentation


class CarvanaTestDataset(Dataset):

    def __init__(self, test_data_dir="./Data/carvana/data/test/val_test"):
        super().__init__()

        self.test_dir = test_data_dir
        self.img_dir = self.test_dir + "/image"
        self.mask_dir = self.test_dir + "/mask"


        self.test_data = os.listdir(self.img_dir)
        self.mask_data = os.listdir(self.mask_dir)

        self.transform = transforms.Compose([transforms.Resize((572, 572)),
                                             transforms.ToTensor()])

        self.mask_transform = transforms.Compose([transforms.Resize((388, 388)),
                                             transforms.ToTensor()])

    def __len__(self):
        return len(self.test_data)

    def __getitem__(self, item):

        item_path = self.img_dir + f"./{self.test_data[item]}"
        mask_path = self.mask_dir + f"./{self.mask_data[item]}"

        img = Image.open(item_path)
        mask = Image.open(mask_path)

        transformed_image = self.transform(img)
        transformed_mask = self.mask_transform(mask)
        return transformed_image, transformed_mask, self.test_data[item]


if __name__ == "__main__":

    path_to_model = "./tb_logs/my_model/version_6/checkpoints/epoch=14.ckpt"
    model = Unet_segmentation.load_from_checkpoint(checkpoint_path=path_to_model)
    model.eval()

    pil_trans = transforms.ToPILImage()

    # Setup Test Data
    test_ds = CarvanaTestDataset()
    test_dl = DataLoader(test_ds, batch_size=1, num_workers=1, shuffle=False)

    dice_score_list = []

    for sample in test_dl:

        image, gt_mask, name = sample

        gt_mask = gt_mask.float()

        pred_mask = model(image)

        thres_hold_mask = (.75 < pred_mask).float()
        thres_mask_squeeze = thres_hold_mask.squeeze(dim=0)

        d_score = f1_score(thres_mask_squeeze, gt_mask)
        print("Dice Score: ",d_score )

        dice_score_list.append(d_score)


        output = pil_trans(thres_mask_squeeze)


        plt.imshow(output)
        plt.title(f"{name}")
        plt.show()

    print("Mean Dice Score", np.mean(dice_score_list))












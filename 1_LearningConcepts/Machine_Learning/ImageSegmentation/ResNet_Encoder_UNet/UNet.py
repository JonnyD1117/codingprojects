import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as trans
import torchvision.transforms.functional as TF
from torch.utils.data import Dataset, DataLoader, random_split
from torchvision.datasets import ImageFolder, DatasetFolder
import numpy as np
import albumentations as A
from albumentations.pytorch import ToTensorV2

from PIL import Image
import cv2

import os
import re
from shutil import copyfile

from tqdm import tqdm


# def process_raw_data(flag=False):
#     extract_training_data()
#     extract_training_data(dir="./Data/raw_data/images/validation", train=False)
#
#
# def extract_training_data(dir="./Data/raw_data/images/training", train=True):
#     training_path = dir
#     train_folders = os.listdir(training_path)
#
#     train_flag = train
#
#     ignore_list = ['misc', 'outliers']
#
#     for folder in train_folders:
#
#         sub_dir = training_path + f"/{folder}"
#
#         if os.path.getsize(sub_dir) == 0:
#             continue
#         if os.path.exists(sub_dir) is False:
#             continue
#
#         if folder == ignore_list[0] or folder == ignore_list[1]:
#             continue
#         else:
#
#             for subject in os.listdir(sub_dir):
#
#                 sub_sub_dir = sub_dir + f'/{subject}'
#                 image_folder = os.listdir(sub_sub_dir)
#
#                 output1 = None
#                 # print(sub_dir)
#                 # print(sub_sub_dir)
#
#                 for file_name in image_folder:
#                     if file_name.endswith('.jpg'):
#                         # print(file_name)
#                         r1 = re.findall(r"[0-9]{8}", file_name)
#
#                         # Compute the Numbers
#                         for i in range(0, len(r1)):
#                             if i == 0:
#                                 output1 = r1[i]
#                             else:
#                                 output1 += r1[i]
#
#                         output1 = int(output1)
#
#                 try:
#                     file_num = f"{output1:08}"
#
#                     if train_flag:
#                         target_dir = "./Data/training_data"
#                     else:
#                         target_dir = "./Data/validation_data"
#
#                     save_image_and_mask(file_num, sub_sub_dir, target_dir,train=train_flag)
#
#                 except:
#                     continue
#
#
# def save_image_and_mask(file_number,
#                         file_dir,
#                         target_dir,
#                         train=True):
#
#     img_postfix = ".jpg"
#     mask_postfix = "_seg.png"
#     base_name = "ADE_"
#     indicator = None
#     train_val_fold = None
#
#
#     file_num = file_number
#
#     if isinstance(file_num, str):
#         pass
#     else:
#         file_num = str(file_num)
#
#     if train:
#         indicator = "train_"
#         train_val_fold = '/training'
#     else:
#         indicator = "val_"
#         train_val_fold = '/validation'
#
#     file_name = base_name + indicator + file_num
#
#     img_name = file_name + img_postfix
#     mask_name = file_name + mask_postfix
#
#     old_image_path = file_dir + "/" + img_name
#     old_mask_path = file_dir + "/" + mask_name
#
#     new_image_path = target_dir + "/Images/" + img_name
#     new_mask_path = target_dir + "/Masks/" + mask_name
#
#     copyfile(old_image_path, new_image_path)
#     copyfile(old_mask_path, new_mask_path)


class ADE20K(Dataset):

    def __index__(self, root_dir =".", data_dir="/Data", train_dir='/training_data', val_dir='/validation_data', img_dir="/Images", mask_dir="/Masks"):

        # Training Data
        self.img_train_path = root_dir + data_dir + train_dir + img_dir
        self.mask_train_path = root_dir + data_dir + train_dir + mask_dir

        self.img_train_ds = ImageFolder(self.img_train_path)
        self.mask_train_ds = ImageFolder(self.mask_train_path)

        # Validation Data
        self.img_val_path = root_dir + data_dir + val_dir + img_dir
        self.mask_val_path = root_dir + data_dir + val_dir + mask_dir

        self.img_val_ds =ImageFolder(self.img_val_path)
        self.mask_val_ds =ImageFolder(self.mask_val_path)


    def __len__(self):
        pass

    def __getitem__(self, item):
        pass

class Carvana_Dataset(Dataset):

    def __init__(self, data_dir='./Data/carvana/data/', train_data=True, img_size=572):

        self.train_flag = train_data
        self.path_descript = None
        if self.train_flag:
            self.path_descript = 'train'
        else:
            self.path_descript = 'val'

        self.data_dir = data_dir + self.path_descript
        self.img_folder_path = self.data_dir + '/images'
        self.mask_folder_path = self.data_dir + '/masks'

        self.img_ds = os.listdir(self.img_folder_path)
        self.mask_ds = os.listdir(self.mask_folder_path)
        self.sample_paths = []

        # Populate the Sample_paths which will contain the directory location for both corresponding Image and Mask
        self.create_sample_paths()

        self.sample_transform = A.Compose([A.HorizontalFlip(),
                                           A.ShiftScaleRotate(border_mode=cv2.BORDER_CONSTANT,
                                                              scale_limit=0.3,
                                                              rotate_limit=(10, 30),
                                                              p=0.5),
                                           A.RandomResizedCrop(img_size, img_size),
                                           ToTensorV2()
                                           ])

    def create_sample_paths(self):
        sample_ds = zip(self.img_ds, self.mask_ds)

        for image, mask in sample_ds:

            img_num = re.split(r'\.', image)[0]
            mask_num = re.split(r'_mask', mask)[0]

            if img_num == mask_num:

                img_path = self.img_folder_path + f'/{image}'
                mask_path = self.mask_folder_path + f'/{mask}'

                self.sample_paths.append((img_path, mask_path))
            else:
                continue

    def __len__(self):

        return len(self.sample_paths)

    def __getitem__(self, item):

        img_path, mask_path = self.sample_paths[item]

        img = Image.open(img_path)
        mask = Image.open(mask_path)

        img_numpy = np.array(img) / 255
        mask_numpy = np.array(mask) / 255

        transformed = self.sample_transform(image=img_numpy, mask=mask_numpy)

        trans_image = transformed['image']
        trans_mask = transformed['mask']

        trans_sample = (trans_image, trans_mask)

        return trans_sample


class UNet(nn.Module):

    def __init__(self):
        super().__init__()

        self.max_pooling = nn.MaxPool2d(kernel_size=2, stride=2)

        self.L1_down = self.conv_block(in_chan=3, out_chan=64)
        self.L2_down = self.conv_block(in_chan=64, out_chan=128)
        self.L3_down = self.conv_block(in_chan=128, out_chan=256)
        self.L4_down = self.conv_block(in_chan=256, out_chan=512)
        self.L5 = self.conv_block(in_chan=512, out_chan=1024)

        self.L4_up = self.conv_block(in_chan=1024, out_chan=512)
        self.L3_up = self.conv_block(in_chan=512, out_chan=256)
        self.L2_up = self.conv_block(in_chan=256, out_chan=128)
        self.L1_up = self.conv_block(in_chan=128, out_chan=64)

        self.up_conv1 = nn.ConvTranspose2d(in_channels=1024, out_channels=512, kernel_size=2, stride=2)
        self.up_conv2 = nn.ConvTranspose2d(in_channels=512, out_channels=256, kernel_size=2, stride=2)
        self.up_conv3 = nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=2, stride=2)
        self.up_conv4 = nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=2, stride=2)

        self.final_conv = nn.Conv2d(64, 1, 1)

    def conv_block(self, in_chan, out_chan, kernal=3, stride=1, pad=0):

        layer = nn.Sequential(
            nn.Conv2d(in_chan, out_chan, kernal, stride, pad),
            nn.ReLU(),
            nn.Conv2d(out_chan, out_chan, kernal, stride, pad),
            nn.ReLU()

        )

        return layer

    def crop_img(self, tensor, target_tensor):

        target_size = target_tensor.size()[2]
        tensor_size = tensor.size()[2]

        delta = tensor_size - target_size
        delta = delta//2

        return tensor[:, :, delta:tensor_size-delta, delta:tensor_size-delta]

    def forward(self, x):

        # Encoder
        x1 = self.L1_down(x)
        x = self.max_pooling(x1)
        x2 = self.L2_down(x)

        x = self.max_pooling(x2)
        x3 = self.L3_down(x)

        x = self.max_pooling(x3)
        x4 = self.L4_down(x)

        # Bottle Neck
        x = self.max_pooling(x4)
        x5 = self.L5(x)


        # Decoder
        x5_up = self.up_conv1(x5)

        x4_crop = self.crop_img(x4, x5_up)
        x4_cat = torch.cat((x4_crop, x5_up), dim=1)
        x4_conv = self.L4_up(x4_cat)
        x4_up = self.up_conv2(x4_conv)

        x3_crop = self.crop_img(x3, x4_up)
        x3_cat = torch.cat((x3_crop, x4_up), dim=1)
        x3_conv = self.L3_up(x3_cat)
        x3_up = self.up_conv3(x3_conv)

        x2_crop = self.crop_img(x2, x3_up)
        x2_cat = torch.cat((x2_crop, x3_up), dim=1)
        x2_conv = self.L2_up(x2_cat)
        x2_up = self.up_conv4(x2_conv)

        x1_crop = self.crop_img(x1, x2_up)
        x1_cat = torch.cat((x1_crop, x2_up), dim=1)
        x1_conv = self.L1_up(x1_cat)

        output_conv = self.final_conv(x1_conv)

        output_sig = torch.sigmoid(output_conv)

        return output_sig


if __name__ == "__main__":

    car_ds = Carvana_Dataset()
    Unet_dl = DataLoader(car_ds, batch_size=4, shuffle=True, num_workers=4, drop_last=True)

    model = UNet()
    model = model.float()

    for batch in Unet_dl:
        img, mask = batch
        img, mask = img.float(), mask.float()

        print(f"Min Img {torch.min(img)} Max Img {torch.max(img)}")
        print(f"Min Mask {torch.min(mask)} Max Mask {torch.max(mask)}")

        print(img.shape)
        print(mask.shape)
        output = model(img)
        print(output.shape)
        break

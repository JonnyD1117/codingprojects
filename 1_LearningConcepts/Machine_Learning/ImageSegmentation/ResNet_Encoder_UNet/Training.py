import torch
import torch.nn as nn
from torch.nn import functional as F
from torch.utils.data import DataLoader, Dataset, random_split
from torchvision import transforms as transforms
from torchvision.utils import make_grid
from pytorch_lightning.loggers import TensorBoardLogger
import pytorch_lightning as pl
from pytorch_lightning.metrics.functional.classification import dice_score, iou, average_precision, f1_score

from UNet import UNet

import os
import re
from PIL import Image
import cv2
import albumentations as A
from albumentations.pytorch import ToTensorV2
import numpy as np


class Carvana_Dataset(Dataset):

    def __init__(self, data_dir='./Data/carvana/data/', train_data=True, img_size=572):

        self.train_flag = train_data
        self.path_descript = None
        if self.train_flag:
            self.path_descript = 'train'
        else:
            self.path_descript = 'val'

        self.data_dir = data_dir + self.path_descript
        self.img_folder_path = self.data_dir + '/images'
        self.mask_folder_path = self.data_dir + '/masks'

        self.img_ds = os.listdir(self.img_folder_path)
        self.mask_ds = os.listdir(self.mask_folder_path)
        self.sample_paths = []

        # Populate the Sample_paths which will contain the directory location for both corresponding Image and Mask
        self.create_sample_paths()

        self.sample_transform = A.Compose([A.HorizontalFlip(),
                                           A.ShiftScaleRotate(border_mode=cv2.BORDER_CONSTANT,
                                                              scale_limit=0.3,
                                                              rotate_limit=(10, 30),
                                                              p=0.5),
                                           A.GridDistortion(),
                                           # A.RandomBrightnessContrast(),
                                           A.RandomResizedCrop(img_size, img_size),
                                           ToTensorV2()
                                           ])

    def create_sample_paths(self):
        sample_ds = zip(self.img_ds, self.mask_ds)

        for image, mask in sample_ds:

            img_num = re.split(r'\.', image)[0]
            mask_num = re.split(r'_mask', mask)[0]

            if img_num == mask_num:

                img_path = self.img_folder_path + f'/{image}'
                mask_path = self.mask_folder_path + f'/{mask}'

                self.sample_paths.append((img_path, mask_path))
            else:
                continue

    def __len__(self):

        return len(self.sample_paths)

    def __getitem__(self, item):

        img_path, mask_path = self.sample_paths[item]

        img = Image.open(img_path)
        mask = Image.open(mask_path)

        img_numpy = np.array(img) / 255
        mask_numpy = np.array(mask) / 255

        transformed = self.sample_transform(image=img_numpy, mask=mask_numpy)

        trans_image = transformed['image']
        trans_mask = transformed['mask']

        trans_mask = torch.unsqueeze(trans_mask, dim=0)

        trans_sample = (trans_image, trans_mask)

        return trans_sample


class CarvanaDataModule(pl.LightningDataModule):

    def __init__(self, batch_size=4):
        super().__init__()
        self.batch_size = batch_size
        # self.data_dir = data_dir
        self.train_ds = None
        self.val_ds = None

    def setup(self, stage=None):
        self.train_ds = Carvana_Dataset(train_data=True)
        self.val_ds = Carvana_Dataset(train_data=False)

    def train_dataloader(self):
        return DataLoader(self.train_ds, batch_size=self.batch_size, drop_last=True, num_workers=4, shuffle=True)

    def val_dataloader(self):
        return DataLoader(self.val_ds, batch_size=self.batch_size, num_workers=4, shuffle=False)


class Unet_segmentation(pl.LightningModule):
    
    def __init__(self):
        super().__init__()

        self.unet_model = UNet().float()
        self.lr = 1e-3

        self.loss_func = nn.BCELoss()

    def forward(self, x):
        return self.unet_model(x)
    
    def configure_optimizers(self):

        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer,
                                                               mode='min',
                                                               factor=0.2,
                                                               patience=2,
                                                               threshold=0.0001,
                                                               threshold_mode='rel',
                                                               min_lr=1e-6,
                                                               verbose=False)
        # return [optimizer], [scheduler]
        return {'optimizer': optimizer, 'lr_scheduler': scheduler, 'monitor': 'val_loss'}

    def training_step(self, batch, batch_idx):

        image, mask = batch
        image, mask = image.float(), mask.float()
        pred_mask = self.unet_model(image)

        cmask = self.crop_mask(mask, pred_mask)

        input_grid = make_grid(image)
        pred_mask_grid = make_grid(pred_mask)
        gt_mask_grid = make_grid(cmask)

        self.logger.experiment.add_image("Input Images", input_grid, 0)
        self.logger.experiment.add_image("Predicted Masks", pred_mask_grid, 0)
        self.logger.experiment.add_image("Ground Truth Masks", gt_mask_grid, 0)



        # loss = 1. - f1_score(pred_mask, cmask, num_classes=1)
        loss = self.loss_func(pred_mask, cmask)

        # loss.requires_grad = True
        self.log('train_loss', loss)


        return loss

    def on_train_epoch_start(self):
        self.log('Lr', self.lr)
    def validation_step(self, batch, batch_idx):
        image, mask = batch
        image, mask = image.float(), mask.float()

        pred_mask = self.unet_model(image)
        cmask = self.crop_mask(mask, pred_mask)

        # loss = 1. - f1_score(pred_mask, cmask, num_classes=1)
        loss = self.loss_func(pred_mask, cmask)

        # loss.requires_grad = True

        self.log('val_loss', loss)
        return loss

    def crop_mask(self, tensor, target_tensor):

        target_size = target_tensor.size()[2]
        tensor_size = tensor.size()[2]

        delta = tensor_size - target_size
        delta = delta // 2

        cropped_tensor = tensor[:, :, delta:tensor_size - delta, delta:tensor_size - delta]
        return cropped_tensor


if __name__ == "__main__":

    carvana_dm = CarvanaDataModule()
    tb_logger = TensorBoardLogger('tb_logs', name='my_model')

    check_point_path = "./tb_logs/my_model/version_5/checkpoints/epoch=13.ckpt"

    UNET = Unet_segmentation()
    # trainer = pl.Trainer(gpus=1, fast_dev_run=False, accumulate_grad_batches=2, max_epochs=100, logger=tb_logger)
    trainer = pl.Trainer(gpus=1, fast_dev_run=False, resume_from_checkpoint=check_point_path, accumulate_grad_batches=3, max_epochs=100, logger=tb_logger)

    # trainer.tune(UNET, datamodule=carvana_dm)
    trainer.fit(UNET, datamodule=carvana_dm)

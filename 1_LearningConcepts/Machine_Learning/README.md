# Machine Learning 

Currently, this directory contains previous machine learning projects of mine (written in python) that I have archived. 

## Objective 
The goal of projects in this directory are primarily as reference to past work I have done, but also might provide the starting point for more interesting application of Machine Learning that I would like to integrate into larger more complex systems (probably written in C++).
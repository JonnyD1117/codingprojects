import torch
import torch.nn as nn
import torchvision
from PIL import Image
from torch.utils.data import DataLoader, Dataset
import os
import albumentations as A
from albumentations.pytorch.transforms import ToTensorV2
import cv2
import re
import numpy as np


class Carvana_Dataset(Dataset):

    def __init__(self, data_dir='./Data/carvana/data/', train_data=True, img_size=572):

        self.train_flag = train_data
        self.path_descript = None
        if self.train_flag:
            self.path_descript = 'train'
        else:
            self.path_descript = 'val'

        self.data_dir = data_dir + self.path_descript
        self.img_folder_path = self.data_dir + '/images'
        self.mask_folder_path = self.data_dir + '/masks'

        self.img_ds = os.listdir(self.img_folder_path)
        self.mask_ds = os.listdir(self.mask_folder_path)
        self.sample_paths = []

        # Populate the Sample_paths which will contain the directory location for both corresponding Image and Mask
        self.create_sample_paths()

        self.sample_transform = A.Compose([A.HorizontalFlip(),
                                           A.ShiftScaleRotate(border_mode=cv2.BORDER_CONSTANT,
                                                              scale_limit=0.3,
                                                              rotate_limit=(10, 30),
                                                              p=0.5),
                                           A.RandomResizedCrop(img_size, img_size),
                                           ToTensorV2()
                                           ])

    def create_sample_paths(self):
        sample_ds = zip(self.img_ds, self.mask_ds)

        for image, mask in sample_ds:

            img_num = re.split(r'\.', image)[0]
            mask_num = re.split(r'_mask', mask)[0]

            if img_num == mask_num:

                img_path = self.img_folder_path + f'/{image}'
                mask_path = self.mask_folder_path + f'/{mask}'

                self.sample_paths.append((img_path, mask_path))
            else:
                continue

    def __len__(self):

        return len(self.sample_paths)

    def __getitem__(self, item):

        img_path, mask_path = self.sample_paths[item]

        img = Image.open(img_path)
        mask = Image.open(mask_path)

        img_numpy = np.array(img) / 255
        mask_numpy = np.array(mask) / 255

        transformed = self.sample_transform(image=img_numpy, mask=mask_numpy)

        trans_image = transformed['image']
        trans_mask = transformed['mask']

        trans_sample = (trans_image, trans_mask)

        return trans_sample

class ResnetBasedSegmentationNet(nn.Module):

    def __init__(self):
        super().__init__()
        resnet50 = torchvision.models.resnet50(pretrained=True)

        self.conv1 = resnet50.conv1.requires_grad_(False)
        self.bn1 = resnet50.bn1.requires_grad_(False)
        self.relu = resnet50.relu.requires_grad_(False)
        self.layer1 = resnet50.layer1.requires_grad_(False)
        self.layer2 = resnet50.layer2.requires_grad_(False)
        self.layer3 = resnet50.layer3.requires_grad_(False)
        self.layer4 = resnet50.layer4.requires_grad_(False)

        self.decoder1 = self.UpConv_Block(inchan=2048, outchan=1024)        # Upsampled Inchan: 2048 OutChan: 1024
        self.decoder2 = self.UpConv_Block(inchan=1024, outchan=512)        # Upsampled Inchan: 1024 OutChan: 512
        self.decoder3 = self.UpConv_Block(inchan=512, outchan=256)        # Upsampled Inchan: 512 OutChan: 256
        self.decoder4 = self.UpConv_Block(inchan=256, outchan=128)        # Upsampled Inchan: 256 OutChan: 128

        self.end_conv = self.convbloc()         # Convolutions Inchan: 128 Outchan:64
        self.output_layer = nn.Conv2d(in_channels=64, out_channels=1, kernel_size=1)

    def convbloc(self, inchan=128, outchan=64 ):

        layer = nn.Sequential(nn.Conv2d(in_channels=inchan, out_channels=outchan, stride=1, kernel_size=3),
                              nn.ReLU(),
                              nn.Conv2d(in_channels=outchan, out_channels=outchan, stride=1, kernel_size=3),
                              nn.ReLU()
                              )

        return layer

    def UpConv_Block(self, inchan, outchan):


        up_block = nn.Sequential(nn.ConvTranspose2d(in_channels=inchan, out_channels=outchan, stride=2, kernel_size=2),
                                     nn.Conv2d(in_channels=outchan, out_channels=outchan, stride=1, kernel_size=3),
                                     nn.ReLU(),
                                     nn.Conv2d(in_channels=outchan, out_channels=outchan, stride=1, kernel_size=3),
                                     nn.ReLU())

        return up_block

    def forward(self, x):
        # print("INPUT SHAPE", x.shape)
        x = self.relu(self.bn1(self.conv1(x)))
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)

        # print("BOTTLENECK SHAPE", x.shape)

        x = self.decoder1(x)
        # print("Decoder #1 SHAPE", x.shape)

        x = self.decoder2(x)
        # print("Decoder #2 SHAPE", x.shape)

        x = self.decoder3(x)
        # print("Decoder #3 SHAPE", x.shape)

        x = self.decoder4(x)
        # print("Decoder #4 SHAPE", x.shape)

        x = self.end_conv(x)
        # print("Final Conv SHAPE", x.shape)

        output = self.output_layer(x)
        # print("OUTPUT SHAPE", output.shape)

        output_sig = torch.sigmoid(output)

        return output_sig

    def crop_img(self, tensor, target_tensor):
        target_size = target_tensor.size()[2]
        tensor_size = tensor.size()[2]

        delta = tensor_size - target_size
        delta = delta // 2

        return tensor[:, :, delta:tensor_size - delta, delta:tensor_size - delta]


if __name__ == "__main__":

    net = ResnetBasedSegmentationNet()

    x = torch.randn((1, 3, 572, 572))
    pred_mask = net(x)


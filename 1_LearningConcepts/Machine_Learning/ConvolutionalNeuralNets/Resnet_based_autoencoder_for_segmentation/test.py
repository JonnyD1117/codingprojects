import torch
import torch.nn as nn
import torchvision


resnet = torchvision.models.resnet50(pretrained=True)

# print(resnet)
# print(list(resnet.children()))

# print(resnet.state_dict())



# for param in resnet.named_parameters():
#     param_name, param_state = param
#     # print(param)
#     print(param_name)
#     print(param_state)
#     print("###############################################################")
#     print("###############################################################")
#     print("###############################################################")
#     print("###############################################################")
#
#
#     break

for layer in resnet.named_children():
    name, detail = layer

    print(name)
    print(detail)

    print(layer)

    # param = dict(resnet.named_parameters())
    # print(param[str(name) + ".weight"])


    break

resnet_State_dict = resnet.state_dict()

print(resnet_State_dict)
# print(resnet_State_dict['conv1.weight'])

new_layer = nn.Conv2d(3, 64, kernel_size=(7, 7), stride=(2, 2), padding=(3, 3), bias=False)


new_layer.load_state_dict({'new_layer.weight': resnet_State_dict['conv1.weight']})

print(new_layer.state_dict())

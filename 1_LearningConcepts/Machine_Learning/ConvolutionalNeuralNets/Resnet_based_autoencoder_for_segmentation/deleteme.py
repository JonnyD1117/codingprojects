import torch
import torch.nn as nn
import torchvision
from PIL import Image


class testRes(nn.Module):

    def __init__(self):
        super().__init__()

        resnet50 = torchvision.models.resnet50(pretrained=True)

        print(resnet50)
        self.conv1 = resnet50.conv1
        self.bn1 = resnet50.bn1
        self.relu = resnet50.relu
        self.layer1 = resnet50.layer1
        self.layer2 = resnet50.layer2
        self.layer3 = resnet50.layer3
        self.layer4 = resnet50.layer4
        self.avgpool = resnet50.avgpool
        self.fc = resnet50.fc



    def forward(self, x):

        x = self.relu(self.bn1(self.conv1(x)))
        x = self.layer1(x)
        x = self.layer2(x)
        x = self.layer3(x)
        x = self.layer4(x)
        x = self.avgpool(x)

        x_flat = nn.Flatten()(x)
        output = self.fc(x_flat)

        return output





model = testRes()

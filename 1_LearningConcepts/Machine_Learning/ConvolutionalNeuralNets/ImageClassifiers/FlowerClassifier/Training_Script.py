
import torch
import torch.nn as nn
import pytorch_lightning as pl
from torch.nn import functional as F
from torch.utils.data import DataLoader, random_split, Dataset
from torchvision import transforms
from torchvision.datasets import ImageFolder

import matplotlib.pyplot as plt


class FlowerDataSet(Dataset):

    def __init__(self,  root_dir='./', img_height=200, img_width=200, transform=None):

        self.root_dir = root_dir
        self.data_dir = root_dir + 'Data/raw_data/'

        if transform is None:
            self.transform = transforms.Compose([
                transforms.Resize((img_height, img_width)),
                transforms.ToTensor(),
                transforms.Normalize(std=[0.229, 0.224, 0.225], mean=[0.485, 0.456, 0.406])
            ])
        else:
            self.transform = transform

        self.raw_data = ImageFolder(self.data_dir, transform=self.transform)

    def __len__(self):
        return len(self.raw_data)

    def __getitem__(self, idx):

        return self.raw_data[idx]


class FlowerDataModule(pl.LightningDataModule):

    def __init__(self, data_dir='./', batch_size=4, num_chan=3, img_height=200, img_width=200, num_class=6):
        super().__init__()

        self.data_dir = data_dir
        self.batch_size = batch_size

        self.flower_data_train = None
        self.flower_data_val = None

        self.num_classes = num_class
        self.dims = (num_chan, img_height, img_width)

    # def prepare_data(self):
    #     # Download Data

    def setup(self, stage=None):
        # Import Raw Data Set
        complete_dataset = FlowerDataSet(img_height=self.dims[1], img_width=self.dims[2])

        # Split into Train/Validation Sets
        self.flower_data_train, self.flower_data_val = random_split(complete_dataset, [3550, 800])

    def train_dataloader(self):
        return DataLoader(self.flower_data_train, batch_size=self.batch_size, drop_last=True, shuffle=True)

    def val_dataloader(self):
        return DataLoader(self.flower_data_val, batch_size=self.batch_size, drop_last=True)


class ImageClassifierNetwork(pl.LightningModule):
    def __init__(self, input_chan=3, output_class=6, learning_rate=1e-3):
        super().__init__()

        self.num_chan_in = input_chan
        self.num_classes = output_class
        self.lr = learning_rate

        self._to_linear = None

        # Setup NN Features for Assembly
        self.conv1 = nn.Conv2d(input_chan, 32, 5)
        self.conv2 = nn.Conv2d(32, 64, 5)
        self.conv3 = nn.Conv2d(64, 32, 5)

        self.d1 = nn.Dropout2d(.25)
        self.d2 = nn.Dropout2d(.3)

        self.fc1 = nn.Linear(14112, 1024)
        # self.fc1 = None

        self.fc2 = nn.Linear(1024, output_class)

        self.criterion = nn.CrossEntropyLoss()

    def Net(self, x):

        x1 = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x1d = self.d1(x1)
        x2 = F.max_pool2d(F.relu(self.conv2(x1d)), (2, 2))
        x2d = self.d2(x2)
        x3 = F.max_pool2d(F.relu(self.conv3(x2d)), (2, 2))

        if self.fc1 is None:

            x3_flat = x3.flatten(start_dim=1)
            print("Flattness things",x3_flat.shape)
            # self._to_linear = x3_flat.shape[1]*x3_flat.shape[1]*x3_flat.shape[1]
            self._to_linear = x3_flat.shape[1]

            self.fc1 = nn.Linear(self._to_linear, 1024)

        x4 = self.fc1(x3.flatten(start_dim=1))
        x_out = self.fc2(x4)

        return x_out

    def forward(self, x):
        # use forward for inference/predictions

        return self.Net(x)

    def training_step(self, batch, batch_idx):


        x, y = batch
        y_hat = self(x)

        loss = F.cross_entropy(y_hat, y)

        self.log('train_loss', loss, on_epoch=True)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y = batch
        y_hat = self(x)
        loss = self.criterion(y_hat, y)

        self.log('valid_loss', loss, on_step=True)

    def configure_optimizers(self):
        # self.hparams available because we called self.save_hyperparameters()

        optimizer = torch.optim.Adam(self.parameters(), lr=self.lr)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=3, gamma=.75)

        return [optimizer], [scheduler]


if __name__ == '__main__':

    print("Is GPU Available to PyTorch = ", torch.cuda.is_available())
    # print('The GPU is called', torch.cuda.get_device_name())
    # print('Current Device: ', torch.cuda.current_device())



    dm = FlowerDataModule()
    model = ImageClassifierNetwork()
    trainer = pl.Trainer(max_epochs=20)

    trainer.fit(model, dm)

    torch.save(model, "Model/TrainedModel.pt")

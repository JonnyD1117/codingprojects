import pytorch_lightning as pl
import streamlit as st



st.title("Flower Classifier")
st.subheader("This is where interesting stuff goes!")

rose_img = "Data/raw_data/rose/172311368_49412f881b.jpg"
daisy_img = "Data/raw_data/daisy/11642632_1e7627a2cc.jpg"
dand_img = "Data/raw_data/dandelion/10443973_aeb97513fc_m.jpg"
sunflower_img = 'Data/raw_data/sunflower/27465811_9477c9d044.jpg'
tulip_img = "Data/raw_data/tulip/11746452_5bc1749a36.jpg"



flower_dict = {"Roses": rose_img, "Sunflowers":sunflower_img ,"Daisies":daisy_img , "Dandelions":dand_img, "Tulips":tulip_img}

flower_state =  st.sidebar.selectbox('Which Flower do you Want to See?', ['Roses', "Sunflowers", "Daisies", "Dandelions", "Tulips"])

if st.checkbox('Show Awesome Picture'):

    st.image(flower_dict[flower_state])

else:
    st.write("Please Select 'Show Awesome Picture' to see flowers.")







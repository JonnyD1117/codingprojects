import torch
import torch.nn as nn
import torch.nn.functional as F
Yhat = torch.tensor([[ 0.0142,  0.0516, -0.0093,  0.0136,  0.0366, -0.0008],
        [ 0.0188,  0.0012, -0.0007, -0.0018,  0.0059, -0.0384],
        [ 0.0443,  0.0374, -0.0092, -0.0056,  0.0007,  0.0025],
        [ 0.0182, -0.0123, -0.0260,  0.0085, -0.0002, -0.0474]])

print(Yhat.shape)
crit = nn.CrossEntropyLoss()

Y = torch.tensor([4, 3, 4, 5])
Yhat_argmax =  torch.tensor([2, 1, 2, 2])


loss = crit(Yhat_argmax.view(-1), Y.view(-1))


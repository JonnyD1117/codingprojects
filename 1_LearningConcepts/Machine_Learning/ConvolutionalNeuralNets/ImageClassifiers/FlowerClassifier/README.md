# Flower Classifier 

This exercise uses the Kaggle Flower Classification dataset inorder to run image classification using Pytorch Lightning and Streamlit, in order to train the classifier and then access the model via a Streamlit designed Web app.



## Types of Flowers used for Training: 

* Daisies
* Dandelions
* Roses
* Sunflowers
* Tuplips
* California Poppies


## How to run Streamlit Application Locally

To run the streamlit web application for flower classification, launch a new terminal, change into the "FlowerClassifer" directory, and run the following command.


``` 
streamlit run Application.py
```


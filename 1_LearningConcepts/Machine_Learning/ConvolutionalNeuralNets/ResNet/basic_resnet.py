import torch 
import torch.nn as nn 
import torch.nn.functional as F 


class ResNet18(nn.Module):

    def __init__(self):
        super().__init__()

        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2)
        self.bn1 = nn.BatchNorm2d(64)
        self.maxPool = nn.MaxPool2d(kernel_size=3, stride=2)

        self.conv2 = nn.Conv2d(64, 64, kernel_size=3)
        self.bn2 = nn.BatchNorm2d(64)

        self.conv3 = nn.Conv2d(64, 64, kernel_size=3)
        self.bn3 = nn.BatchNorm2d(64)

        self.conv4 = nn.Conv2d(64, 64, kernel_size=3)
        self.bn5 = nn.BatchNorm2d(64)

        self.conv5 = nn.Conv2d(64, 64, kernel_size=3)
        self.bn5 = nn.BatchNorm2d(64)

        self.conv6 = nn.Conv2d(64, 128, kernel_size=3, stride=2)
        self.bn6 = nn.BatchNorm2d(128)

        self.conv7 = nn.Conv2d(128, 128, kernel_size=3)
        self.bn7 = nn.BatchNorm2d(128)

        self.conv8 = nn.Conv2d(128, 128, kernel_size=3)
        self.bn8 = nn.BatchNorm2d(128)

        self.conv9 = nn.Conv2d(128, 128, kernel_size=3)
        self.bn9 = nn.BatchNorm2d(128)

        self.conv10 = nn.Conv2d(128, 256, kernel_size=3, stride=2)
        self.bn10 = nn.BatchNorm2d(256)

        self.conv11 = nn.Conv2d(256, 256, kernel_size=3)
        self.bn10 = nn.BatchNorm2d(256)

        self.conv12 = nn.Conv2d(256, 256, kernel_size=3)
        self.bn12 = nn.BatchNorm2d(256)

        self.conv13 = nn.Conv2d(256, 256, kernel_size=3)
        self.bn13 = nn.BatchNorm2d(256)

        self.conv14 = nn.Conv2d(256, 512, kernel_size=3, stride=2)
        self.bn14 = nn.BatchNorm2d(512)

        self.conv15 = nn.Conv2d(512, 512, kernel_size=3)
        self.bn15 = nn.BatchNorm2d(512)

        self.conv16 = nn.Conv2d(512, 512, kernel_size=3)
        self.bn16 = nn.BatchNorm2d(512)

        self.conv17 = nn.Conv2d(512, 512, kernel_size=3)
        self.bn17 = nn.BatchNorm2d(512)

        self.avgPool = nn.AvgPool2d(1)

        self.fc = nn.Linear(0, 5)


    def forward(self, x):

    





        return x

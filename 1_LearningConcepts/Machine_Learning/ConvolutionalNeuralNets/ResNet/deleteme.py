import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as trans
import torchvision.transforms.functional as TF
from torch.utils.data import Dataset, DataLoader, random_split
from torchvision.datasets import ImageFolder
import matplotlib.pyplot as plt

from tqdm import tqdm





# A = torch.randn((3,5,5))
# print(A)
# print(A.shape)
# flat = nn.Flatten(start_dim=1)(A)
# print(flat)
# print(flat.shape[1])
#
# print(flat.size())
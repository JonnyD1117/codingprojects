import torch
import torch.nn
import torch.nn.functional as F
from ResNet18 import ResNet18, ResBlock, ConvBlock
from torchvision.transforms import transforms as T
import matplotlib.pyplot as plt
import numpy as np


from PIL import Image




model = torch.load('./models/Resnet18.pt').to('cpu')



img_rose = './Data/Test_Data/rose.jpg'
img_sun = './Data/Test_Data/sunflower.jpg'
img_tul = './Data/Test_Data/tulips.jpg'

img_das = './Data/Test_Data/daisy.jpg'
img_dand = './Data/Test_Data/dandelion.jpg'



image = img_dand


transform = T.Compose([T.Resize((224, 224)),
                      T.ToTensor()])

numpy_image = np.array(Image.open(image))

input_img = transform(Image.open(image))

input_img = input_img.unsqueeze(dim=0)




output = model(input_img)

output = F.softmax(output, dim=1)
print(output.shape)

class_output = torch.argmax(output)

print(class_output)

print(output)

prediction_dict = {0:' Daisy', 1:' Dandelion', 2: ' Rose', 3:' Sunflower', 4: ' Tulip'}


plt.imshow(numpy_image)
plt.title(f"Predicted {prediction_dict[class_output.item()]}")
plt.show()

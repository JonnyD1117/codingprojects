import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torchvision.transforms as trans
import torchvision.transforms.functional as TF
from torch.utils.data import Dataset, DataLoader, random_split
from torchvision.datasets import ImageFolder
import numpy as np

from tqdm import tqdm


class ResBlock(nn.Module):

    def __init__(self, in_chan, stride1=1, stride2=1):
        super().__init__()

        self.conv1 = nn.Conv2d(in_chan, in_chan, kernel_size=3, stride=stride1, padding=1)
        self.conv2 = nn.Conv2d(in_chan, in_chan, kernel_size=3, stride=stride2, padding=1)

        self.bn1 = nn.BatchNorm2d(in_chan)
        self.bn2 = nn.BatchNorm2d(in_chan)

    def forward(self, x):

        x1 = F.relu(self.bn1(self.conv1(x)))
        x2 = F.relu(self.bn2(self.conv2(x1)))

        sum = x + x2

        return sum


class ConvBlock(nn.Module):

    def __init__(self, in_chan, out_chan, stride1=2, stride2=1):
        super().__init__()

        self.conv1 = nn.Conv2d(in_chan, out_chan, kernel_size=3, stride=stride1, padding=1)
        self.conv2 = nn.Conv2d(out_chan, out_chan, kernel_size=3, stride=stride2, padding=1)
        self.skip_conv = nn.Conv2d(in_chan, out_chan, kernel_size=1, stride=2)

        self.bn1 = nn.BatchNorm2d(out_chan)
        self.bn2 = nn.BatchNorm2d(out_chan)

    def forward(self, x):

        x1 = F.relu(self.bn1(self.conv1(x)))
        x2 = F.relu(self.bn2(self.conv2(x1)))

        x_skip = self.skip_conv(x)

        sum = x2 + x_skip

        return sum


class ResNet18(nn.Module):

    def __init__(self, batch_size=4, input_channels=3, in_features=18432, output_features=5, image_size=224):
        super().__init__()

        self.features = None
        self.batch_size = batch_size
        self.num_input_chan = input_channels
        self.out_feat = output_features
        self.img_size = image_size

        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2)
        self.bn1 = nn.BatchNorm2d(64)
        self.maxPool = nn.MaxPool2d(kernel_size=3, stride=2)

        self.RB1 = ResBlock(in_chan=64)
        self.RB2 = ResBlock(in_chan=64)

        self.CB1 = ConvBlock(in_chan=64, out_chan=128)
        self.RB3 = ResBlock(in_chan=128)
        self.CB2 = ConvBlock(in_chan=128, out_chan=256)

        self.RB4 = ResBlock(in_chan=256)
        self.CB3 = ConvBlock(in_chan=256, out_chan=512)
        self.RB5 = ResBlock(in_chan=512)

        self.avg_pool = nn.AvgPool2d(kernel_size=2, stride=1)
        # self.output_features = None
        # self.fc = None
        self.fc = nn.Linear(in_features, out_features=self.out_feat)


    def forward(self, x):


        x = self.maxPool(self.bn1(self.conv1(x)))
        x = self.RB1(x)
        x = self.RB2(x)
        x = self.CB1(x)
        x = self.RB3(x)
        x = self.CB2(x)
        x = self.RB4(x)
        x = self.CB3(x)
        x = self.RB5(x)

        x = self.avg_pool(x)

        if self.features is None:
            pass

            # flat_layer = nn.Flatten()(x)
            # self.features = flat_layer.shape[1]
            #
            # self.fc = nn.Linear(in_features=self.features, out_features=self.out_feat)

        return self.fc(nn.Flatten()(x))


class FlowerDS(Dataset):

    def __init__(self, root_dir="./", data_dir='Data/', input_width=224, input_height=224):

        self.root_dir = root_dir
        self.data_dir = data_dir
        self.data_folder = self.root_dir + self.data_dir
        self.dataset = ImageFolder(self.data_folder)

        self.img_width = input_width
        self.img_height = input_height

        self.transform = trans.Compose([trans.Resize(350),
                                        trans.RandomResizedCrop(self.img_width),
                                        trans.RandomGrayscale(),
                                        trans.RandomHorizontalFlip(),
                                        # trans.RandomAffine(12, shear=.5),
                                        trans.ToTensor()
                                        ])

    def __len__(self):

        return len(self.dataset)

    def __getitem__(self, item):

        self.dataset = ImageFolder(self.data_folder, transform=self.transform)

        return self.dataset[item]


def prepare_data(val_pct=.15):
    # Training & Validation DataSet
    train_ds = FlowerDS(root_dir='C:/Users/Indy-Windows/Documents/Machine-Learning-Practice/FlowerClassifer/Data/',
                        data_dir='Regular/')
    # train_ds = FlowerDS(root_dir='E:/DataSets/FlowerDS/', data_dir='Regular/')

    total_len = len(train_ds)
    val_len = int(total_len * val_pct)
    train_len = total_len - val_len

    val_ds = random_split(train_ds, [train_len, val_len])

    return train_ds, val_ds


def train(training_dataset, num_epoch=35, batch_size=15, device='gpu', save_model=False):

    if device == "cpu":
        device = 'cpu'
    else:
        device = 'cuda:0'

    model = ResNet18(batch_size=batch_size).to(device)
    loss_criterion = nn.CrossEntropyLoss()
    optimizer = optim.Adam(model.parameters(), lr=.0001)

    running_loss = 0

    # Training & Validation Dataloaders
    train_dl = DataLoader(training_dataset, batch_size=batch_size, shuffle=True, drop_last=True, num_workers=4)

    EPOCHS = num_epoch

    model.zero_grad()

    for epoch in range(EPOCHS):
        with tqdm(total=len(train_dl)) as pbar:
            for count, sample in enumerate(train_dl):

                # print(sample)
                img, label = sample
                img, label = img.to(device), label.to(device)

                output = model(img)

                loss = loss_criterion(output, label)

                running_loss += loss.item()

                loss.backward()
                optimizer.step()
                model.zero_grad()
                pbar.update()
                pbar.set_description(f'Training Model - Epoch {epoch}, Loss {loss.item(): .4f}, Running_loss {running_loss:.4f}')

    if save_model:
        torch.save(model, './models/Resnet18.pt')


def validation(val_ds, batch_size=4, device='gpu'):

    if device == "cpu":
        device = 'cpu'
    else:
        device = 'cuda:0'

    batch_size = batch_size

    model = ResNet18(batch_size=batch_size).to(device)


    model = torch.load('./models/Resnet18.pt')

    model.eval()

    # loss_criterion = nn.CrossEntropyLoss()
    # optimizer = optim.Adam(model.parameters(), lr=.0001)
    #
    # running_loss = 0

    num_correct = 0
    num_samples = 0

    # Training & Validation Dataloaders
    val_dl = DataLoader(val_ds, batch_size=batch_size, shuffle=True)

    with torch.no_grad():
        for sample in val_dl:

            img, label = sample
            img, label = img.to(device), label.to(device)

            output = model(img)

            pred_label = torch.argmax(output, dim=1)

            num_correct += (pred_label == label).sum().float()
            num_samples += pred_label.size(0)

        print(f"Accuracy: {float(num_correct)/float(num_samples)}")




if __name__ == "__main__":

    if torch.cuda.is_available():
         print(torch.cuda.get_device_name())

    train_ds, val_ds = prepare_data(val_pct=.15)

    # train(train_ds)

    validation(train_ds)





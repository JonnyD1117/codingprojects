import torch 
import torch.nn as nn 
import torchvision.transforms as transforms
import torch.nn.functional  as F
from torch.utils.data import DataLoader, Dataset, random_split
from torchvision.datasets.mnist import MNIST
import torch.optim as optim

from tqdm import tqdm


class Linear_MNIST_VAE(nn.Module):

    def __init__(self, num_bottleneck_features=16):
        super().__init__()
        self.num_features = num_bottleneck_features

        # VAE Encoder
        self.encoder_1 = nn.Linear(in_features=784, out_features=512)
        self.encoder_2 = nn.Linear(in_features=512, out_features=self.num_features*2)

        # VAE Decoder
        self.decoder_1 = nn.Linear(in_features=self.num_features, out_features=512)
        self.decoder_2 = nn.Linear(in_features=512, out_features=784)

    def reparameterize(self, mu, log_var):

        std = torch.exp(0.5*log_var)
        epsilon = torch.randn_like(std)

        z = mu + (std*epsilon)
        return z

    def forward(self, x):

        # Encoder
        x = F.relu(self.encoder_1(x))
        x = self.encoder_2(x).view(-1, 2, self.num_features)

        # Extracting the Mean and Log Variance
        mu = x[:, 0, :]
        log_var = x[:, 1, :]

        # Reparameterize the by Sampling Normal Distribution with Epsilon
        z = self.reparameterize(mu, log_var)

        # Decoder
        x = F.relu(self.decoder_1(z))
        reconstruction = torch.sigmoid(self.decoder_2(x))
        return reconstruction, mu, log_var


class MNIST_dataset(Dataset):

    def __init__(self):
        super().__init__()

        self.transforms = transforms.Compose([transforms.ToTensor()])

        self.mnist_data = MNIST(root="./mnist_data", transform=self.transforms, download=True)

    def __len__(self):

        return len(self.mnist_data)

    def __getitem__(self, item):

        return self.mnist_data[item]


def train_model(train_dl):
    EPOCHS = 20

    running_loss = 0
    model.zero_grad()
    # Training Loop
    for epoch in range(EPOCHS):
        with tqdm(total=len(train_dl)) as pbar:
            for count, sample in enumerate(train_dl):
                img, label = sample
                img, label = img.to(device), label.to(device)

                output = model(img)

                loss = loss_criterion(output, label.item())

                running_loss += loss/float(count)

                loss.backward()
                optimizer.step()
                model.zero_grad()
                pbar.update()
                pbar.set_description(f'Training Model - Epoch {epoch}, Loss {loss.item(): .4f}, Running_loss {running_loss:.4f}')
    torch.save(model.state_dict(),"./model/VAE_1.pt")


def validate_model(val_dl):
    model.eval()
    for sample in val_dl:
        img, label = sample
        img, label = img.to(device), label.to(device)

        output = model(img)

        loss = loss_criterion(output, label.item())


if __name__ == "__main__":

    device = "cuda:0"

    mnist_ds = MNIST_dataset()

    val_pct = .10
    tot_len = len(mnist_ds)
    val_len = int(tot_len*val_pct)
    train_len = tot_len - val_len

    train_ds, val_ds = random_split(mnist_ds, [train_len, val_len])

    train_dl = DataLoader(train_ds, batch_size=32, shuffle=True, num_workers=4)
    val_dl = DataLoader(val_ds, batch_size=10, shuffle=False, num_workers=4)

    EPOCHS = 20

    # Loss Function
    loss_criterion = nn.BCELoss().to(device)
    # Define Model
    model = Linear_MNIST_VAE().to(device)
    model.train()
    # Define Optimizer
    optimizer = optim.Adam(model.parameters(), lr=1e-3)


    train_model(train_dl)

    validate_model(val_dl)








import torch
import torch.nn as nn
import torch.nn.functional as F

import torch.optim as optim
import torchvision
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms
from tqdm import tqdm

import matplotlib.pyplot as plt

class AutoEncoder(nn.Module):

    def __init__(self):
        super().__init__()

        self.encoder = Encoder()
        self.decoder = Decoder()

    def forward(self, x):
        bottle_neck = self.encoder(x.view(-1, 28*28))
        output = self.decoder(bottle_neck)

        # output = F.sigmoid(output)

        return output.view(-1, 28, 28)


class Encoder(nn.Module):

    def __init__(self, num_input=28*28, num_bottle_neck=128):
        super().__init__()

        # encoder
        self.enc1 = nn.Linear(in_features=784, out_features=256)
        self.enc2 = nn.Linear(in_features=256, out_features=128)
        self.enc3 = nn.Linear(in_features=128, out_features=64)
        self.enc4 = nn.Linear(in_features=64, out_features=32)
        # self.enc5 = nn.Linear(in_features=32, out_features=16)



    def forward(self, x):

        x = F.relu(self.enc1(x))
        x = F.relu(self.enc2(x))
        x = F.relu(self.enc3(x))
        x = F.relu(self.enc4(x))
        # x = F.relu(self.enc5(x))

        return x


class Decoder(nn.Module):

    def __init__(self, num_input=100, num_bottle_neck=28*28):
        super().__init__()

        # decoder
        # self.dec1 = nn.Linear(in_features=16, out_features=32)
        self.dec2 = nn.Linear(in_features=32, out_features=64)
        self.dec3 = nn.Linear(in_features=64, out_features=128)
        self.dec4 = nn.Linear(in_features=128, out_features=256)
        self.dec5 = nn.Linear(in_features=256, out_features=784)

    def forward(self, x):
        # x = F.relu(self.dec1(x))
        x = F.relu(self.dec2(x))
        x = F.relu(self.dec3(x))
        x = F.relu(self.dec4(x))
        x = F.relu(self.dec5(x))

        return x


if __name__ == "__main__":

    # Create/Download Dataset
    train_data = torchvision.datasets.MNIST(root='./data/', download=True, train=True, transform=transforms.Compose([transforms.ToTensor()]))
    val_data = torchvision.datasets.MNIST(root='./data/', download=True, train=False, transform=transforms.Compose([transforms.ToTensor()]))

    # Create DataLoaders
    train_loader = DataLoader(train_data, batch_size=32, shuffle=True, num_workers=4)
    val_loader = DataLoader(val_data, batch_size=10, shuffle=False, num_workers=4)

    gpu = "cuda:0"
    cpu = 'cpu'


    # Create Model:
    model = AutoEncoder()
    model.to(gpu)
    print(model)

    # Define Optimizer
    optimizer = optim.Adam(model.parameters(), lr=.0001)
    # lr_scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=.1)

    # Define Loss Function
    loss_criterion = nn.MSELoss()

    # loss_criterion = nn.KLDivLoss()

    # Training Loop
    EPOCHS = 25

    # print(model.encoder.state_dict())

    model.zero_grad()

   # Iterate Over Number of Epoch
    for epoch in range(EPOCHS):
        # Iterate over Training data
        with tqdm(train_loader) as tepoch:
            for cnt, sample in enumerate(tqdm(train_loader)):

                # tepoch.set_description(f"Epoch {epoch}")

                # Unpack Training Data
                data, label = sample

                data = data.squeeze(dim=1).to(gpu)


                # Pass Data Through NN Model
                output = model(data)

                # Perform Pixelwise Loss Computation on Binary Data
                loss = loss_criterion(output, data).to(gpu)



                if cnt % 1000 == 0:
                    print("Training Loss= ", loss.item())

                # tepoch.set_postfix(loss=loss.item())

                # print(loss)

                # Performance Back-Prop
                loss.backward()

                optimizer.step()
                # lr_scheduler.step()

                # Zero Model Gradients
                model.zero_grad()


    torch.save(model, 'models/mnist_auto_encoder.pt')
    for sample in val_loader:

        test_data, label = sample

        test_output = model(test_data.to(gpu))

        print(test_data.shape)
        print(test_output.shape)

        test_data = test_data.squeeze(dim=1)[0].to(cpu)
        test_output = test_output[0].to(cpu)

        print(test_data.shape)
        print(test_output.shape)
        test_data = test_data.detach().numpy()
        test_output = test_output.detach().numpy()

        plt.figure(0)
        plt.imshow(test_data)
        plt.title("Input Data")

        plt.figure(2)
        plt.imshow(test_output)
        plt.title("Output Data")

        plt.show()







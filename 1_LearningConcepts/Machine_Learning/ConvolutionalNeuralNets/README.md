# Machine-Learning-Practice

## Neural Networks Architecture in Pytorch 

Below is the list of Neural Networks arch. which I am trying to implement from scratch using the Pytorch library. 

- [ ] Feed Forward (FC)
- [ ] Convolutional (CNN)
- [ ] Image Classifier
- [ ] Auto Encoder 
- [ ] Variational Auto Encoder (VAE) 
- [ ] Residual Network (ResNet) 
- [ ] Recurrent Network (RNN) 
- [ ] Long Short Term Memory (LSTM)
- [ ] Gated Recurrent Unit (GRU) 
- [ ] LeNet 
- [ ] VGG 
- [ ] EfficientNet 
- [ ] Yolo 
- [ ] YoloV3
- [ ] Generative Adversarial Network (GAN) 
- [ ] Cycle GAN 

**IDEA:** Generate Matlab code from Latex or handwritten math, output variables or constants required to complete description of problem

import torch
import torch.nn as nn
from torch.optim import optimizer
import torch.optim as optim
import random
import os
import numpy as np
import matplotlib.pyplot as plt
import wandb







class sine_net(nn.Module):

    def __init__(self):
        super().__init__()

        self.input_layer = nn.Linear(in_features=1, out_features=10, bias=True)
        self.hidden1 = nn.Linear(in_features=10, out_features=20, bias=True)

        self.hidden2 = nn.Linear(in_features=20, out_features=10, bias=True)

        self.output_layer = nn.Linear(in_features=10, out_features=1, bias=True)

        self.relu = nn.ReLU(inplace=True)


    def forward(self, x):
        x = self.relu(self.input_layer(x))
        x = self.relu(self.hidden1(x))
        x = self.relu(self.hidden2(x))
        output = self.output_layer(x)

        return output


def train_network(input_vec, label_vec):

    optimizer = optim.Adam(model.parameters(), lr=.001)
    sample = list(zip(input_vec, label_vec))

    loss_func = nn.MSELoss()
    model.train()
    EPOCHS = 25
    for epoch in range(EPOCHS):
        random.shuffle(sample)
        model.zero_grad()
        for counter, samp in enumerate(sample):

            x, y  = samp



            x = x.unsqueeze(dim=0)
            y = y.unsqueeze(dim=0)
            pred_out = model(x.float())
            loss = loss_func(pred_out, y.float())

            wandb.log({"Train Loss": loss})
            if counter % 1000 == 0:
                print(f"Epoch: {epoch} & Iteration: {counter}, Loss: {loss.item()}")
            loss.backward()
            optimizer.step()

    torch.save(model.state_dict(), "./sine_model.pt")
    torch.save(model.state_dict(), os.path.join(wandb.run.dir, 'model.pt'))










def test_model():


    model.load_state_dict(torch.load("./sine_model.pt"))

    x_inputs = np.arange(0, 2 * np.pi, .001)
    x_inputs = torch.from_numpy(x_inputs)

    input_np_list = []
    output_np_list = []

    for x in x_inputs:

        input_np_list.append(x.numpy())

        x = x.unsqueeze(dim=0).float()
        ten_output = model(x)

        ten_sq = ten_output.squeeze(dim=0)
        output_np = ten_sq.detach().numpy()

        output_np_list.append(output_np)


    plt.plot(input_np_list, output_np_list)
    plt.show()



if __name__ == "__main__":
    wandb.init(project="wb_testing")

    model = sine_net()

    wandb.watch(model)

    x_inputs = np.arange(0, 2 * np.pi, .0001)
    y_output = np.sin(x_inputs)

    x_inputs = torch.from_numpy(x_inputs)
    y_output = torch.from_numpy(y_output)

    train_network(x_inputs, y_output)
    #
    # test_input = np.array([0])
    # test_ten = torch.from_numpy(test_input)
    # output = model(test_ten.unsqueeze(dim=0).float())

    test_model()
/*
 * Simple TCP/IP Server Example
 */

#include "socket_headers.hpp"
#include <iostream> 
#include <unistd.h>

#define PORT 8080

int main()
{
    // Create Host Socket
    int socket_fd = socket(AF_INET, SOCK_STREAM, 0);   

    // Define Socket Address Struct
    struct sockaddr_in host_addr, client_addr;

    // Popular Socket Struct
    host_addr.sin_family = AF_INET;
    host_addr.sin_port = htons(PORT);
    host_addr.sin_addr.s_addr = INADDR_ANY;

    // Bind Server Socket to itself Address
    if(bind(socket_fd, (struct sockaddr*)&host_addr, sizeof(host_addr))==0)
    {
        std::cout << "Server BIND successful" << std::endl;
    }


    // Listen for Incoming Connections from clients
    if(listen(socket_fd, 10) == -1 )
    {
        std::cout << "Server LISTENed Failed." << std::endl;
    }
    else{
        std::cout << "Server Listen Successfully" << std::endl;
    }

    int addr_len = sizeof(client_addr);

    int client_socket_fd = accept(socket_fd, (struct sockaddr*)&client_addr, (socklen_t*)&addr_len);

    if(client_socket_fd == -1) { std::cout << "Accept Error" << std::endl; }
    else { std::cout << "Server Accepted Connection" << std::endl; }

    char buffer[1026] = {0};

    // while (1) {
    //     // Read data from the client
        
    // }

    if (recv(client_socket_fd, buffer, sizeof(buffer), 0) <= 0) 
    {
        perror("Receive failed");
    }
    else
    {
        std::cout << "Message From Client Recieved: Breaking Loop" << std::endl; 
    }

    

    for(int i =0; buffer[i] != '\0'; i++)
    {
        std::cout << buffer[i] << std::endl;
    }


    char message[256] = "Server Acknowledges CLIENT message";
    int msg_len = sizeof(message);

    send(client_socket_fd, message, msg_len, MSG_WAITALL);

    std::cout << "Message Sent" << std::endl;


    close(client_socket_fd);
    close(socket_fd);

    std::cout << "Server socket has been closed" << std::endl;



    
    return 0; 
}
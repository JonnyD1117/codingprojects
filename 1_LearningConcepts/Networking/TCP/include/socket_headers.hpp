#include <sys/types.h>      // Supplies TypeDef'ed types 
#include <sys/socket.h>     // Provides access to "socket()" functions/API
#include <netdb.h>          
#include <arpa/inet.h>      // Provides functions IP addresses and manipulation between host machine and network standard. 
#include <netinet/in.h>     // Provides socket address structs 
#include <openssl/ssl.h>    // SSL Provides the encrypted tunnel for HTTP-S connections  

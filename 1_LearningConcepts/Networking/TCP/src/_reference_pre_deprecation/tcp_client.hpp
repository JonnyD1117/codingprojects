#include "socket_headers.hpp"
#include "tcp_errors.hpp"

#include <string>
#include <iostream>
#include <cstring>
#include <unistd.h>
#include <bits/stdc++.h>

#pragma once 


namespace TCP
{

    const int MAX_BUFFER_SIZE = 4096;         // Max Number of Bytes
    
    class Client
    {
        public: 

        Client(std::string ip_address, size_t port) :  ip_address(ip_address), port(port) 
        {
            populate_address_struct();
            CreateSocket(); 
        }

        void CreateSocket(const int num_attempts=3) { create_socket(num_attempts); }

        void Bind()
        {
            // Optional - Bind Client Socket to IP Address and TCP Port
            // NOT IMPLEMENTED
            TcpMethodNotImplemented MethodNotImplemented("Bind Method Has not been Implemented"); 
            throw MethodNotImplemented; 
        }

        void Connect(const int num_attempts=3)
        {
            int connection_status;

            for(int attempts=0; attempts<num_attempts; ++attempts)
            {
                try
                {
                    connection_status = connect(socket_fd, (struct sockaddr*)&server_addr, sizeof(server_addr));

                    if(connection_status == -1) 
                    {   
                        std::cout << "Connection Failed: " << connection_status << std::endl;
                        TcpClientConnectFailed ConnectionFailed("TCP Client Connection Failed: Reattempting now...");
                        throw ConnectionFailed;
                    }
                    break;

                }
                catch(const TcpClientConnectFailed& e)
                {
                    if(attempts == (num_attempts-1))
                    throw std::runtime_error("TCP Client Connection Failed: Attempts at establishing a Connection have failed");
                }   
            }
        }

        void Send(std::string message)
        {    
            message = append_termination_delimiter(message);                                   // Add Deliminator
            size_t message_num_chars = message.length() + 1;                            

            if(message_num_chars <= MAX_BUFFER_SIZE)
            {   
                clear_buffer(send_buffer, MAX_BUFFER_SIZE);                                   // TODO: dynamic buffer size adaptation, E.G> (char msg_buffer[message.length() + 1];)  
                size_t msg_len = message.copy(send_buffer, message_num_chars);                    // NOTE: <string>.length() does not include the NULL (Add +1 to accout for NULL Term) 
                send(socket_fd, send_buffer, msg_len, 0);
            }
            else
            {              
                size_t msg_split_index = 0;                                                  // Index for keeping track of where payload string was split
                size_t num_chars_left = message_num_chars;                                   // Running Tally for how many chars are left to copy into the buffer
                size_t buffer_size = MAX_BUFFER_SIZE;
                size_t num_sends = sends_required(message_num_chars, MAX_BUFFER_SIZE);  
                
                for(int i=0; i < num_sends; ++i)
                {
                    // NOTE: TCP Just needs to send all of the bytes across the network, TCP will keep track of the ordering. 
                    clear_buffer(send_buffer, MAX_BUFFER_SIZE);
                    
                    // Determine the number of characters to be copied to the buffer. 
                    buffer_size = (num_chars_left > MAX_BUFFER_SIZE) ? MAX_BUFFER_SIZE : num_chars_left; 
                    size_t msg_len = message.copy(send_buffer, buffer_size, msg_split_index); 

                    // Book Keeping for Splitting of Message into Buffer sized chunks
                    num_chars_left -= msg_len; 
                    msg_split_index += MAX_BUFFER_SIZE; 

                    // Send the Data over the socket
                    send(socket_fd, send_buffer, msg_len, 0);
                }
            }
        }

        std::string Receive()
        {   
            // Uniform Size Data Buffer w/ Null Termination delimiter
            size_t bytes_read;                                          // Number of Bytes Read at each `recv()`
            std::string received_msg;                                   // String representation of entire message
            size_t delimiterPos;                                        // Position of Null Terminated parsed from received buffer

            // Loop Until Null Terminator Delimiter has been detected. 
            while (true) { 
                bytes_read = 0; 
                bytes_read = recv(socket_fd, recv_buffer, MAX_BUFFER_SIZE, 0);

                if (bytes_read <= 0) {
                    // Handle connection closure or error
                    break;
                }

                // Load received char[] into string for processsing
                std::string msg_buffer_str(recv_buffer, bytes_read);

                // Test if Null Terminator Exists Inside of String
                delimiterPos = msg_buffer_str.find('\0');

                // Detect Null Terminator Delimiter 
                if(delimiterPos == std::string::npos)
                {
                    // Null Terminator Not Received Keep Receiving Client Data.
                    received_msg.append(msg_buffer_str);
                    msg_buffer_str.clear();
                    clear_buffer(recv_buffer, sizeof(MAX_BUFFER_SIZE));
                }
                else 
                {
                    // Null Terminator Received Handle & stop receiving from client
                    received_msg.append(msg_buffer_str, 0, delimiterPos);
                    break;
                }
            }
            return received_msg;
        }

        void Close()
        {
            int status = shutdown(socket_fd, SHUT_RDWR); 

            if(status == 0)
            {
                close(socket_fd);
            }
        }
        private:

        // Socket Variables 
        int socket_fd; 
        size_t port;
        std::string ip_address;
        struct sockaddr_in server_addr ;
        char send_buffer[MAX_BUFFER_SIZE]; 
        char recv_buffer[MAX_BUFFER_SIZE];

        // Heartbeat Methods
        size_t connection_timeout_Ms = 35000;      // Connection will timeout and throw and exception connection time exceeds this time.   
        size_t heart_beat_duration = 1000;        // Heat Beat Duration in Milliseconds 
        

        /****************************
         * Private Class Memeber
         ****************************/ 

        void create_socket(int num_attempts) 
        { 
            for(int attempt_num=0; attempt_num < num_attempts; ++attempt_num)
            {
                try
                {
                    // Attempt to Create the Socket
                    socket_fd = socket(AF_INET, SOCK_STREAM, 0); 
                    if(socket_fd == -1) { throw TcpClientSocketCreationFailed("TCP Client Socket Creation returned Error"); }
                    break;
                }

                catch(TcpClientSocketCreationFailed& error)
                {   
                    if(attempt_num == (num_attempts -1)) 
                    { 
                        std::string attempt_error = "TCP Client Socket Creation was ReAttempted & failed!";
                        TcpClientSocketCreationReAttemptFailed ReAttemptFailed( attempt_error.c_str());  
                        throw ReAttemptFailed; 
                    }
                }
                catch(TcpClientSocketCreationReAttemptFailed& error)
                {
                    throw std::runtime_error(error.what());
                }
            }                               
        }

        void populate_address_struct()
        {
            char* ip_addr_char = strdup(ip_address.c_str());
            server_addr.sin_family = AF_INET;
            server_addr.sin_port = htons(port);
            server_addr.sin_addr.s_addr = inet_addr(ip_addr_char);
        }
    
        void establish_heartbeat()
        {

        }


        /*************************** 
         *     Utility Methods
         **************************/
        void clear_buffer(char* buffer, size_t buffer_len)  
        { 
            for(int i=0; i< buffer_len; ++i) buffer[i] = 0;
        }

        size_t sends_required(size_t num_chars_in_message, size_t buffer_size)
        {
            if(num_chars_in_message % buffer_size == 0)
            {
                return (num_chars_in_message / buffer_size);
            }
            return ((num_chars_in_message / buffer_size) + 1);
        }

        std::string append_termination_delimiter(std::string message) 
        {  
            message += '\0'; 
            return message ; 
        }
    };
}
# Computer Networking 
The objective of projects in this directory are to learn about the fundamental construction and design of networked computer systems, as well as to learn how to implement the standard networking c api (sockets, protocols ...etc.) that provide the fundamental abstraction for how humans directly interact with networking concepts. 

These concepts include... 

1. Low-level design and implementation of TCP/UDP Clients & servers
2. Design and implementation of HTTP and how to use it to create web servers and clients 
3. Learn the [ZeroMQ Networking Library](https://zeromq.org/)
4. Implement higher-level networking abstractions of TCP & HTTP in languages like C++ 
5. Write several basic networking projects (from scratch) where desired task to be completed requires the cooperation of two or more machines across a network. 
#include <iostream> 

class SwitchCaseFSMBase
{   
    protected:

    bool terminated = false; 

    public: 

    // SwitchCaseFSMBase() = default;
    // ~SwitchCaseFSMBase()= delete; 
    
    virtual void state_logic_hook() = 0;            // Pure Virtual Function Hook REQUIRES implementation 


    void Terminate() { terminated = true; }         // Call to Terminate the FSM

    void Run()
    {
        while(!terminated)
        {
            state_logic_hook();                     // Function will loop until terminate is called
        }
    }
};
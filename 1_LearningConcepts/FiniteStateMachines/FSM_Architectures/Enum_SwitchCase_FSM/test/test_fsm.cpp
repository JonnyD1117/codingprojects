#include <iostream> 
#include <chrono>
#include "BasicSwitchCaseFSM.hpp"

enum states
{
    green, 
    yellow, 
    red,
    flashing_red,
    terminate
}; 





class MyFSM : public SwitchCaseFSMBase
{

    virtual void state_logic_hook() override; 

};


void MyFSM::state_logic_hook()
{
    // Define Initial State 
    states current_state; 
    
    current_state = states::red;

    for(int i =0; i < 10; ++i)
    {
        switch(current_state) {
        case states::red:
            std::cout << "The Traffic Sign is: RED" << std::endl;

            // Red -> Green
            current_state = states::green;
            break;
        case states::yellow:
            std::cout << "The Traffic Sign is: YELLOW" << std::endl;

            // Yellow -> Red
            current_state = states::red;
            break;

        case states::green:
            std::cout << "The Traffic Sign is: GREEN" << std::endl;

            // Green -> Yellow
            current_state = states::yellow;

        // default:
        //     std::cout << "The Traffic Sign is: FLASHING RED" << std::endl;
        //     current_state
    }

    }

    // Terminate the FSM Loop
    Terminate();
}




int main()
{   
    std::cout << "Hello From FSM Test" <<std::endl;

    MyFSM trafficLight; 

    trafficLight.Run();

    return 0;
}
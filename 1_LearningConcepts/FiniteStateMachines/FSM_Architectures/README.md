# Finite State Machine Architectures & Design Implementations

One of the most interesting things about Finite State Machines is the fact that it is a simplified **model** of computation. This means the details of it's implementation are completely up for interpretation by the FSM designer. 

## Enum & Switch-Case FSMs
This is one of the most simple and straight forward FSMs to code. The `state` of the FSM is encoded into an Enumerated Type (e.g. Enum). The main mechanism for switch states in this design is to place a switch-case statement (possibly nest switch-case) inside of an infinite loop. 

Given an initial state, the loop will be entered and the current state of the FSM will be updated according the state transition rules as encoded by the switch-case statement(s).

It should be noted that in this implementation the definition of the `state` and any logic or action that needs to be performed in that state are "decoupled". Since the enum, IS the state and the switch-case performs the logic. There is a very loose coupling between states and their logic/actions. 

This loose coupling along with the complexity of coding a large FSM using nest switch cases means that this type of FSM is better suited for smaller more intuitive systems that just need to a quick and easy solution. 


## State Pattern (OOP) FSMs

The State Pattern is an Object Oriented approach to implementing a FSM that using polymorphism to go from one state to another. Each state object inherits from a common abstract (virtual) state class. Each 'concrete' state implementation must implement a `handle` method that performs any actions required within that state. Additionally a `context` object is required to manage the mechanism of initializing and updating the states. 

This pattern is perfectly fine; however, from an implementation point of view, EVERY state of the system needs to implement a class. For large FSM this leads to a large explosion of code required to implement, even though the concept of the State Pattern is very simple. 

Additionally, like every OOP construct there is an added overhead to this type of system which might make it unacceptable for embedded applications were resources and computer are highly constrained. 

This type of state-machine has a very tight coupling between its "state-ness" and its action/logic as the implementation of the each state is encapsulated into its out class, and then each class is responsible for implementing its out `handling` logic. This means that the relationship between the state and the actions or logic it needs to perform are theoretically inseperable.

## Table of Function Pointers & Enums FSMs

Unlike, the Enum/Switch-case FSM, this design of FSM implements a state-transition table, with state enums and/or event/input enums that index into a table of function-pointers. 

In this design, each state is encoded into a enum and the logic or actions required to be performed by this state are stored inside of a function who has a function-pointer stored in the state-transition table. 

While this design doesn't inherently better couple the state and the action logic, the construct of the transition table as a data structure of function pointers is a more general concept that scales with inputs, states, and complexity far better than the enum/switch-case FSM does. 

This type of FSM is a fairly standard FSM implementation for applications written in the C-programming language. 

## Function-based FSMs 

Function-based state machines are a class of FSM that embed the state of the system and the action/logic that state needs to transition into a single function. This is typically implemented as the current state of the system "is" the function-pointer to that given state. The nice feature of this type of FSM is the high coupling between state and action. The state itself is encoded into the function-pointer while the actions that state needs to perform are explicitly written in the body of the function. 

Unlike the function-pointer state-transition table FSM, this function-based FSM does not need an explicitly constructed transition table. The currently state of the system is provided as a function pointer to the initial state, and the context mechanism of the FSM will simply call that function pointer and that state function will return a function pointer to the next state. 

This implementation of state machine has a high coupling between its `state-ness` and its action logic seeing as both of these are encapsulated into the same function. I personally like this implementation as the process of creating a state machine is less boiler-plate code heavy (unlike the State Pattern FSM) and can be implemented using an FSM class that manages the run context and executation of each state, while the state machine transition logic is directly encoded inside of each member function. 

This type of state machine can be adapted for asynchronouse/event driven FSM designs as well. 

## Hierarchical FSMs 

Hierarchical FSMs are state machines in which each state can itself be a state machine. While this construct is incredibly power in allowing developers to create highly expressive and customized state machines, it comes at the cost of added base complexity in the FSM design and implementation. 
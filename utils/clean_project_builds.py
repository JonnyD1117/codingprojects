#!/usr/bin/env python3

import os 
import subprocess 

PROJECT_ROOT = os.getcwd()

def traverse_file_system():

    dir_list = list()

    # Search for ".gitclean" files
    for( root, dirs, files) in os.walk(PROJECT_ROOT, topdown=True):
        if '.gitclean' in files:
            dir_list.append(root)

    # Call "make clean" on all directories
    for dir in dir_list:
        os.chdir(dir)
        print(dir)
        result = subprocess.run([f"make clean"], shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        os.chdir(PROJECT_ROOT)


if __name__ == '__main__':
    traverse_file_system()

# CodingProjects
This repository is a collection of micro-projects that I have or want to build, but that do not merit the creation of their own repos. Most of the projects contained are didactic in nature and never meant for serious development or production and should never be used in those environments. 

The secondary goal of this repo is to provide a single location that functions more or less as a portfolio of my smaller coding projects. 

## Coding Languages 
The coding languages that I am most familiar with are C++, C#, & Python. Most of the code found in this repository will be written in one of these languages. 

## Repository Structure 
This repo is broken into the following sections... 

### Learning Concepts 
This directory contains micro-projects related (more or less) to fundamental Computer Science concepts. The intent of the projects within this directory are to understand computer science through practice by direct implementation. These projects will never be production ready or polished and will often provide redudant functionality to tools or libraries that are widely used in industry. Projects under this diectory are not intended to be used "long-term" but more of proof of understanding and as a practical reference.

### Application Specific 
This directory contains projects that are fall under a specific domain or application purview. The objective of these projects are to go beyond the basic computer science fundamentals and instead to build the algorithms, structures, systems, tools, and libraries required within a specific domain or application. In a way, these projects are similar the projects within the `LearningConcepts` directory with the exception that instead of implementing basic CS concepts, these projects implement the basic concepts of the domain that they are a part of. (e.g. Learning how to implement template matching within the domain of Computer Vision). The a primary characteristic of these projects are that while they are usually far more complex, they are still more for learning and exploration of application specific coding.

### Stand-Alone Projects 
This directory contains larger/long term projects that I want to develop, maintain, and work on but do not require the scope and creation of a separate git repository. This directory can be thought of as a contain for libraries, tools, algorithms, and software that I might actually want to use/support for larger projects I have. These projects (while interesting) do not merit there own repo or perhaps are the first steps of projects that will eventually transition into standalone repos

### Libraries 
This directory contains libraries or versions of projects that I want to incorportate into other projects directly with as little friction as possible This could take the form of Python Packages, Linux Distro Packages (.deb || .rpm), or could even be as simple as CMake exporting a previous projects library targets. 
#include <iostream>
#include "FEA_from_Scratch/primitive_geometries/geometry.h"
#include <vector>
#include <string>
//#include <matplot/matplot.h>
#include <cmath>

#include "cmake-build-debug/_deps/matplotlib-cpp/matplotlibcpp.h"

namespace plt = matplotlibcpp;


int main() {

    using namespace FEA_geo;


//    Point<int> p0(1,2,3);
//    Point<int> p1(4,5,6);
//    Point<int> p2(7,8,9);
//    Point<int> p3(10,11,12);
//    Point<int> p4(13,14,15);

    Point<int> p0(2,6,8);
    Point<int> p1(8,3,1);
    Point<int> p2(7,8,9);
    Point<int> p3(10,11,12);
    Point<int> p4(13,14,15);

    Edge<int> e0(p0, p1);
    Edge<int> e1(p1, p2);
    Edge<int> e2(p2, p3);
    Edge<int> e3(p3, p4);

    List<Point<int>> p_list;


    p_list.Append(p0);
    p_list.Append(p1);
    p_list.Append(p2);
    p_list.Append(p3);
    p_list.Append(p4);


    p_list.plot_list();

//    std::vector<int> x {0,1};
//    std::vector<int> y {0,1};
//    std::vector<int> z {0,1};
//
//    plt::plot3(x, y, z);
//    plt::xlabel("X-Axes");
//    plt::ylabel("X-Axes");
//    plt::title("This is a Title");
//    plt::show();



    return 0;
}

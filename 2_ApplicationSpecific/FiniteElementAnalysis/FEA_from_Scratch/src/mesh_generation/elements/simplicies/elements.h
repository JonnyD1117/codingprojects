//
// Created by jonathan on 5/11/22.
//

#ifndef BOWYER_WATSON_ELEMENTS_H
#define BOWYER_WATSON_ELEMENTS_H


template<typename T>
class Triangle {
public:
    Point<T> p0, p1, p2;
    Edge<T> e0, e1, e2;

    Triangle() = default;

    Triangle(Point<T> p0, Point<T> p1, Point<T> p2) : p0(p0), p1(p1), p2(p2), e0(p0, p1), e1(e1, e2), e2(e2, e0) {}
};

template <typename  T>
class Tetrahedron{

};

template <typename T>
class Quad{

};

template<typename T>
class Triangulation
{
private:
    std::vector<Triangle<T>> tri_list;
public:
    Triangulation() = default;
    Triangulation(std::vector<Triangle<T>> input_triangulation)
    {
        tri_list = input_triangulation;
    }

    // Print Edges to Screen

    // Append Triangle to Triangulation

    // Index into
};


#endif //BOWYER_WATSON_ELEMENTS_H

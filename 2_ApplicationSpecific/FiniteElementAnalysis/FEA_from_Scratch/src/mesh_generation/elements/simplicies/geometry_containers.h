//
// Created by jonathan on 5/3/22.
//

#ifndef BOWYER_WATSON_GEOMETRY_CONTAINERS_H
#define BOWYER_WATSON_GEOMETRY_CONTAINERS_H

#include <iostream>
#include <vector>
#include "../../../cmake-build-debug/_deps/matplotlib-cpp/matplotlibcpp.h"



class geometry_containers {

    template<typename T>
    class List
    {

    private:
        std::vector<T> list_var;

    public:
        List() = default;
        List(std::vector<T> input_vec) : list_var(input_vec){}

        void Append(T element)
        {
            list_var.push_back(element);
        }

        void RemoveAt(int index)
        {
            typename std::vector<T>::iterator it = this->list_var.begin();

            if(index < 0)
            {
                this->list_var.erase(this->list_var.end());
            }
            else {
                it += index;
                this->list_var.erase(it);
            }
        }
        T PopAt(int index)
        {
            T rm_pt = this->list_var[index];
            this->RemoveAt(index);
            return rm_pt;
        }

        T operator [] (int const &index)
        {
            return this->list_var[index];
        }

        void print_list()
        {

            for(int i =0; i< this->list_var.size(); i++)
            {
                std::cout<<  "<" + std::to_string(this->list_var[i].x) + ", " + std::to_string(this->list_var[i].y) + ", "+ std::to_string(this->list_var[i].z) + ", " + ">" << std::endl;
            }

        }

        void plot_list()
        {
            std::vector<int> x;
            std::vector<int> y;
            std::vector<int> z;

            for(int i=0; i< this->list_var.size(); i++)
            {
                T temp = this->list_var[i];
                x.push_back(temp.x);
                y.push_back(temp.y);
                z.push_back(temp.z);
            }

            plt::plot3(x, y, z);
            plt::xlabel("X-Axes");
            plt::ylabel("Y-Axes");
            plt::title("This is a Title of the Thing");
            plt::show();

        }
    };

};


#endif //BOWYER_WATSON_GEOMETRY_CONTAINERS_H

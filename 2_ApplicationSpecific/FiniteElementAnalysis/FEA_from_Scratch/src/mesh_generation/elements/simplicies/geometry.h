
#ifndef GEOMETRY_H
#define GEOMETRY_H

#include <fstream>
#include <vector>
#include <list>
#include <string>
#include <iterator>
#include <map>

namespace simplicies {


    template<typename T>
    class Point {
    public:
        T x, y, z;

        Point() {};

        Point(T x, T y, T z) : x(x), y(y), z(z) {}

        Point<T> operator+(Point<T> const &rhs) {
            Point p;
            p.x = this->x + rhs.x;
            p.y = this->y + rhs.y;
            p.z = this->z + rhs.z;
            return p;
        }
    };

    template<typename T>
    class Edge {
    private:
        Point<T> pStart, pEnd;
        std::pair<Point<T>, Point<T>> edge;
    public:
        Edge() = default;

        Edge(Point<T> p0, Point<T> p1) : pStart(p0), pEnd(p1), edge(p0, p1) {}

        std::pair<Point<T>, Point<T>> getEdgePoints() {
            return edge;
        }
    };

    template <typename T>
    class Triangle{
    private:
    public:


        // Default Constructor

    };


    template <typename T>
    class Tetrahedron{
    private:
    public:

    };


}


#endif //GEOMETRY_H

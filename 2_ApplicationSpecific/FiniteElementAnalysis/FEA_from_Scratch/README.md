# FEA From Scratch Project 

As a mechanical engineer, I have always been interested in solving practical engineering problems with computers & software. One of the most popular methods for accomplishing this is the use of Finite Element Methods to solve partial & ordinary differential equations for a variety of problem domains. 

## FEA From Scratch 


### Mesh Generation 

### GUI Visualization 

### FEA Solver 

### Handling Input Models


## Mesh Generation

Mesh generation is the process of deconstructing a domain into a mesh of elements. The purpose of this mesh is typically used for the purpose of implementing Finite Element Analysis (FEA). FEA requires the discretization of a domain such that governing equations which define the behavior of a phenomenon can be numerically approximated in a tractable fashion. 

The category of mesh which we desire to create are known as Delauny Triangulations. Meshes in this category possess special properties which are considered desirable. 
There are numerous types of meshes and mesh generation algorithms which are commonly used for 2d or 3d meshes all implemented in different ways. 

This project will use the Incremental Insertion method of mesh generation strategy which incrementally generates a Delauny Triangulation by inserting points within the boundary of the domain, and then enforces certain relations between the newly inserted point and the existing edges of the mesh. If necessary these edges might be removed and new edges constructed which follow the desired properties.  

1. Define Mesh Format 
2. Generate Input Data from mesh file 
3. Compute Convex Hull of 2d & 3d boundaries
4. Apply Bowyer-Watson Algorithm to create Constrained Delauny Triangulation (CDT)  in 2 & 3D
5. Plot results
6. Measure element quality 
7. Refine Mesh 

## Finite Element Analysis
The immediate goal of this project is to perform simple stress analysis of a simple geometry under a given loading and with given boundary conditions. However a more broad implemention would include support for several domains of numerical analysis (electrical, mechanical, eletromagnetic, acoustic...etc)
To accomlish this, it is my opinion that Galerkin's Method would be a suitable starting point as its structure is not tied directly to the domain of study like more direct FEA models and methods. 


1. Implement Galerkin's Method of Finite Elements over Generated Mesh 
2. Add Convergence functionality to show that the solution of the solver is asymptotically approaching a solution


## GUI Design 
 As I would like the final form of this project to be more than a merely commandline interface, a GUI would be a nice way of visually packaging all of the information, options, and capabilities of this package into a single unit. 

The goal is to use DearIMGUI (c++ gui library) and inside of it to display each section of the FEA process in a seperate tab (like Comsol/Ansys) and to allow the user to provide graphical inputs to the system instead of just coding from the command line. However a commandline interface would probably be useful as well. 



## Optimizations 

The first optimization I would like to make to this project is to implement meaningful data strcutures which improve searching through the mesh and performing the mesh generation. 

Secondly, I would like to use CUDA and parallel process the mesh generation and solver to improve performance


# FEA_from_Scratch

The objective of this repo is to learn the fundamentals of the techniques and programming required to perform basic FEA on simple 2D & 3D solid/homegeous parts. 

## GOALS 
The primary goal of this repo is to solve steady and transient solutions for the 2D/3D Heat Conduction/Convection for a rod, and the 2D/3D stress analysis for steel plate with a hole in the center, under load. 


## RoadMap 

- [ ] Import geometry from CAD model 
- [ ] Generate Mesh 
- [ ] Apply Loads & Boundary Conditions 
- [ ] Use Galerkin's Method of Weighted Residules to setup up FEA problem 
- [ ] Compute Iterative Solution (for large structures) 


## Import Geometry 

1. Read CAD file 
2. Translate CAD file into a geometric description which simplifies usage latter 
3. Export translated geometry file 

## Mesh Generation 

1. Implement algorithm for determination of the Convex Hull of Point Sets in both 2D & 3D
2. Implement Fortune's Algorithm for Voronoi Diagrams 
3. Implement Algorithms for Delaunay Triangulation in 2d & 3D 
4. Implmenent improved Triangulation algorithm for fitting to the geometry boundary
5. Implement Mesh Refinement Algorithm for improving existing mesh with element Quality/Curvature measures 

Mesh Types to implement Triangular, Tetrahedral, Quad elements.

## FEA Setup 

1. Apply Loads & Boundary Conditions 
2. Apply Galerkin's Method of Weighted Residuals 
3. 

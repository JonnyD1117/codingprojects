import numpy as np
from Points import Point2D
import random
import matplotlib.pyplot as plt

from time import sleep


class ConvexHull(Point2D):

    def __init__(self):

        super().__init__()

    def GenerateRandomPointSet(self, num_points=25000, max_val=25, min_val=-25):

        # P = [(random.randint(min_val, max_val), random.randint(min_val, max_val)) for p in range(num_points)]

        # P = [(random.uniform(min_val, max_val), random.uniform(min_val, max_val)) for p in range(num_points)]

        P = [(random.uniform(min_val, max_val), random.uniform(min_val, max_val)) for p in range(num_points)]

        A = []

        for x, y in P:

            if (1/25)**2*x**2 + (1/4)*y**2 <= 1:
                A.append((x, y))

        return A

        # return P

    def PlotPointSet(self, Point_set):

        x_data = [ Point_set[i][0] for i in range(len(Point_set))]
        y_data = [ Point_set[i][1] for i in range(len(Point_set))]

        plt.scatter(x_data, y_data)
        plt.xlabel("X Axis")
        plt.ylabel("Y Axis")
        plt.title("Plot of Point Set")
        plt.show()

    def DirectionOfLine(self, Origin: Point2D, Dest: Point2D, Test: Point2D):

        """
        :param Origin:
        :param Dest:
        :param Test:
        :return: 1 if to the right -1 if to the left
        """

        Ox = Origin.x
        Oy = Origin.y

        Dx = Dest.x
        Dy = Dest.y

        Tx = Test.x
        Ty = Test.y

        Ax = Dx - Ox
        Ay = Dy - Oy

        Bx = Tx - Ox
        By = Ty - Oy

        Det = Ax*By - Ay*Bx

        direction = (1 if Det>0 else -1)

        return direction




        print("Hell Direction of Line")

    def ComputeSlowConvexHull(self, Point_set):
        """
        This function takes the existing Point Set and Return the vertices of the Point Set which define the Convex Hull.
        The function returns the list of vertices in clockwise-order

        :param Point_set:
        :return: CH Set
        """

        edge_list = []

        ordered_pair = []

        for i in range(len(Point_set)):
            for j in range(len(Point_set)):

                if i == j:
                    continue

                else:
                    ordered_pair.append((Point_set[i], Point_set[j]))


        # print(ordered_pair)

        for p, q in ordered_pair:

            valid = True

            if p == q:
                continue

            for r in Point_set:

                if r == p or r == q:
                    continue

                R = Point2D(r[0], r[1])
                P = Point2D(p[0], p[1])
                Q = Point2D(q[0], q[1])

                point_direction = self.DirectionOfLine(P, Q, R)

                if point_direction == -1:        # Point is to the LEFT

                    valid = False

            if valid:

                edge_list.append((p, q))

        return edge_list

    def ComputeFastConvexHull(self, Point_set):

        L_Hull = []

        sorted_point_set = Point_set   # Sort Point set by X-coordinate
        sort_func = lambda point: point[0]  # Define LAMBDA function to obtain x value for each point
        sorted_point_set.sort(key=sort_func)    # Sort the Point Set by X Value


        lower_sorted_point_set = Point_set
        lower_sorted_point_set.sort(key=sort_func, reverse=True)
        L_upper_hull = self.ComputeUpperHull(sorted_point_set)
        L_lower_hull = self.ComputeLowerHull(lower_sorted_point_set)


        L_Hull = [L_upper_hull[i] for i in range(len(L_upper_hull))]

        # print(F"Upper Hull: {L_upper_hull}")
        # print(f"Lower Hull: {L_lower_hull}")

        # del L_lower_hull[0]
        # del L_lower_hull[-1]

        L_lower_hull.sort(key=sort_func, reverse=True)    # Sort the Point Set by X Value




        # print(f"Lower Hull After Deletion: {L_lower_hull}")

        for i in range( len(L_lower_hull)):

            L_Hull.append(L_lower_hull[i])

        return L_Hull, L_upper_hull, L_lower_hull





        # L_upper.append(sorted_point_set[0])     # Append First element of X sorted List to L_upper
        # L_upper.append(sorted_point_set[1])     # Append Second elemnt of X sorted list to L_upper
        #
        # for i in range(2, len(sorted_point_set)):       #
        #
        #     L_upper.append(sorted_point_set[i])
        #
        #     while len(L_upper) > 2 and self.ThreePointTurn(L_upper[-3:]) == 1:
        #
        #         del L_upper[-2]
        #
        # return L_upper

    def ThreePointTurn(self, last_three_points):

        p1 = last_three_points[0]
        p2 = last_three_points[1]
        p3 = last_three_points[2]

        P1 = Point2D(p1[0], p1[1])
        P2 = Point2D(p2[0], p2[1])
        P3 = Point2D(p3[0], p3[1])

        turn_dir = -1*self.DirectionOfLine(P1, P2, P3)     # 1 = RIGHT & -1 = LEFT

        return turn_dir

    def ComputeUpperHull(self, sorted_point_set):

        print(f"Sorted Point Set: {sorted_point_set}")

        L_upper = []  # Define Empty list for Upper Hull
        L_upper.append(sorted_point_set[0])  # Append First element of X sorted List to L_upper
        L_upper.append(sorted_point_set[1])  # Append Second elemnt of X sorted list to L_upper

        # P = sorted_point_set
        #
        # x_point = [P[i][0] for i in range(len(P))]
        # y_point = [P[i][1] for i in range(len(P))]


        for i in range(2, len(sorted_point_set)):  #

            L_upper.append(sorted_point_set[i])

            # x_upper = [L_upper[i][0] for i in range(len(L_upper))]
            # y_upper = [L_upper[i][1] for i in range(len(L_upper))]
            #
            # fig1 = plt.figure(1)
            #
            # plt.scatter(x_point, y_point)
            # plt.scatter(x_upper[-1], y_upper[-1], c='yellow')
            # plt.plot(x_upper, y_upper)
            # plt.show()

            del_x = -20
            del_y = -20

            while len(L_upper) > 2 and self.ThreePointTurn(L_upper[-3:]) == -1:
                del_x = L_upper[-2][0]
                del_y = L_upper[-2][1]
                del L_upper[-2]

            # fig1 = plt.figure(1)
            # x_upper = [L_upper[i][0] for i in range(len(L_upper))]
            # y_upper = [L_upper[i][1] for i in range(len(L_upper))]
            # plt.scatter(x_point, y_point)
            # plt.scatter(del_x, del_y, c='red')
            # plt.plot(x_upper, y_upper)
            # plt.show()

        return L_upper

    def ComputeLowerHull(self, sorted_point_set):

        L_lower = []  # Define Empty list for Upper Hull
        L_lower.append(sorted_point_set[0])  # Append First element of X sorted List to L_upper
        L_lower.append(sorted_point_set[1])  # Append Second elemnt of X sorted list to L_upper

        for i in range(2, len(sorted_point_set)):  #

            L_lower.append(sorted_point_set[i])

            while len(L_lower) > 2 and self.ThreePointTurn(L_lower[-3:]) == 1:
                del L_lower[-2]

        return L_lower


if __name__ == "__main__":

    Thing = ConvexHull()

    P = Thing.GenerateRandomPointSet()

    l_hull, upper, lower = Thing.ComputeFastConvexHull(P)
    print(f"Convex Hull {l_hull}")
    print(f"Upper Hull {upper}")
    print(f"Lower Hull {lower}")

    x_hull = [l_hull[i][0] for i in range(len(l_hull))]
    y_hull = [l_hull[i][1] for i in range(len(l_hull))]

    x_upper = [upper[i][0] for i in range(len(upper))]
    y_upper = [upper[i][1] for i in range(len(upper))]

    x_lower = [lower[i][0] for i in range(len(lower))]
    y_lower = [lower[i][1] for i in range(len(lower))]


    # edges = Thing.ComputeSlowConvexHull(P)
    # print(edges)

    x_point = [P[i][0] for i in range(len(P))]
    y_point = [P[i][1] for i in range(len(P))]


    plt.figure()
    plt.scatter(x_point, y_point)
    plt.plot(x_upper, y_upper)
    plt.plot(x_lower, y_lower)
    # plt.plot(x_hull, y_hull)
    plt.show()



    # plt.figure()
    # plt.scatter(x_point, y_point)
    # plt.plot(x_hull, y_hull)
    # plt.show()

    # plt.figure
    # plt.scatter(x_point, y_point)
    #
    # for edge in edges:
    #     x_edge_point = [edge[i][0] for i in range(len(edge))]
    #     y_edge_point = [edge[i][1] for i in range(len(edge))]
    #
    #     plt.plot(x_edge_point, y_edge_point)
    #
    # plt.show()


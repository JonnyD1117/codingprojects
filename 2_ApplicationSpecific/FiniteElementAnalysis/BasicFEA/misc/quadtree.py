"""
Author: Jonathan Dorsey
Date: 10/3/2021
Purpose: Learn Quadtree Meshing
"""

import numpy as np
import matplotlib.pyplot as plt

"""
1) Import Surface Boundary 
2) Discretize Surface Boundary 
3) Compute Convex Hull 
4) Build Quadtree structure and expansion logic 
5) Create Initial Bounding Box 
6) Analyze Number of points in each quadrant of bounding box 
7) Discretize in quadrant as needed 
8) Repeat
"""


if __name__ == '__main__':

    pass
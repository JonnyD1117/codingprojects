from enum import Enum
import matplotlib.pyplot as plt
import numpy as np
import math
import json
from collections import Counter


class PlanarElementTypes(Enum):
    TRIANGULAR = 1
    QUADRILATERAL = 2


class Point2D:

    def __init__(self, x, y):
        self.x = x
        self.y = y


class Planar_FE_Boundary:
    def __init__(self, right_hand=True, element_type=PlanarElementTypes.TRIANGULAR):
        self.right_hand_coord = right_hand

        self.vertex_list = []
        self.edge_list = []

    def compute_nearest_edge(self, point):

        min_edge_dist = np.infty
        min_edge_ind = None

        for edge_ind, edge in enumerate(self.edge_list):
            edge_to_point_dist = self.compute_edge_to_point_dist(point, edge)

            if edge_to_point_dist < min_edge_dist:
                min_edge_dist = edge_to_point_dist
                min_edge_ind = edge_ind

        return self.edge_list[min_edge_ind]

    @staticmethod
    def compute_signed_area(point, from_edge_point, to_edge_point):

        px = point.x
        py = point.y
        Ax = from_edge_point.x
        Ay = from_edge_point.y
        Bx = to_edge_point.x
        By = to_edge_point.y

        matrix = np.array([[px, py, 1], [Ax, Ay, 1], [Bx, By, 1]])

        signed_area = .5*np.linalg.det(matrix)

        return signed_area

    def compute_edge_to_point_dist(self, point, edge):
        '''
        Computes perpendicular dist between the given point and edge
        '''
        px = point.x
        py = point.y

        A = edge[0]
        B = edge[1]

        AB_dist = self.compute_point_to_point_dist(A, B)
        AP_dist = self.compute_point_to_point_dist(A, point)

        ap_x = A.x - px
        ap_y = A.y - py

        u_x, u_y = self.compute_unit_vect_components(A, B)

        alpha = ap_x*u_x + ap_y*u_y

        if alpha < 0:
            return AP_dist
        elif alpha > AB_dist:
            return self.compute_point_to_point_dist(B, point)

        return (AP_dist - alpha)**2

    @staticmethod
    def compute_point_to_point_dist(p2, p1):
        """
        Computes the point-to-point distance
        """
        return math.sqrt((p2.x - p1.x)**2 + (p2.y - p1.y)**2)

    def compute_unit_vect_components(self, B, A):
        """
        Compute the Components of the Unit Vector in Cartesian Frame
        """
        a_x = A.x
        b_x = B.x
        a_y = A.y
        b_y = B.y

        ab_dist = self.compute_point_to_point_dist(B, A)

        u_x = (b_x - a_x)/ab_dist
        u_y = (b_y - a_y)/ab_dist

        return u_x, u_y

    def plot_boundary(self, point=None, arrows=False, lines=True, nearest_edge=False):

        if point is None:

            x_vals = [self.edge_list[i][0].x for i in range(len(self.edge_list))]
            y_vals = [self.edge_list[i][0].y for i in range(len(self.edge_list))]

            plt.scatter(x_vals, y_vals)
            plt.title("FE Boundary")
            plt.xlabel('X Axis')
            plt.ylabel('Y Axis')
            plt.show()

        else:
            to_node, from_node = self.compute_nearest_edge(point)

            print(f"Signed Area = {self.compute_signed_area(point, from_node, to_node)}")

            x_vals = [self.edge_list[i][0].x for i in range(len(self.edge_list))]
            y_vals = [self.edge_list[i][0].y for i in range(len(self.edge_list))]

            if nearest_edge:
                plt.plot([to_node.x, from_node.x], [to_node.y, from_node.y])
            plt.scatter(x_vals, y_vals)
            plt.scatter(point.x, point.y)
            plt.title("FE Boundary")
            plt.xlabel('X Axis')
            plt.ylabel('Y Axis')
            plt.show()

    def load_sample_boundary(self):
        point_A = Point2D(1, 3)
        point_B = Point2D(2, 8)
        point_C = Point2D(4, 9)
        point_D = Point2D(9, 7.5)
        point_E = Point2D(8, 1.25)
        point_F = Point2D(5.5, .75)
        point_G = Point2D(2.75, 0)

        self.edge_list = [(point_A, point_B),
                          (point_B, point_C),
                          (point_C, point_D),
                          (point_D, point_E),
                          (point_E, point_F),
                          (point_F, point_G),
                          (point_G, point_A)]

    def is_point_in_boundary(self, point):

        to_node, from_node = self.compute_nearest_edge(point)
        signed_area = self.compute_signed_area(point, from_node, to_node)

        if signed_area > 0:
            return True
        elif signed_area <0:
            return False
        else:
            raise Exception('ON Boudary')


class Box:
    def __init__(self, init_center):
        self.bb_corner_points = []
        self.init_center_point = None

    def create_bounding_box(self):
        pass

    def set_bounding_box(self, corner_list):
        self.bb_corner_points = corner_list

    def find_bb_center(self):
        BL_point = self.bb_corner_points[0]
        TR_point = self.bb_corner_points[3]

        min_x = BL_point.x
        min_y = BL_point.y
        max_x = TR_point.x
        max_y = TR_point.y

        bb_center_x = .5*(max_x - min_x)
        bb_center_y = .5 * (max_y - min_y)
        bb_center = Point2D(bb_center_x, bb_center_y)
        return bb_center

    def create_sub_quad_corner_point(self, center_point):


        pass

    def create_sub_quadrants(self, center_point):
        pass

    def find_quadrant_centers(self):
        pass

    def create_quadrant(self):
        pass


class Quadtree(Box):
    def __init__(self):
        # super(Quadtree, self).__init__()
        self.boundary_edge_list = None

    def import_boundary(self, new_edge_list):
        self.boundary_edge_list = new_edge_list

    def create_initial_domain(self):

        min_x = np.infty
        min_y = np.infty
        max_x = -np.infty
        max_y = -np.infty

        scale = 1.5

        for edge in self.boundary_edge_list:
            point_x = edge[0].x
            point_y = edge[0].y

            if point_x < min_x:
                min_x = point_x

            if point_x >= max_x:
                max_x = point_x

            if point_y < min_y:
                min_y = point_y

            if point_y >= max_y:
                max_y = point_y

        if min_x >= 0 and min_y >= 0:
            min_scaled_x = 0
            min_scaled_y = 0
            max_scaled_x = scale * max_x
            max_scaled_y = scale * max_y
        else:
            min_scaled_x = scale * min_x
            min_scaled_y = scale * min_y
            max_scaled_x = scale * max_x
            max_scaled_y = scale * max_y

        point_A = Point2D(min_scaled_x, min_scaled_y)             # Bottom Left
        point_B = Point2D(max_scaled_x, min_scaled_y)             # Bottom Right
        point_C = Point2D(min_scaled_x, max_scaled_y)             # Top Left
        point_D = Point2D(max_scaled_x, max_scaled_y)             # Top Right

        return point_A, point_B, point_C, point_D


if __name__ == '__main__':

    # point = Point2D(1, 5)
    point = Point2D(1, 6)
    boundary = Planar_FE_Boundary()
    boundary.load_sample_boundary()
    boundary.plot_boundary(point, nearest_edge=True)


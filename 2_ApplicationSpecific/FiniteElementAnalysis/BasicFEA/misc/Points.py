class Point2D:

    def __init__(self, x_val=None, y_val=None):

        self.x = x_val
        self.y = y_val


class Point3D:

    def __init__(self, x_val=None, y_val=None, z_val=None):

        self.x = x_val
        self.y = y_val
        self.z = z_val

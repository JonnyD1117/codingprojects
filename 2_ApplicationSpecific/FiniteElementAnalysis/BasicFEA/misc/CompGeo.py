import numpy as np
import matplotlib.pyplot as plt


class Point:

    def __init__(self, x=None, y=None):

        self.x = x
        self.y = y
        # self.z = z
        self.p_list = [x, y]
        self.ctr = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.ctr < len(self.p_list):
            p = self.p_list[self.ctr]
            self.ctr += 1
            return p
        else:
            raise StopIteration

    def get_point(self):
        _point = (self.x, self.y)
        return _point


class Edge:
    def __init__(self, p1: Point, p2: Point):
        self._edge = [p1, p2]
        self.ctr = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.ctr < 2:
            e = self._edge[self.ctr]
            self.ctr += 1
            return e
        else:
            raise StopIteration


class Triangle:
    def __init__(self, p1: Point, p2: Point, p3: Point, E=None, P=None):
        """ Note Triangle is define clockwise, from left_bottom point
                        P2
                       /  \
                     /     \
                   /_ _ _ __\
                  P1         P3
        """
        if E is None and P is None:
            self._p1 = p1
            self._p2 = p2
            self._p3 = p3

        else:
            for indx, point in enumerate(E):
                if indx == 1:
                    self._p1 = point
                if indx == 2:
                    self._p3 = point
            self._p2 = P

        self._e1 = Edge(p1, p2)
        self._e2 = Edge(p2, p3)
        self._e3 = Edge(p3, p1)
        self._edge_list = [self._e1, self._e2, self._3]
        self._tri_points = [p1, p2, p3]
        self.ctr = 0

    def __iter__(self):
        return self

    def __next__(self):
        if self.ctr < len(self._edge_list):
            e = self._edge_list[self.ctr]
            self.ctr += 1
            return e
        else:
            raise StopIteration

    def get_edges(self):
        return self._edge_list

    def get_points(self):
        return self._tri_points


class Triangulation:
    def __init__(self):
        self._triangulation = []
        self._idx = 0

    def __iter__(self):
        return self

    def __next__(self):
        self._idx += 1
        return self._triangulation[self._idx]

    def append_triangle(self, t: Triangle):
        self._triangulation.append(t)


class Polygon:

    def __init__(self):
        self._edge_list = []
        self.ctr

    def __iter__(self):
        return self

    def __next__(self):
        if self.ctr < len(self._edge_list):

            e = self._edge_list[self.ctr]
            self.ctr += 1
            return e
        else:
            raise StopIteration

    def append_edge(self, e: Edge):
        self._edge_list.append(e)


class BowyerWatsonAlgo:

    def __init__(self):
        self._triangulation = Triangulation()
        self.create_super_triangle()

    def create_super_triangle(self, p1=None, p2=None, p3=None):

        if p1 is not None and p2 is not None and p3 is not None:
            tri = Triangle(p1, p2, p3)
            self._triangulation.append_triangle(tri)

    def check_circumcircle(self, point, tri):
        # Check whether point is in the circumcirle of the triangle
        pass

        return True or False

    def check_shared_triangle_edge(self, list_of_triangles, edge):
        # if edge is not shared by any other triangles in badTriangles
        # add
        # edge
        # to
        # polygon
        pass
        return True or False

    def compute_DT(self, pointList):

        for point in pointList:
            bad_triangles = []

            for triangle in self._triangulation:

                check = self.check_circumcircle(point, triangle)

                if check is True:
                    bad_triangles.append(triangle)
            polygon = Polygon()
            for triangle in bad_triangles:
                for edge in triangle:
                    check_shared = self.check_shared_triangle_edge(bad_triangles, edge)

                    if check_shared is True:
                        polygon.append_edge(edge)

                # Remove triangle from badTriagles
                bad_triangles.remove(triangle)

            for edge in polygon:
                newTri = Triangle(E=edge, P=point)

        for tri in self._triangulation:
            pass
    #     if triangle contains a vertex from original super-triangle remove triangle from triangulation





if __name__ == "__main__":
    p = Point(12, 52)

    for i in p:
        print(i)

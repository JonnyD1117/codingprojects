//
// Created by 15107 on 9/28/2022.
//

#ifndef INC_6502_C_COMPILER_STATEMACHINE_H
#define INC_6502_C_COMPILER_STATEMACHINE_H

#include <functional>
#include <iostream>
#include <queue>

namespace FSM
{
    using namespace std;

    enum Events
    {
        INIT = 0,
        IDLE,
        WHITESPACE,
        SEMICOLON
    };




    class StateMachine {

        /// State Type alias
        using state_t = void(Events&);

        queue<Events> event_queue;
        static  function<state_t> state;

        static void return_state(function<state_t> stateFunc);
        void push_event(Events& event);
        void handle_event();
        void unhandled_event(Events& event);

    public:
        void StateEngine();

         void state_init(Events& event);
         void state_whitespace(Events& event);
         void state_idle(Events& event);
         void state_semicolon(Events& event);
    };
}




#endif //INC_6502_C_COMPILER_STATEMACHINE_H

//
// Created by Indy-Windows on 9/10/2022.
//
#include <iostream>
#include <fstream>
#include "Fridman.h"
#include <algorithm>
#include <exception>


namespace SimpleCCompiler{

    using namespace std;

    vector<string> Fridman::extract_keyword(string path) {

        string keywords_string;
        ifstream file;
        file.open(path);

        stringstream strStream;
        strStream << file.rdbuf();
        keywords_string = strStream.str();

        istringstream ss(keywords_string);
        string temp;
        vector<string>keyword_vec;
        while(ss>>temp){
            keyword_vec.push_back(temp);
        }

        return keyword_vec;
    }


}



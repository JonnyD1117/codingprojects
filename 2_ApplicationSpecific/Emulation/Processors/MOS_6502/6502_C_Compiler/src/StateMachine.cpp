//
// Created by 15107 on 9/28/2022.
//

#include "StateMachine.h"


namespace FSM
{
    using namespace std;



        void StateMachine::return_state(function<state_t> stateFunc)
        {
            state = stateFunc;
        }

        void StateMachine::push_event(Events& event)
        {
            event_queue.push(event);
        }

        void StateMachine::handle_event()
        {
            event_queue.pop();
        }

        void StateMachine::unhandled_event(Events& event)
        {
            cout << "hello"<< endl;
        }




        void StateMachine::StateEngine()
        {
            cout << "hello"<< endl;
        }

        void StateMachine::state_init(Events& event)
        {
            if ( event == Events::INIT)
            {
                cout << "hello init" << endl;
                return_state(state_whitespace);
            }

            else if (event == Events::WHITESPACE)
            {
                return_state(state_semicolon);
            }

            else if (event == Events::WHITESPACE)
            {
                return_state(state_semicolon);
            }

            else
            {
                unhandled_event(event);
                return_state(state_init);
            }
        }

        void StateMachine::state_whitespace(Events& event);
        void StateMachine::state_idle(Events& event);
        void StateMachine::state_semicolon(Events& event);

}

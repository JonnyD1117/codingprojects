#pragma once

#include <set>

namespace fsm
{
    using namespace  std;

    set<char> lower_alpha = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'a', 'a', 'a', 'a', 'a', 'a'};
    set<char> upper_alpha = {};
    set<char> numeric = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
    set<char> punctuation = {'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'a'};

}
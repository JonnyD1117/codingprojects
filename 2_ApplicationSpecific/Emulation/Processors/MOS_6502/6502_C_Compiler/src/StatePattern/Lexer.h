#pragma once

// Standard Library
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <optional>

// Local Includes
#include "LexerStates.h"
#include "Token.h"

namespace fsm
{
    using namespace std;

    class Lexer
    {
        bool run_flag = false;
        bool is_eof = false;
        class IState *current;
        fstream in_file;
        char current_event;

        void process(bool flag);



        void request();

        optional<char> get_next_character();

        Token run_fsm(IState* state, Token& tok);

    public:

        Lexer() = default;

        Token get_token();

        void setCurrent(IState *s);

        void set_input_file(string& if_path);
    };
}


//#include <iostream>
//using namespace std;
//class Machine
//{
//    class State *current;
//public:
//    Machine();
//    void setCurrent(State *s)
//    {
//        current = s;
//    }
//    void on();
//    void off();
//};
//
//class State
//{
//public:
//    virtual void on(Machine *m)
//    {
//        cout << "   already ON\n";
//    }
//    virtual void off(Machine *m)
//    {
//        cout << "   already OFF\n";
//    }
//};

//int main()
//{
//    void(Machine:: *ptrs[])() =
//            {
//                    Machine::off, Machine::on
//            };
//    Machine fsm;
//    int num;
//    while (1)
//    {
//        cout << "Enter 0/1: ";
//        cin >> num;
//        (fsm. *ptrs[num])();
//    }
//}

#include "LexerStates.h"
#include "Lexer.h"
#include "Token.h"

#include <cctype>
#include <cwctype>

namespace fsm {

    void Lparen::handle(Lexer *e, char event) {

    }
    void Rparen::handle(Lexer *e, char event) {

    }
    void Lbracket::handle(Lexer *e, char event) {

    }
    void Rbracket::handle(Lexer *e, char event) {

    }
    void Lcurly::handle(Lexer *e, char event) {

    }
    void Rcurly::handle(Lexer *e, char event) {

    }
    void semicolon::handle(Lexer *e, char event) {

        // GOTO SEMICOLON

    }

    void Asterisk::handle(Lexer *e, char event) {
        if (isalpha(event) || isdigit(event) || ispunct(event)) // execpt for *
        {
            Asterisk star;
            e->setCurrent(&star);
        }

        if
    }
    void ForwardSlash::handle(Lexer *e, char event) {
        if (event == '*')
        {

        }

        else if (event == '/')
        {

        }


    }
    void EOL::handle(Lexer *e, char event) {

    }
    void comment::handle(Lexer *e, char event) {

    }
    void multiline_comment::handle(Lexer *e, char event) {

    }
    void integer_literal::handle(Lexer *e, char event) {

    }
    void double_literal::handle(Lexer *e, char event) {

    }
    void float_literal::handle(Lexer *e, char event) {

    }
    void character::handle(Lexer *e, char event) {

    }
    void string_literal::handle(Lexer *e, char event) {

    }
    void keyword::handle(Lexer *e, char event) {

    }
    void assignment::handle(Lexer *e, char event) {

    }
    void greater_than::handle(Lexer *e, char event) {

    }
    void less_than::handle(Lexer *e, char event) {

    }
    void shift_left::handle(Lexer *e, char event) {

    }
    void shift_right::handle(Lexer *e, char event) {

    }
    void lt_equal::handle(Lexer *e, char event) {

    }
    void gt_equal::handle(Lexer *e, char event) {

    }
    void equality::handle(Lexer *e, char event) {

    }
    void not_equal::handle(Lexer *e, char event) {

    }
    void not_operator::handle(Lexer *e, char event) {

    }
    void increment::handle(Lexer *e, char event) {

    }
    void decrement::handle(Lexer *e, char event) {

    }

    void fullstop::handle(Lexer *e, char event) {
        if (isspace(event))
        {
            // GO TO DOUBLE Literal
        }
        else if (isdigit(event))
        {
            isDigit digit;
            e->setCurrent(&digit);
        }
    }

    void isAlphaNumeric::handle(Lexer *e, char event)
    {
        if (isdigit(event) || isalpha(event))
        {
            isAlpha alpha;
            e->setCurrent(&alpha);
        }

        else if (isspace(event))
        {
            return; // IDENTIFIER
        }
    }

    void isAlpha::handle(Lexer *e, char event) {


    }
    void isDigit::handle(Lexer *e, char event) {

        if (isdigit(event))
        {
            isDigit digit;
            e->setCurrent(&digit);
        }

        else if (isspace(event))
        {
            // GOTO INTEGER LITERAL
        }

        else if (event == '.')
        {
            fullstop period;
            e->setCurrent(&period);
        }

        else if (event == 'f')
        {
            // GOTO FLOAT
        }

    }

    void WhiteSpace::handle(Lexer *e, char event) {

        if (event == '('){
            Lparen lparen;
            e->setCurrent(&lparen);
        }

        else if (event == '/'){
            ForwardSlash fslash;
            e->setCurrent(&fslash);
        }

        else if (event == ')'){
            Rparen rparen;
            e->setCurrent(&rparen);
        }

        else if (event == '['){
            Lbracket lbracket;
            e->setCurrent(&lbracket);
        }

        else if (event == ']'){
            Rbracket rbracket;
            e->setCurrent(&rbracket);
        }

        else if (event == '{'){
            Rcurly rcurly;
            e->setCurrent(&rcurly);
        }

        else if (event == '}'){
            Lcurly lcurly;
            e->setCurrent(&lcurly);
        }

        else if (event == ';'){
            semicolon semi;
            e->setCurrent(&semi);
        }

        else if (event == ';'){
            semicolon semi;
            e->setCurrent(&semi);
        }

        else if (isalpha(event)){
            isAlpha alpha;
            e->setCurrent(&alpha);
        }

        else if (isdigit(event)){
            isDigit digit;
            e->setCurrent(&digit);
        }
    }
}

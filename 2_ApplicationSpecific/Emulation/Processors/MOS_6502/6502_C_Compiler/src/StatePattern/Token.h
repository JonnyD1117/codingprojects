#pragma once


namespace fsm
{
    using namespace std;

    enum TokenEnums
    {
        WHITESPACE = 0,
        INTEGER_LITERAL,
        DOUBLE_LITERAL,
        FLOATING_POINT,
        OPERATOR,
        KEYWORD,
        IDENTIFIER,
        ALPHA_NUMERIC,
        PUNCTUATION,
        ALPHA,
        NUMERIC,
        SPACE,
        PLUS_OP,
        MINUS_OP,
        MULTI_OP,
        DIV_OP,
        MOD_OP,
        INCR_OP,
        DECR_OP,
        SEMICOLON,
    };

    struct Token
    {
        TokenEnums token = WHITESPACE;
        string token_value = "";

        void set_token_type(TokenEnums tok)
        {
            token = tok;
        }

        void append_token_value(optional<char> tok_char)
        {
            if (tok_char.has_value())
            {
                token_value += tok_char.value();
            }
        }
    };
}


//
// Created by Indy-Windows on 10/15/2022.
//
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include "Lexer.h"
#include "LexerStates.h"
#include "Token.h"


namespace fsm
{
    void Lexer::process(bool flag)
    {
        this->run_flag = flag;
    }

    void Lexer::setCurrent(IState *s){
        current = s;
    }

    void Lexer::request() {
        current->handle(this, this->current_event);
    }

    Token Lexer::run_fsm(IState *state, Token& token)
    {
        this->process(true);
        this->current = state;

        while (this->run_flag)
        {
            if (is_eof){ break; }
            optional<char> next_char = this->get_next_character();

            if (!in_file.eof() && next_char.has_value())
            {
                this->current_event = next_char.value();
            }
            else
            {
                if (in_file.eof())
                {
                    cout << "-----END OF FILE MOTHER FUCKER" << endl;
                    is_eof = true;
                }
            }

            // Traverse the FSM
            request();

            // Start in the initial state provided
            // Walk the FSM until a terminal symbol occurs and a complete token can be generated
            // Generate Token
            // Return Token
            // AND exit
        }

        return token;
    }

    Token Lexer::get_token()
    {
        Token token;                                    // Create a new Empty Token
        WhiteSpace def_state;                           // Create Instance of WhiteSpace State
        run_fsm(&def_state, token);                     //

        return token;
    }
    optional<char> Lexer::get_next_character() {
        char x;
        if (in_file.eof()) {
            return { };
        }
        else {
            in_file >> x;
            return x;
        }
    }

    void Lexer::set_input_file(string& if_path)
    {
        // Set the file stream for the src file to be compiled.
        this->in_file.open(if_path);
    }


}


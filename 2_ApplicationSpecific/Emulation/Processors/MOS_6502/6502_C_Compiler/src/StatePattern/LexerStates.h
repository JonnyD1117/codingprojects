#pragma  once

#include "Lexer.h"
#include "LexerSets.h"
#include <set>

namespace fsm {

    class Lexer;        // Forward Declaration

    class IState {

    public:
        virtual void handle(Lexer *e, char event) = 0;

    };

    class WhiteSpace : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class Lparen : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class Rparen : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class Lbracket : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class Rbracket : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class Lcurly : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class Rcurly : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class semicolon : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class fullstop : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class EOL : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class comment : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class multiline_comment : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class integer_literal : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class double_literal : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class float_literal : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class ForwardSlash : public IState
    {
    public:
        void handle(Lexer *e, char event);
    };
    class Asterisk : public  IState
    {
    public:
        void handle(Lexer *e, char event);
    };

    class character : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class string_literal : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class keyword : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class assignment : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class greater_than : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class less_than : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class shift_left : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class shift_right : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class gt_equal : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class lt_equal : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class equality : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class not_equal : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class not_operator : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class increment : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class decrement : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class isAlpha : public IState {
    public:
        void handle(Lexer *e, char event);

    };


    class isDigit : public IState {
    public:
        void handle(Lexer *e, char event);

    };

    class isAlphaNumeric : public IState {
    public:
        void handle(Lexer *e, char event);

    };

}
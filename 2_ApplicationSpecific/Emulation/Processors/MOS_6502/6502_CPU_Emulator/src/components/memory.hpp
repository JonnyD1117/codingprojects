
#include <cassert>
#include "native_types.hpp"

/*
----------------------------------------------------------------------------------------
|                          6502 Memory (Random Access Memory)                          |
----------------------------------------------------------------------------------------
The 6502 has a 16-bit "address" bus & a 8-bit "data" bus. It can address 64K (65,535) 
unique bytes of memory (when fully populated - not required) but inside each "slot" of 
memory it can only store a 8-bit value (e.g. a byte).
_______________________________________________________________________________________

1) Mininum Required RAM ~ 8k
2) [0x0000 - 0x00FF] "Zero-Page"
3) [0x0100 - 0x01FF] "The Stack" 
----------------------------------------------------------------------------------------
*/

namespace MOS_6502
{
    class  Memory
    {
        public: 
        void Initialize()                        // Initialize entire memory to 0
        {
            for( u32 i=0; i < MAX_MEM; i++) // NOLINT(modernize-loop-convert)
            {
                Data[i] = 0;
            }
        }

        Byte operator[](u32 address) const      // Define Operator for indexing into Mem STRUCT directly
        {
            assert(address < MAX_MEM);
            return Data[address];
        }

        Byte& operator[](u32 address)           // Define Operator for setting values of Memory
        {
            assert(address < MAX_MEM);
            return Data[address];
        }

        void operator()(u32 address, Byte data)
        {
            assert(address < MAX_MEM);
            Data[address] = data;
        }

        private: 
        static constexpr u32 MAX_MEM = 65535;           // Maximum Memory Size ( 2^(16) -1 )
        Byte Data[MAX_MEM] = {0};                       // Allocate memory data of size (MAX_MEM) to zeros
    };
}
#include <cassert>
#include <iostream>
#include "memory.hpp"
#include "instruction_set.hpp"


#pragma once

namespace MOS_6502
{

struct CPU : Instruction_Set
{
    // Define 6502 Internal Registers
    Word PC;        // Declare Program Counter
    Word SP;        // Declare Stack Pointer
    Byte A;         // Declare Accumulator
    Byte X;         // Declare Index Reg. X
    Byte Y;         // Declare Index Reg. Y
    Byte P;         // Declar Processor Register

    // Define Processor Status Reg as Bit Fields
    Byte Z : 1;     // Zero Page Flag
    Byte C : 1;     // Carry Bit Flag
    Byte I : 1;     // Interrupt Disable
    Byte D : 1;     // Decimal Mode Flag
    Byte B : 1;     // Break Command
    Byte V : 1;     // Overflow Flag
    Byte N : 1;     // Negative Flag

    using Mem = Memory;

    void Set_Negative_Flag(Byte result)
    {
        /// Check Negative Flag
        ((result & 0b10000000) > 0) ?  N = 1 : N =0;
    }
    void Set_Overflow_Flag(Word result)
    {
        /// Check Overflow Flag
        (result > 127) ? V = 1: V = 0;
    }
    void Set_Break_Flag(Byte instr)
    {
        /// Check Break Flag
        (instr == INS_BRK_IM) ? B = 1 : B = 0;
    }

    void Set_Zero_Flag(Word result)
    {
        /// Check Zero Flag
        (result == 0) ? Z = 1: Z = 0;
    }
    void Set_Carry_Flag(Byte result)
    {   // "Result" is the output of some addition or subtraction
        /// Check Carry Flag
        C = (result > 127 && V == true);                    // Compute Carry Flag
    }

    Byte FetchByte(Mem& memory, u32& Cycles)
    {
        Byte mem_data = memory[PC];         // Get byte of data at the current PC addr in Memory
        PC++;                               // Increment Program Counter
        Cycles--;                           // Fetch Operation takes one Cycle
        return mem_data;                    // Return the data obtained from PC addr of memory
    };

    Byte ReadByte(Mem& memory, Byte Address ,u32& Cycles)
    {
        Byte mem_data = memory[Address];         // Get byte of data at the current PC addr in Memory
        Cycles--;                           // Fetch Operation takes one Cycle
        return mem_data;                    // Return the data obtained from PC addr of memory
    };

    Word FetchWord(Mem& memory, u32& Cycles)
    {
        Word mem_low_byte = memory[PC];
        PC++;
        Cycles-=1;

        Word mem_high_byte = memory[PC];
        PC++;
        Cycles-=1;

        Word address = (mem_high_byte << 8) + mem_low_byte;

        return address;
    };

    Word FetchWord_X(Mem& memory, u32& Cycles)
    {
        Word PC_w_offset_x = PC + X;
        Byte mem_data = memory[PC_w_offset_x];
        PC++;
        Cycles-=2;
        return mem_data;
    };

    Word FetchWord_Y(Mem& memory, u32& Cycles)
    {
        Word PC_w_offset_x = PC + Y;
        Byte mem_data = memory[PC_w_offset_x];
        PC++;
        Cycles-=2;
        return mem_data;
    };

    Byte ReadWord(Mem& memory, Word Address, u32& Cycles)
    {
        Byte mem_data = memory[Address];
        Cycles--;
        return mem_data;
    };

    void Reset(Mem& memory)
    {
        PC = 0x00FF;        // Initialize Program Counter at (0xfffc)
        SP = 0xFFFF;        // Initialize Stack Pointer to (0x0100)

        C = Z = I = D = B = V = N = 0; // Clear all Processor Status Reg Flags
        A = X = Y = P = 0;      // Initialize All Interal Registers to 0
        memory.Initialize();
    }

    void Execute(Mem& memory, u32 Cycles)
    {
        while( Cycles > 0 )
        {
            Byte next_instr = FetchByte(memory, Cycles);                // Fetch Byte at current PC address -> Opt Code

            switch (next_instr)                                         // Depending on Instruction SWITCH into correct instruction
            {
                case INS_ADC_IM:
                {
                    // Add with Carry
                    // OpCode: 0x69
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]

                    /// Perform Instruction Operation
                    Byte value = FetchByte(memory, Cycles);
                    Word result  = A + value + C;

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag( result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;
                    break;
                }
                case INS_ADC_ZP: {
                    // Add with Carry (zero-page)
                    // OpCode: 0x65
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

//                    ASSERT_GE(zero_page_addr, 0);
//                    ASSERT_LT(zero_page_addr, 256);

                    Byte value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Word result = A + value + C;                               // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag( result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ADC_ZP_X: {
                    // Add with Carry (zero-page, X)
                    // OpCode: 0x75
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]

                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    Byte zp_x_addr = zero_page_addr + X;                        // Offset Zero page address by X reg value
                    Cycles--;                                                   // Operation Takes 1 Cycle????
                    Byte value = ReadByte(memory, zp_x_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)
                    Word result = A + value + C;                                // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ADC_ABS:
                {
                    // Add with Carry - Absolute Addressing
                    // OpCode: 0x6D
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte value = memory[abs_addr];
                    Word result = A + value + C;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ADC_ABS_X:
                {
                    // Add with Carry - Abs, X
                    // OpCode: 0x7D
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_x_offset = abs_addr + X;
                    Byte value = memory[addr_w_x_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A + value + C;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ADC_ABS_Y:
                {
                    // Add with Carry - Abs, Y
                    // OpCode: 0x79
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_y_offset = abs_addr + Y;
                    Byte value = memory[addr_w_y_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A + value + C;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ADC_INDR_X:
                {
                    // Add with Carry - (IND, X)
                    // OpCode: 0x61
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_x = indirect_addr + X;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Byte value = ReadByte(memory, offsetAddr_x, Cycles);
                    Word result = A + value + C;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ADC_INDR_Y:
                {
                    // Add with Carry - (IND, Y)
                    // OpCode: 0x71
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 5*  [1 opcode, 1 Fetch ZP addr, 1 Fetch Y-reg, 1 Fetch (zp_add + Y), 1 Operate?   ]
                    // * Cycle++ if crossing page boundary

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    Word dir_addr = ReadWord(memory, indirect_addr, Cycles);

                    Word offset_addr = dir_addr + Y;

                    Byte value = ReadByte(memory, offset_addr, Cycles);

                    Word result = A + value + C;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_AND_ACCUM:
                {
                    // AND (Immediate)
                    // OpCode: 0x29
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]

                    /// Perform Instruction Operation
                    Byte value = FetchByte(memory, Cycles);
                    Word result  = A & value;

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;
                    break;
                }
                case INS_AND_ZP:
                {
                    // AND (zero-page)
                    // OpCode: 0x25
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    Byte value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Word result = A & value;                               // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_AND_ZP_X:
                {
                    // AND (zero-page, X)
                    // OpCode: 0x35
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]

                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    Byte zp_x_addr = zero_page_addr + X;                        // Offset Zero page address by X reg value
                    Cycles--;                                                   // Operation Takes 1 Cycle????
                    Byte value = ReadByte(memory, zp_x_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)
                    Word result = A & value;                                // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_AND_ABS:
                {
                    // AND - Absolute Addressing
                    // OpCode: 0x2D
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte value = memory[abs_addr];
                    Word result = A & value;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_AND_ABS_X:
                {
                    // AND - Abs, X
                    // OpCode: 0x3D
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_x_offset = abs_addr + X;
                    Byte value = memory[addr_w_x_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A & value;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_AND_ABS_Y:
                {
                    // AND - Abs, Y
                    // OpCode: 0x39
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_y_offset = abs_addr + Y;
                    Byte value = memory[addr_w_y_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A & value;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_AND_INDR_X:
                {
                    // Add with Carry - (IND, X)
                    // OpCode: 0x21
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_x = indirect_addr + X;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Byte value = ReadByte(memory, offsetAddr_x, Cycles);
                    Word result = A & value;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_AND_INDR_Y:
                {
                    // AND - (IND, Y)
                    // OpCode: 0x31
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 5*  [1 opcode, 1 Fetch ZP addr, 1 Fetch Y-reg, 1 Fetch (zp_add + Y), 1 Operate?   ]
                    // * Cycle++ if crossing page boundary

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    Byte dir_addr = ReadByte(memory, indirect_addr, Cycles);
                    dir_addr += Y;
                    Byte value = ReadByte(memory, dir_addr, Cycles);
                    Word result = A & value;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ASL_ACC:
                {
                    // ASL (Accumulator)
                    // OpCode: 0x0A
                    // Number of Bytes: 1
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]

                    /// Perform Instruction Operation
                    Byte result  = A << 1;

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);
                    (A & 0b10000000) ? C = 1: C = 0;
                    /// Set Registers as Appropriate
                    A = result;
                    break;
                }
                case INS_ASL_ZP:
                {
                    // ASL (ZeroPage)
                    // OpCode: 0x06
                    // Number of Bytes: 2
                    // Number of Cycles: 5  [1 opcode, 1 Fetch byte literal]

                    Byte zero_page_Addr = FetchByte(memory, Cycles);
                    Byte zero_page_contents = ReadByte(memory, zero_page_Addr, Cycles);

                    /// Perform Instruction Operation
                    Byte result  = zero_page_contents << 1;
                    Cycles--;

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);
                    (zero_page_contents & 0b10000000) ? C = 1: C = 0;
                    /// Set Registers as Appropriate
                    memory[zero_page_Addr] = result;
                    Cycles--;
                    break;
                }
                case INS_ASL_ZP_X:
                {
                    // ASL (ZeroPage,X)
                    // OpCode: 0x16
                    // Number of Bytes: 2
                    // Number of Cycles: 6  [1 opcode, 1 Fetch byte literal]

                    Byte zero_page_Addr = FetchByte(memory, Cycles);
                    Word zero_page_offset = zero_page_Addr + X;
                    Cycles--;
                    Byte zero_page_X_incr_contents = ReadByte(memory, zero_page_offset, Cycles);

                    /// Perform Instruction Operation
                    Byte result  = zero_page_X_incr_contents << 1;
                    Cycles--;

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);
                    (zero_page_X_incr_contents & 0b10000000) ? C = 1: C = 0;
                    Cycles--;
                    /// Set Registers as Appropriate
                    memory[zero_page_Addr] = result;
                    break;
                }
                case INS_ASL_ABS:
                {
                    // ASL (ABS)
                    // OpCode: 0x0E
                    // Number of Bytes: 3
                    // Number of Cycles: 6  [1 opcode, 1 Fetch byte literal]

                    Word abs_addr = FetchWord(memory, Cycles);
                    Byte abs_addr_contents = ReadWord(memory, abs_addr, Cycles);

                    /// Perform Instruction Operation
                    Byte result  = abs_addr_contents << 1;
                    Cycles--;

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);
                    (abs_addr_contents & 0b10000000) ? C = 1: C = 0;
                    /// Set Registers as Appropriate
                    memory[abs_addr] = result;
                    Cycles--;
                    break;
                }
                case INS_ASL_ABS_X:
                {
                    // ASL (ABS, X)
                    // OpCode: 0x1E
                    // Number of Bytes: 3
                    // Number of Cycles: 7  [1 opcode, 1 Fetch byte literal]

                    Word abs_addr = FetchWord(memory, Cycles);
                    Word abs_addr_w_x_offset = abs_addr + X;
                    Cycles--;
                    Byte abs_addr_w_x_contents = ReadWord(memory, abs_addr_w_x_offset, Cycles);

                    /// Perform Instruction Operation
                    Byte result  = abs_addr_w_x_contents << 1;
                    Cycles--;

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);
                    (abs_addr_w_x_contents & 0b10000000) ? C = 1: C = 0;
                    /// Set Registers as Appropriate
                    memory[abs_addr] = result;
                    Cycles--;
                    break;
                }
                case INS_BCC_REL:
                {
                    // BCC (Relative)
                    // OpCode: 0x90
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Byte value = FetchByte(memory, Cycles);
                    PC--;
                    if(C == 1)
                    {
                        // If Carry Flag is set Take NO Action
                        break;
                    }
                    else
                    {
                        (value & 0b10000000) ? PC-= value : PC+=value;
                        break;
                    }
                }
                case INS_BCS_REL:
                {
                    // BCS (Relative)
                    // OpCode: 0xB0
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Byte value = FetchByte(memory, Cycles);
                    PC--;
                    if(C == 0)
                    {
                        // If Carry Flag is not set Take NO Action
                        break;
                    }
                    else
                    {
                        (value & 0b10000000) ? PC-= value : PC+=value;
                        break;
                    }
                }
                case INS_BEQ_REL:
                {
                    // BEQ (Relative)
                    // OpCode: 0xF0
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Byte value = FetchByte(memory, Cycles);
                    PC--;
                    if(Z == 0)
                    {
                        // If Carry Flag is not set Take NO Action
                        break;
                    }
                    else
                    {
                        (value & 0b10000000) ? PC-= value : PC+=value;
                        break;
                    }
                }
                case INS_BIT_ZP:
                {
                    // BIT Compare memory bits to Accumulator (zero-page)
                    // OpCode: 0x24
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    Byte cmpr_value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A & 0b01000000 ) ? V = 1 : V = 0;
                    (A && cmpr_value) ? Z = 1 : Z = 0;                                       // Set Accumulator equal to result
                    break;
                }
                case INS_BIT_ABS:
                {
                    // BIT Compare memory bits to Accumulator - Absolute Addressing
                    // OpCode: 0x2C
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte cmpr_value = memory[abs_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A & 0b01000000 ) ? V = 1 : V = 0;
                    (A && cmpr_value) ? Z = 1 : Z = 0;                                       // Set Accumulator equal to result
                    break;
                }
                case INS_BMI_REL:
                {
                    // BMI (Relative)
                    // OpCode: 0x30
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Byte value = FetchByte(memory, Cycles);
                    PC--;
                    if(N == 0)
                    {
                        // If Carry Flag is not set Take NO Action
                        break;
                    }
                    else
                    {
                        (value & 0b10000000) ? PC-= value : PC+=value;
                        break;
                    }
                }
                case INS_BNE_REL:
                {
                    // BNE (Relative)
                    // OpCode: 0xD0
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Byte value = FetchByte(memory, Cycles);
                    PC--;
                    if(Z == 1)
                    {
                        // If Carry Flag is not set Take NO Action
                        break;
                    }
                    else
                    {
                        (value & 0b10000000) ? PC-= value : PC+=value;
                        break;
                    }
                }
                case INS_BPL_REL:
                {
                    // BPL (Relative)
                    // OpCode: 0x10
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Byte value = FetchByte(memory, Cycles);
                    PC--;
                    if(N == 1)
                    {
                        // If Carry Flag is not set Take NO Action
                        break;
                    }
                    else
                    {
                        (value & 0b10000000) ? PC-= value : PC+=value;
                        break;
                    }
                }
                case INS_BRK_IM:
                {
                    break;
                }
                case INS_BVC_REL:
                {
                    // BVC (Relative)
                    // OpCode: 0x50
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Byte value = FetchByte(memory, Cycles);
                    PC--;
                    if(V == 1)
                    {
                        // If Carry Flag is not set Take NO Action
                        break;
                    }
                    else
                    {
                        (value & 0b10000000) ? PC-= value : PC+=value;
                        break;
                    }
                }
                case INS_BVS_REL:
                {
                    // BVS (Relative)
                    // OpCode: 0x70
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Byte value = FetchByte(memory, Cycles);
                    PC--;
                    if(V == 0)
                    {
                        // If Carry Flag is not set Take NO Action
                        break;
                    }
                    else
                    {
                        (value & 0b10000000) ? PC-= value : PC+=value;
                        break;
                    }
                }
                case INS_CLC_IM:
                {
                    // CLC (Relative)
                    // OpCode: 0x18
                    // Number of Bytes: 1
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Cycles--;
                    C = 0;
                    break;
                }
                case INS_CLD_IM:
                {
                    // CLD (Relative)
                    // OpCode: 0xD8
                    // Number of Bytes: 1
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Cycles--;
                    D = 0;
                    break;
                }
                case INS_CLI_IM:
                {
                    // CLI (Relative)
                    // OpCode: 0x58
                    // Number of Bytes: 1
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Cycles--;
                    I = 0;
                    break;
                }
                case INS_CLV_IM:
                {
                    // CLV (Relative)
                    // OpCode: 0xB8
                    // Number of Bytes: 1
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]
                    Cycles--;
                    V = 0;
                    break;
                }
                case INS_CMP_IM:
                {
                    // Compare to Accumulator (Immediate)
                    // OpCode: 0xC9
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]

                    /// Perform Instruction Operation
                    Byte cmpr_value = FetchByte(memory, Cycles);

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A >= cmpr_value ) ? C = 1 : C = 0;
                    (A == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CMP_ZP:
                {
                    // Compare to Accumulator (zero-page)
                    // OpCode: 0xC5
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    Byte cmpr_value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A >= cmpr_value ) ? C = 1 : C = 0;
                    (A == cmpr_value) ? Z = 1 : Z = 0;                                       // Set Accumulator equal to result
                    break;
                }
                case INS_CMP_ZP_X:
                {
                    // Compare to Accumulator (zero-page, X)
                    // OpCode: 0xD5
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]

                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    Byte zp_x_addr = zero_page_addr + X;                        // Offset Zero page address by X reg value
                    Cycles--;                                                   // Operation Takes 1 Cycle????
                    Byte cmpr_value = ReadByte(memory, zp_x_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A >= cmpr_value ) ? C = 1 : C = 0;
                    (A == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CMP_ABS:
                {
                    // Compare to Accumulator - Absolute Addressing
                    // OpCode: 0xCD
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte cmpr_value = memory[abs_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A >= cmpr_value ) ? C = 1 : C = 0;
                    (A == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CMP_ABS_X:
                {
                    // Compare to Accumulator - Abs, X
                    // OpCode: 0xDD
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_x_offset = abs_addr + X;
                    Byte cmpr_value = memory[addr_w_x_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A >= cmpr_value ) ? C = 1 : C = 0;
                    (A == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CMP_ABS_Y:
                {
                    // Compare to Accumulator - Abs, Y
                    // OpCode: 0xD9
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Byte cmpr_value = memory[abs_addr] + Y;
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A >= cmpr_value ) ? C = 1 : C = 0;
                    (A == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CMP_INDR_X:
                {
                    // Compare to Accumulator - (IND, X)
                    // OpCode: 0xC1
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_x = indirect_addr + X;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Byte cmpr_value = ReadByte(memory, offsetAddr_x, Cycles);
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A >= cmpr_value ) ? C = 1 : C = 0;
                    (A == cmpr_value) ? Z = 1 : Z = 0;
                    Cycles--;
                    break;
                }
                case INS_CMP_INDR_Y:
                {
                    // Compare to Accumulator- (IND, Y)
                    // OpCode: 0xD1
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 5*  [1 opcode, 1 Fetch ZP addr, 1 Fetch Y-reg, 1 Fetch (zp_add + Y), 1 Operate?   ]
                    // * Cycle++ if crossing page boundary

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    Word dir_addr = ReadWord(memory, indirect_addr, Cycles);


                    Byte value = ReadByte(memory, dir_addr, Cycles);
                    Word cmpr_value = value + Y;


//                    Cycles--;                                           // Operation Takes 1 Cycle????

                    (A & 0b10000000) ? N = 1 : N = 0;
                    (A >= cmpr_value ) ? C = 1 : C = 0;
                    (A == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CPX_IM:
                {
                    // Compare to X-Register (Immediate)
                    // OpCode: 0xE0
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]

                    /// Perform Instruction Operation
                    Byte cmpr_value = FetchByte(memory, Cycles);

                    (X & 0b10000000) ? N = 1 : N = 0;
                    (X >= cmpr_value ) ? C = 1 : C = 0;
                    (X == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CPX_ZP:
                {
                    // Compare to X-Register (zero-page)
                    // OpCode: 0xE4
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    Byte cmpr_value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)

                    (X & 0b10000000) ? N = 1 : N = 0;
                    (X >= cmpr_value ) ? C = 1 : C = 0;
                    (X == cmpr_value) ? Z = 1 : Z = 0;                                       // Set Accumulator equal to result
                    break;
                }
                case INS_CPX_ABS:
                {
                    // Compare to X-Register - Absolute Addressing
                    // OpCode: 0xEC
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte cmpr_value = memory[abs_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    (X & 0b10000000) ? N = 1 : N = 0;
                    (X >= cmpr_value ) ? C = 1 : C = 0;
                    (X == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CPY_IM:
                {
                    // Compare to Y-Register (Immediate)
                    // OpCode: 0xC0
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]

                    /// Perform Instruction Operation
                    Byte cmpr_value = FetchByte(memory, Cycles);

                    (Y & 0b10000000) ? N = 1 : N = 0;
                    (Y >= cmpr_value ) ? C = 1 : C = 0;
                    (Y == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_CPY_ZP:
                {
                    // Compare to Y-Register (zero-page)
                    // OpCode: 0xC4
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    Byte cmpr_value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)

                    (Y & 0b10000000) ? N = 1 : N = 0;
                    (Y >= cmpr_value ) ? C = 1 : C = 0;
                    (Y == cmpr_value) ? Z = 1 : Z = 0;                                       // Set Accumulator equal to result
                    break;
                }
                case INS_CPY_ABS:
                {
                    // Compare to Y-Register - Absolute Addressing
                    // OpCode: 0xCC
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte cmpr_value = memory[abs_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    (Y & 0b10000000) ? N = 1 : N = 0;
                    (Y >= cmpr_value ) ? C = 1 : C = 0;
                    (Y == cmpr_value) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_DEC_ZP:
                {
                    // Decrement Memory Addr - Zero Page Addressing
                    // OpCode: 0xC6
                    // Number of Bytes: 2 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 5  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle
                    Byte zp_addr_value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Cycles -= 2;

                    Byte dec_val = --zp_addr_value;
                    memory[zero_page_addr] = dec_val;

                    (dec_val < 0 ) ? N = 1 : N = 0;
                    (dec_val == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_DEC_ZP_X:
                {
                    // Decrement Memory Addr - Zero Page, X Addressing
                    // OpCode: 0xD6
                    // Number of Bytes: 2 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle
                    Byte x_incr_zp_addr = zero_page_addr + X;
                    Cycles--;
                    Byte value = ReadByte(memory, x_incr_zp_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Cycles -= 2;

                    Byte dec_val = --value;
                    memory[x_incr_zp_addr] = dec_val;

                    (dec_val < 0 ) ? N = 1 : N = 0;
                    (dec_val == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_DEC_ABS:
                {
                    // Decrement Memory Addr - ABS Addressing
                    // OpCode: 0xCE
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle
                    Byte addr_value = ReadWord(memory, abs_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Cycles -= 2;

                    Byte dec_val = --addr_value;
                    memory[abs_addr] = dec_val;

                    (dec_val < 0 ) ? N = 1 : N = 0;
                    (dec_val == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_DEC_ABS_X:
                {
                    // Decrement Memory Addr - ABS, X Addressing
                    // OpCode: 0xDE
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 7  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle
                    Word x_incr_abs_addr = abs_addr + X;
                    Cycles--;
                    Byte value = ReadWord(memory, x_incr_abs_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Cycles -= 2;

                    Byte dec_val = --value;
                    memory[x_incr_abs_addr] = dec_val;

                    (dec_val < 0 ) ? N = 1 : N = 0;
                    (dec_val == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_DEX_IM:
                {
                    // Decrement X Reg - Immediate
                    // OpCode: 0xCA
                    // Number of Bytes: 1 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 2  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    X -= 1;

                    (X < 0 ) ? N = 1 : N = 0;
                    (X == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_DEY_IM:
                {
                    // Decrement Y Reg - Immediate
                    // OpCode: 0x88
                    // Number of Bytes: 1 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 2  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Y -= 1;

                    (Y < 0 ) ? N = 1 : N = 0;
                    (Y == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_EOR_IM:
                {
                    // EOR - Immediate Addressing
                    // OpCode: 0x49
                    // Number of Bytes: 2 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 2  [1 opcode, Fetch byte literal ]

                    Word abs_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Byte value = memory[abs_addr];
                    Word result = A ^ value;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;

                }
                case INS_EOR_ZP:
                {
                    // EOR (zero-page)
                    // OpCode: 0x45
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Word result = A ^ value;                               // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_EOR_ZP_X:
                {
                    // EOR (zero-page, X)
                    // OpCode: 55
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]

                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    Byte zp_x_addr = zero_page_addr + X;                        // Offset Zero page address by X reg value
                    Cycles--;                                                   // Operation Takes 1 Cycle????
                    Byte value = ReadByte(memory, zp_x_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)
                    Word result = A ^ value;                                // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_EOR_ABS:
                {
                    // EOR - Absolute Addressing
                    // OpCode: 0x4D
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte value = memory[abs_addr];
                    Word result = A ^ value;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_EOR_ABS_X:
                {
                    // EOR  - Abs, X
                    // OpCode: 0x5D
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_x_offset = abs_addr + X;
                    Byte value = memory[addr_w_x_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A ^ value;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_EOR_ABS_Y:
                {
                    // EOR  - Abs, Y
                    // OpCode: 0x59
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_y_offset = abs_addr + Y;
                    Byte value = memory[addr_w_y_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A ^ value;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_EOR_INDR_X:
                {
                    // EOR - (IND, X)
                    // OpCode: 0x41
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_x = indirect_addr + X;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Word value = ReadWord(memory, offsetAddr_x, Cycles);
                    Word result = A ^ value ;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_EOR_INDR_Y:
                {
                    // EOR - (IND, Y)
                    // OpCode: 0x51
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_y = indirect_addr + Y;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Word value = ReadWord(memory, offsetAddr_y, Cycles);
                    Word result = A ^ value ;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_INC_ZP:
                {
                    // Increment Memory Addr - Zero Page Addressing
                    // OpCode: 0xE6
                    // Number of Bytes: 2 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 5  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle
                    Byte zp_addr_value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Cycles -= 2;

                    Byte inc_val = ++zp_addr_value;
                    memory[zero_page_addr] = inc_val;

                    (inc_val < 0 ) ? N = 1 : N = 0;
                    (inc_val == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_INC_ZP_X:
                {
                    // Increment Memory Addr - Zero Page, X Addressing
                    // OpCode: 0xF6
                    // Number of Bytes: 2 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle
                    Byte x_incr_zp_addr = zero_page_addr + X;
                    Cycles--;
                    Byte value = ReadByte(memory, x_incr_zp_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Cycles -= 2;

                    Byte inc_val = ++value;
                    memory[x_incr_zp_addr] = inc_val;

                    (inc_val < 0 ) ? N = 1 : N = 0;
                    (inc_val == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_INC_ABS:
                {
                    // Increment Memory Addr - ABS Addressing
                    // OpCode: 0xEE
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle
                    Byte addr_value = ReadWord(memory, abs_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Cycles -= 2;

                    Byte inc_val = ++addr_value;
                    memory[abs_addr] = inc_val;

                    (inc_val < 0 ) ? N = 1 : N = 0;
                    (inc_val == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_INC_ABS_X:
                {
                    // Increment Memory Addr - ABS, X Addressing
                    // OpCode: 0xFE
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 7  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle
                    Word x_incr_abs_addr = abs_addr + X;
                    Cycles--;
                    Byte value = ReadWord(memory, x_incr_abs_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Cycles -= 2;

                    Byte inc_val = ++value;
                    memory[x_incr_abs_addr] = inc_val;

                    (inc_val < 0 ) ? N = 1 : N = 0;
                    (inc_val == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_INX_IM:
                {
                    // Increment X Reg - Immediate
                    // OpCode: 0xE8
                    // Number of Bytes: 1 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 2  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    X += 1;

                    (X < 0 ) ? N = 1 : N = 0;
                    (X == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_INY_IM:
                {
                    // Increment Y Reg - Immediate
                    // OpCode: 0xC8
                    // Number of Bytes: 1 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 2  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Y += 1;

                    (Y < 0 ) ? N = 1 : N = 0;
                    (Y == 0 ) ? Z = 1 : Z = 0;
                    break;
                }
                case INS_JMP_ABS:
                {
                    // JMP - Absolute Addressing
                    // OpCode: 0x4c
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 3  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    PC = abs_addr;                                        // Set Accumulator equal to result
                    break;
                }
                case INS_JMP_INDR:
                {
                    // JMP - Indirect Addressing
                    // OpCode: 0x6c
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 5  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low...idk]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte low_byte = memory[abs_addr];
                    Byte high_byte = memory[++abs_addr];

                    Word value = (Word)(high_byte << 7) + (Word)low_byte;
                    PC = value;                                        // Set Accumulator equal to result
                    break;
                }
                case INS_JSR_ABS:
                {
                    // JSR - Absolute Addressing
                    // OpCode: 0x20
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low...idk]


                    SP = PC + 2;
                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    PC = abs_addr;
                    break;
                }
                case INS_LDA_IM:
                {
                    /*
                     * LDA - Load Accumulator: Immediate
                     * 1 Cycle - Fetch Opt Code
                     * 1 Cycle - Fetch operand
                     * Assign value to accumulator
                     * Set Processor Flags
                     */
                    Byte value = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    A = value;                                          // Store in the accumulator
                    // Check Processor Status Flag
                    Z = (A == 0);                                       // If value of accumulator is 0 set Z flag "true"
                    N = (A & 0b10000000) > 0;                           // If bit 7 of A is "true" set N true
                    break;
                }
                case INS_LDA_ZP:
                {
                    Byte zero_page_addr = FetchByte(memory, Cycles);    // Fetch Zero_page Opt Code (Increments PC Takes 1 Cycle)
                    A = ReadByte(memory, zero_page_addr, Cycles);       // Load Accumulator with Value from ZeroPage Address

                    // Check Processor Status Flag
                    Z = (A == 0);                                       // If value of accumulator is 0 set Z flag "true"
                    N = (A & 0b10000000) > 0;
                    break;
                }
                case INS_LDA_ZP_X:
                {
                    // LDA (zero-page, X)
                    // OpCode: 0xB5
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]

                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    Byte zp_x_addr = zero_page_addr + X;                        // Offset Zero page address by X reg value
                    Cycles--;

                    Byte result = ReadByte(memory, zp_x_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDA_ABS:
                {
                    // LDA - Absolute Addressing
                    // OpCode: 0xAD
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte result = memory[abs_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDA_ABS_X:
                {
                    // LDA - Abs, X
                    // OpCode: 0xBD
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_x_offset = abs_addr + X;
                    Byte data = memory[addr_w_x_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle???

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    A = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDA_ABS_Y:
                {
                    // LDA - Abs, Y
                    // OpCode: 0xB9
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_y_offset = abs_addr + Y;
                    Byte data = memory[addr_w_y_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    A = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDA_INDR_X:
                {
                    // LDA - (IND, X)
                    // OpCode: 0xA1
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_x = indirect_addr + X;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Byte data = ReadByte(memory, offsetAddr_x, Cycles);
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    A = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDA_INDR_Y:
                {
                    // LDA - (IND, Y)
                    // OpCode: 0xB1
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 5*  [1 opcode, 1 Fetch ZP addr, 1 Fetch Y-reg, 1 Fetch (zp_add + Y), 1 Operate?   ]
                    // * Cycle++ if crossing page boundary

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code

                    Byte dir_addr = ReadByte(memory, indirect_addr, Cycles);
                    dir_addr += Y;
                    Byte data = ReadByte(memory, dir_addr, Cycles);
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    A = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDX_IM:
                {
                    // LDX, Immediate Addressing
                    // OpCode: 0xA2
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]

                    /// Perform Instruction Operation
                    Byte data = FetchByte(memory, Cycles);

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    X = data;
                    break;
                }
                case INS_LDX_ZP:
                {
                    // LDX (zero-page)
                    // OpCode: 0xA5
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte data = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    X = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDX_ZP_Y:
                {
                    // LDX (zero-page, y)
                    // OpCode: 0x75
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]
                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte zp_y_addr = zero_page_addr + ReadByte(memory, Y, Cycles);                        // Offset Zero page address by X reg value
                    Byte data = ReadByte(memory, zp_y_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    X = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDX_ABS:
                {
                    // LDX - Absolute Addressing
                    // OpCode: 0xAE
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte data = memory[abs_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    X = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDX_ABS_Y:
                {
                    // LDX - Absolute Addressing Y
                    // OpCode: 0xAE
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Word abs_y_addr = abs_addr + Y;
                    Byte data = memory[abs_y_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    X = data;
                }
                case INS_LDY_IM:
                {
                    // LDY - Immediate Addressing
                    // OpCode: 0xA2
                    // Number of Bytes: 2 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 2  [1 opcode, 1 Byte Data]

                    Byte data = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    Y = data;
                }
                case INS_LDY_ZP:
                {
                    // LDY (zero-page)
                    // OpCode: 0xA4
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte data = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    Y = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDY_ZP_X:
                {
                    // LDY (zero-page, x)
                    // OpCode: 0xB4
                    // Number of Bytes: 4
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]
                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte zp_x_addr = zero_page_addr + X;                        // Offset Zero page address by X reg value
                    Byte data = ReadByte(memory, zp_x_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    Y = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDY_ABS:
                {
                    // LDY - Absolute Addressing
                    // OpCode: 0xAC
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte data = memory[abs_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    Y = data;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_LDY_ABS_X:
                {
                    // LDY - Absolute Addressing X
                    // OpCode: 0xBC
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Word abs_y_addr = abs_addr + X;
                    Byte data = memory[abs_y_addr];
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);

                    /// Set Registers as Appropriate
                    Y = data;
                }
                case INS_LSR_ACC:
                {
                    // LSR - Accumulator
                    // OpCode: 0x4A
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Byte cur_accum = A;
                    A = cur_accum >> 1;
                    C = (C & 1) ? 1 : 0;
                    Z = (A == 0);                                       // If value of accumulator is 0 set Z flag "true"
                    N = 0;
                    break;
                }
                case INS_LSR_ZP:
                {
                    // LSR - (ZeroPage)
                    // OpCode: 0x46
                    // Number of Bytes: 2 [opcode, zero_page addr]
                    // Number of Cycles: 5  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Byte zp_addr = FetchByte(memory, Cycles);
                    Byte data = ReadByte(memory, zp_addr, Cycles);

                    A = data >> 1;
                    C = (C & 1) ? 1 : 0;
                    Z = (A == 0);                                       // If value of accumulator is 0 set Z flag "true"
                    N = 0;
                    break;
                }
                case INS_LSR_ZP_X:
                {
                    // LSR - (ZeroPage, X Indexed)
                    // OpCode: 0x56
                    // Number of Bytes: 2 [opcode, zero_page addr]
                    // Number of Cycles: 6  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Byte zp_addr = FetchByte(memory, Cycles);

                    Byte zp_x_addr = zp_addr + X;

                    Cycles--;
                    Byte data = ReadByte(memory, zp_x_addr, Cycles);

                    Cycles-=2;

                    A = data >> 1;
                    C = (C & 1) ? 1 : 0;
                    Z = (A == 0);                                       // If value of accumulator is 0 set Z flag "true"
                    N = 0;
                    break;
                }
                case INS_LSR_ABS:
                {
                    // LSR - Absolute Addressing
                    // OpCode: 0x4E
                    // Number of Bytes: 2 [opcode, zero_page addr]
                    // Number of Cycles: 6  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Word abs_addr = FetchWord(memory, Cycles);

                    Cycles--;
                    Byte data = memory[abs_addr];

                    A = data >> 1;
                    C = (C & 1) ? 1 : 0;
                    Z = (A == 0);                                       // If value of accumulator is 0 set Z flag "true"
                    N = 0;
                    break;
                }
                case INS_LSR_ABS_X:
                {
                    // LSR - Absolute Addressing, X
                    // OpCode: 0x5E
                    // Number of Bytes: 3 [opcode, zero_page addr]
                    // Number of Cycles: t  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Word abs_addr = FetchWord(memory, Cycles);

                    Word abs_x_addr = abs_addr + (Word)X;

                    Cycles--;
                    Byte data = memory[abs_x_addr];

                    A = data >> 1;
                    C = (C & 1) ? 1 : 0;
                    Z = (A == 0);                                       // If value of accumulator is 0 set Z flag "true"
                    N = 0;
                    break;
                }
                case INS_NOP_IM:
                {
                    // NOP -
                    // OpCode: 0xEA
                    // Number of Bytes: 0
                    // Number of Cycles: 2
                    Cycles--;
                    Cycles--;
                    break;
                }
                case INS_ORA_IM:
                {
                    // ORA - Immediate Addressing
                    // OpCode: 0x09
                    // Number of Bytes: 2 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 2  [1 opcode, Fetch byte literal ]

                    Word abs_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Byte value = memory[abs_addr];
                    Word result = A | value;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;

                }
                case INS_ORA_ZP:
                {
                    // ORA (zero-page)
                    // OpCode: 0x05
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Word result = A | value;                               // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ORA_ZP_X:
                {
                    // ORA (zero-page, X)
                    // OpCode: 15
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]

                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    Byte zp_x_addr = zero_page_addr + X;                        // Offset Zero page address by X reg value
                    Cycles--;                                                   // Operation Takes 1 Cycle????
                    Byte value = ReadByte(memory, zp_x_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)
                    Word result = A | value;                                // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ORA_ABS:
                {
                    // ORA - Absolute Addressing
                    // OpCode: 0x0D
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte value = memory[abs_addr];
                    Word result = A | value;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ORA_ABS_X:
                {
                    // ORA  - Abs, X
                    // OpCode: 0x1D
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_x_offset = abs_addr + X;
                    Byte value = memory[addr_w_x_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A | value;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ORA_ABS_Y:
                {
                    // ORA  - Abs, Y
                    // OpCode: 0x19
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_y_offset = abs_addr + Y;
                    Byte value = memory[addr_w_y_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A | value;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ORA_INDR_X:
                {
                    // ORA - (IND, X)
                    // OpCode: 0x01
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_x = indirect_addr + X;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Word value = ReadWord(memory, offsetAddr_x, Cycles);
                    Word result = A | value ;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_ORA_INDR_Y:
                {
                    // ORA - (IND, Y)
                    // OpCode: 0x11
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_y = indirect_addr + Y;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Word value = ReadWord(memory, offsetAddr_y, Cycles);
                    Word result = A | value ;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Zero_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_PHA_IM:
                {
                    // PHA -
                    // OpCode: 0x48
                    // Number of Bytes: 1
                    // Number of Cycles: 3

                    memory[SP] = A;
                    SP -= 1;
                    Cycles -= 2;
                    break;
                }
                case INS_PHP_IM:
                {
                    // PHP -
                    // OpCode: 0x08
                    // Number of Bytes: 1
                    // Number of Cycles: 3

                    memory[SP] = P;
                    SP -= 1;
                    Cycles -= 2;
                    break;
                }
                case INS_PLA_IM:
                {
                    // PLA
                    // OpCode: 0x68
                    // Number of Bytes: 1
                    // Number of Cycles: 4

                    A = memory[SP];
                    SP += 1;
                    Cycles -= 3;

                    /// Set Processor Status
                    Set_Negative_Flag(A);
                    Set_Zero_Flag(A);
                    break;
                }
                case INS_PLP_IM:
                {
                    // PLP -
                    // OpCode: 0x28
                    // Number of Bytes: 1
                    // Number of Cycles: 4

                    P = memory[SP];
                    SP += 1;
                    Cycles -= 3;

                    /// Set Processor Status
                   Set_Negative_Flag(P);
                   Set_Overflow_Flag(P);
                   // Set_Break_Flag();
                   Set_Zero_Flag(P);
                   Set_Carry_Flag(P);
                    break;
                }
                case INS_ROL_ACC:
                {
                    // ROL - Accumulator
                    // OpCode: 0x2A
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    C = (A & 0b10000000) ? 1 : 0;
                    A = A << 1;

                    /// Set Processor Status
                    Set_Negative_Flag(A);
                    Set_Zero_Flag(A);
                    break;
                }
                case INS_ROL_ZP:
                {
                    // ROL - ZeroPage
                    // OpCode: 0x26
                    // Number of Bytes: 2 [opcode, Zp-addr]
                    // Number of Cycles: 5  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Byte zp_addr = FetchByte(memory, Cycles);
                    Byte data = ReadByte(memory, zp_addr, Cycles);

                    C = (data & 0b10000000) ? 1 : 0;
                    Byte rol_data = data << 1;

                    memory[zp_addr] = rol_data;

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);
                    break;
                }
                case INS_ROL_ZP_X:
                {
                    // ROL - ZeroPage, X
                    // OpCode: 0x36
                    // Number of Bytes: 2 [opcode, Zp-addr]
                    // Number of Cycles: 6  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Byte zp_addr = FetchByte(memory, Cycles);
                    Byte zp_x_addr = zp_addr + X;
                    Byte data = ReadByte(memory, zp_x_addr, Cycles);

                    C = (data & 0b10000000) ? 1 : 0;
                    Byte rol_data = data << 1;

                    memory[zp_addr] = rol_data;

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);
                    break;
                }
                case INS_ROL_ABS:
                {
                    // ROL - Absolute Addressing
                    // OpCode: 0x2E
                    // Number of Bytes: 3 [opcode, Zp-addr]
                    // Number of Cycles: 6  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Word abs_addr = FetchWord(memory, Cycles);
                    Byte data = ReadWord(memory, abs_addr, Cycles);

                    C = (data & 0b10000000) ? 1 : 0;
                    Byte rol_data = data << 1;

                    memory[abs_addr] = rol_data;

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);
                    break;
                }
                case INS_ROL_ABS_X:
                {
                    // ROL - Absolute Addressing, X
                    // OpCode: 0x3E
                    // Number of Bytes: 3 [opcode, Zp-addr]
                    // Number of Cycles: 7  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Word abs_addr = FetchWord(memory, Cycles);
                    Word abs_x_addr = abs_addr + X;
                    Byte data = ReadByte(memory, abs_x_addr, Cycles);

                    C = (data & 0b10000000) ? 1 : 0;
                    Byte rol_data = data << 1;

                    memory[abs_addr] = rol_data;

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);
                    break;
                }
                case INS_ROR_ACC:
                {
                    // ROR - Accumulator
                    // OpCode: 0x6A
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    C = (A & 1) ? 1 : 0;
                    A = A >> 1;

                    /// Set Processor Status
                    Set_Negative_Flag(A);
                    Set_Zero_Flag(A);
                    break;
                }
                case INS_ROR_ZP:
                {
                    // ROR - ZeroPage
                    // OpCode: 0x66
                    // Number of Bytes: 2 [opcode, Zp-addr]
                    // Number of Cycles: 5  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Byte zp_addr = FetchByte(memory, Cycles);
                    Byte data = ReadByte(memory, zp_addr, Cycles);

                    C = (data & 1) ? 1 : 0;
                    Byte rol_data = data >> 1;

                    memory[zp_addr] = rol_data;

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);
                    break;
                }
                case INS_ROR_ZP_X:
                {
                    // ROR - ZeroPage, X
                    // OpCode: 0x76
                    // Number of Bytes: 2 [opcode, Zp-addr]
                    // Number of Cycles: 6  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Byte zp_addr = FetchByte(memory, Cycles);
                    Byte zp_x_addr = zp_addr + X;
                    Byte data = ReadByte(memory, zp_x_addr, Cycles);

                    C = (data & 1) ? 1 : 0;
                    Byte rol_data = data >> 1;

                    memory[zp_addr] = rol_data;

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);
                    break;
                }
                case INS_ROR_ABS:
                {
                    // ROR - Absolute Addressing
                    // OpCode: 0x6E
                    // Number of Bytes: 3 [opcode, Zp-addr]
                    // Number of Cycles: 6  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Word abs_addr = FetchWord(memory, Cycles);
                    Byte data = ReadWord(memory, abs_addr, Cycles);

                    C = (data & 1) ? 1 : 0;
                    Byte rol_data = data >> 1;

                    memory[abs_addr] = rol_data;

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);
                    break;
                }
                case INS_ROR_ABS_X:
                {
                    // ROR - Absolute Addressing, X
                    // OpCode: 0x7E
                    // Number of Bytes: 3 [opcode, Zp-addr]
                    // Number of Cycles: 7  [1 opcode, 1 Shift & Redeposit in Accumulator ]

                    Word abs_addr = FetchWord(memory, Cycles);
                    Word abs_x_addr = abs_addr + X;
                    Byte data = ReadByte(memory, abs_x_addr, Cycles);

                    C = (data & 1) ? 1 : 0;
                    Byte rol_data = data >> 1;

                    memory[abs_addr] = rol_data;

                    /// Set Processor Status
                    Set_Negative_Flag(data);
                    Set_Zero_Flag(data);
                    break;
                }
                case INS_RTI_IM:{break;}
                case INS_RTS_IM:{break;}
                case INS_SBC_IM:
                {
                    // Subtract with Carry
                    // OpCode: 0xE9
                    // Number of Bytes: 2
                    // Number of Cycles: 2  [1 opcode, 1 Fetch byte literal]

                    /// Perform Instruction Operation
                    Byte value = FetchByte(memory, Cycles);
                    Word result  = A - value - C;

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;
                    break;
                }
                case INS_SBC_ZP:
                {
                    // Subtract with Carry (zero-page)
                    // OpCode: 0xE5
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte value = ReadByte(memory, zero_page_addr, Cycles);      // Read value in ZP addr (1 Cycle)
                    Word result = A - value - C;                               // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag( result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_SBC_ZP_X:
                {
                    // Subtract with Carry (zero-page, X)
                    // OpCode: 0xF5
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Access X-reg,  1 Fetch value @ X-incremented addr]

                    Byte zero_page_addr = FetchByte(memory, Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    Byte zp_x_addr = zero_page_addr + X;                        // Offset Zero page address by X reg value
                    Cycles--;                                                   // Operation Takes 1 Cycle????
                    Byte value = ReadByte(memory, zp_x_addr, Cycles);           // Read value in ZP (offset by X) addr (1 Cycle)
                    Word result = A - value - C;                                // Add Accumulator + Value of Mem addr + Carry bit

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_SBC_ABS:
                {
                    // Subtract with Carry - Absolute Addressing
                    // OpCode: 0xED
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]

                    Word abs_addr = FetchWord(memory, Cycles);             // Fetch data after Opt Code
                    Byte value = memory[abs_addr];
                    Word result = A - value - C;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_SBC_ABS_X:
                {
                    // Subtract with Carry - Abs, X
                    // OpCode: 0xFD
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_x_offset = abs_addr + X;
                    Byte value = memory[addr_w_x_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A - value - C;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_SBC_ABS_Y:
                {
                    // Subtract with Carry - Abs, Y
                    // OpCode: 0xF9
                    // Number of Bytes: 3 [opcode, PC_high_byte, PC_low_Byte]
                    // Number of Cycles: 4*  [1 opcode, 1 Fetch PC_High, 1 Fetch PC_Low, 1 Decode PC addr?]
                    // * Extra Cycle if crossing Page boundary

                    Word abs_addr = FetchWord(memory, Cycles);          // Fetch data after Opt Code
                    Word addr_w_y_offset = abs_addr + Y;
                    Byte value = memory[addr_w_y_offset];
                    Cycles--;                                           // Operation Takes 1 Cycle????
                    Word result = A - value - C;                        // Add Accumulator + Value of Mem addr + Carry bit

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_SBC_INDR_X:
                {
                    // Subtract with Carry - (IND, X)
                    // OpCode: 0xE1
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 6  [1 opcode, 1 Fetch ZP addr, 1 Fetch X-reg, 1 Fetch (zp_add + X), 1 Operate?   ]

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code
                    Word offsetAddr_x = indirect_addr + X;                     // Increment Operand Address by X-reg
                    Cycles--;                                                     // X-reg access takes a cycle

                    Byte value = ReadByte(memory, offsetAddr_x, Cycles);
                    Word result = A - value - C;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_SBC_INDR_Y:
                {
                    // Subtract with Carry - (IND, Y)
                    // OpCode: 0xF1
                    // Number of Bytes: 2 [opcode, Zero_page_addr ]
                    // Number of Cycles: 5*  [1 opcode, 1 Fetch ZP addr, 1 Fetch Y-reg, 1 Fetch (zp_add + Y), 1 Operate?   ]
                    // * Cycle++ if crossing page boundary

                    Byte indirect_addr = FetchByte(memory, Cycles);             // Fetch data after Opt Code

                    /// CHECK IF ADDRESS CROSSES PAGE BOUNDARY
                    // Cycles-- if true

                    Word dir_addr = ReadWord(memory, indirect_addr, Cycles);

                    Word offset_addr = dir_addr + Y;

                    Byte value = ReadByte(memory, offset_addr, Cycles);

                    Word result = A - value - C;                        // Add Accumulator + Value of Mem addr + Carry bit
                    Cycles--;                                           // Operation Takes 1 Cycle????

                    /// Set Processor Status
                    Set_Negative_Flag(result);
                    Set_Overflow_Flag(result);
                    Set_Zero_Flag(result);
                    Set_Carry_Flag(result);

                    /// Set Registers as Appropriate
                    A = result;                                         // Set Accumulator equal to result
                    break;
                }
                case INS_SEC_IM: {break;}
                case INS_SED_IM: {break;}
                case INS_SEI_IM: {break;}
                case INS_STA_ZP:
                {
                    // STA (zero-page)
                    // OpCode: 0x85
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    memory[zero_page_addr] = A;
                    break;
                }
                case INS_STA_ZP_X:
                {
                    // STA (zero-page, X)
                    // OpCode: 0x95
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte zp_x_addr = zero_page_addr + X;

                    memory[zp_x_addr] = A;
                    break;
                }
                case INS_STA_ABS:
                {
                    // STA Absolute Addressing
                    // OpCode: 0x8D
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    memory[abs_addr] = A;
                    break;
                }
                case INS_STA_ABS_X:
                {
                    // STA Absolute Addressing, X
                    // OpCode: 0x9D
                    // Number of Bytes: 2
                    // Number of Cycles: 5  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]
                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    Word abs_x_addr = abs_addr + (Word)X;
                    memory[abs_x_addr] = A;
                    break;
                }
                case INS_STA_ABS_Y:
                {
                    // STA Absolute Addressing, Y
                    // OpCode: 0x99
                    // Number of Bytes: 2
                    // Number of Cycles: 5  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]
                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    Word abs_y_addr = abs_addr + (Word)Y;
                    memory[abs_y_addr] = A;
                    break;
                }
                case INS_STA_INDR_X:
                {
                    // STA Indirect Addressing, X
                    // OpCode: 0x81
                    // Number of Bytes: 2
                    // Number of Cycles: 5  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]
                    Byte addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    Word abs_x_addr = (Word)addr + (Word)X;
                    memory[abs_x_addr] = A;
                    break;
                }
                case INS_STA_INDR_Y:
                {
                    // STA Indirect Addressing, Y
                    // OpCode: 0x91
                    // Number of Bytes: 2
                    // Number of Cycles: 6  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]
                    Byte addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    Word abs_y_addr = (Word)addr + (Word)Y;
                    memory[abs_y_addr] = A;
                    break;
                }
                case INS_STX_ZP:
                {
                    // STX (zero-page)
                    // OpCode: 0x86
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    memory[zero_page_addr] = X;
                    break;
                }
                case INS_STX_ZP_Y:
                {
                    // STX (zero-page, Y)
                    // OpCode: 0x95
                    // Number of Bytes: 2
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte zp_y_addr = zero_page_addr + Y;

                    memory[zp_y_addr] = X;
                    break;
                }
                case INS_STX_ABS:
                {
                    // STX Absolute Addressing
                    // OpCode: 0x8E
                    // Number of Bytes: 3
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    memory[abs_addr] = X;
                    break;
                }
                case INS_STY_ZP:
                {
                    // STY (zero-page)
                    // OpCode: 0x84
                    // Number of Bytes: 2
                    // Number of Cycles: 3  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)

                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);
                    memory[zero_page_addr] = Y;
                    break;
                }
                case INS_STY_ZP_X:
                {
                    // STY (zero-page, X)
                    // OpCode: 0x94
                    // Number of Bytes: 4
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Byte zero_page_addr = FetchByte(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    ASSERT_GE(zero_page_addr, 0);
                    ASSERT_LT(zero_page_addr, 256);

                    Byte zp_x_addr = zero_page_addr + X;

                    memory[zp_x_addr] = Y;
                    break;
                }
                case INS_STY_ABS:
                {
                    // STY Absolute Addressing
                    // OpCode: 0x8C
                    // Number of Bytes: 3
                    // Number of Cycles: 4  [1 opcode, 1 Fetch Zero-page addr, 1 Fetch value @ addr]

                    Word abs_addr = FetchWord(memory,Cycles);                // Get Zero Page Address (Increments PC, Take 1 Cycle)
                    memory[abs_addr] = Y;
                    break;
                }
                case INS_TAX_IM:
                {
                    // TAX -Implied
                    // OpCode: 0xAA
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 idk]
                    X = A;

                    /// Set Processor Status
                    Set_Negative_Flag(X);
                    Set_Zero_Flag(X);

                    break;
                }
                case INS_TAY_IM:
                {
                    // TAY -Implied
                    // OpCode: 0xA8
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 idk]

                    Y = A;

                    /// Set Processor Status
                    Set_Negative_Flag(Y);
                    Set_Zero_Flag(Y);

                    break;
                }
                case INS_TSX_IM:
                {
                    // TSX -Implied
                    // OpCode: 0xBA
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 idk]

                    X = SP;

                    /// Set Processor Status
                    Set_Negative_Flag(X);
                    Set_Zero_Flag(X);

                    break;
                }
                case INS_TXA_IM:
                {
                    // TXA -Implied
                    // OpCode: 0x8A
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 idk]

                    A = X;

                    /// Set Processor Status
                    Set_Negative_Flag(A);
                    Set_Zero_Flag(A);

                    break;
                }
                case INS_TXS_IM:
                {
                    // TXS -Implied
                    // OpCode: 0x9A
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 idk]

                    SP = X;

                    /// Set Processor Status
                    Set_Negative_Flag(SP);
                    Set_Zero_Flag(SP);

                    break;
                }
                case INS_TYA_IM :
                {
                    // TYA -Implied
                    // OpCode: 0x98
                    // Number of Bytes: 1 [opcode]
                    // Number of Cycles: 2  [1 opcode, 1 idk]
                    A = Y;

                    /// Set Processor Status
                    Set_Negative_Flag(A);
                    Set_Zero_Flag(A);

                    break;
                }

                default:
                    std::cout << "Instruction NOT Handled!" << std::endl;
                    break;
            }
        }
    }


};

}
/*
----------------------------------------------------------------------------------------
|                              6502 Native Data Types                                  |
----------------------------------------------------------------------------------------
The 6502 processor is build with the 16bit address & 6bit data bus architechture. This 
means that the 6502 architecture "naturally" defines the type/size of data that it works
with. 
_______________________________________________________________________________________

This file aliases for the  Native Types of the 6502 for ease of specifyin data types
----------------------------------------------------------------------------------------
*/ 

namespace MOS_6502
{
    using Byte = unsigned char;     // Create 8-bit Alias
    using Word = unsigned short;    // Create 16-bit Alias (Disclaimer: I know the 6502 word length is technically 8-bits. I made a mistake and am too lazy to refactor. Deal with it!)
    using u32 = unsigned int;       // Create 32-bit Alias
}
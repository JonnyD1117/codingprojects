#include "native_types.hpp"

#pragma once 

/*
----------------------------------------------------------------------------------------
|                              6502 Instruction Set                                    |
----------------------------------------------------------------------------------------
The 6502 processor internally defines a set of Instructions that it understands how to 
decode and execute. 
________________________________________________________________________________________

While the values below are written in Hex for human-readability, the processor would in
theory ...

1) Fetch value contained in the "program counter" (PC) register
2) Decode the instruction and any required operands
3) Execute the instruction 
4) Update the PC register for the next instruction
----------------------------------------------------------------------------------------
*/ 

namespace MOS_6502
{

struct Instruction_Set
{
    //ADC - Add with Carry
    static constexpr Byte INS_ADC_IM = 0x69;
    static constexpr Byte INS_ADC_ZP = 0x65;
    static constexpr Byte INS_ADC_ZP_X = 0x75;
    static constexpr Byte INS_ADC_ABS = 0x6D;
    static constexpr Byte INS_ADC_ABS_X = 0x7D;
    static constexpr Byte INS_ADC_ABS_Y = 0x79;
    static constexpr Byte INS_ADC_INDR_X = 0x61;
    static constexpr Byte INS_ADC_INDR_Y = 0x71;

    //AND - Logical AND
    static constexpr Byte INS_AND_ACCUM = 0x29;
    static constexpr Byte INS_AND_ZP = 0x25;
    static constexpr Byte INS_AND_ZP_X = 0x35;
    static constexpr Byte INS_AND_ABS = 0x2D;
    static constexpr Byte INS_AND_ABS_X = 0x3D;
    static constexpr Byte INS_AND_ABS_Y = 0x39;
    static constexpr Byte INS_AND_INDR_X = 0x21;
    static constexpr Byte INS_AND_INDR_Y = 0x31;

    //ASL - Arithmetic Shift Left
    static constexpr Byte INS_ASL_ACC = 0x0A;
    static constexpr Byte INS_ASL_ZP = 0x06;
    static constexpr Byte INS_ASL_ZP_X = 0x16;
    static constexpr Byte INS_ASL_ABS = 0x0E;
    static constexpr Byte INS_ASL_ABS_X = 0x1E;

    //BCC - Branch if Carry Clear
    static constexpr Byte INS_BCC_REL = 0x90;

    //BCS - Branch if Carry Set
    static constexpr Byte INS_BCS_REL = 0xB0;

    //BEQ - Branch if Equal
    static constexpr Byte INS_BEQ_REL = 0xF0;

    //BIT - Bit Test
    static constexpr Byte INS_BIT_ZP = 0x24;
    static constexpr Byte INS_BIT_ABS = 0x2C;

    //BMI - Branch if Minus
    static constexpr Byte INS_BMI_REL = 0x30;

    //BNE Branch if Not Equal
    static constexpr Byte INS_BNE_REL = 0xD0;

    //BPL - Branch if Positive
    static constexpr Byte INS_BPL_REL = 0x10;

    //BRK - Force Interrupt
    static constexpr Byte INS_BRK_IM = 0x00;

    //BVC - Branch if Overflow CLear
    static constexpr Byte INS_BVC_REL = 0x50;

    //BVS - Branch if Overflow Set
    static constexpr Byte INS_BVS_REL = 0x70;

    //CLC - Clear Carry Flag
    static constexpr Byte INS_CLC_IM = 0x18;

    //CLD - Clear Decimal Mode
    static constexpr Byte INS_CLD_IM = 0xD8;

    //CLI - Clear Interrupt Disable
    static constexpr Byte INS_CLI_IM = 0x58;

    //CLV - Clear Overflow Flag
    static constexpr Byte INS_CLV_IM = 0xB8;

    //CMP - Compare
    static constexpr Byte INS_CMP_IM = 0xC9;
    static constexpr Byte INS_CMP_ZP = 0xC5;
    static constexpr Byte INS_CMP_ZP_X = 0xD5;
    static constexpr Byte INS_CMP_ABS = 0xCD;
    static constexpr Byte INS_CMP_ABS_X = 0xDD;
    static constexpr Byte INS_CMP_ABS_Y = 0xD9;
    static constexpr Byte INS_CMP_INDR_X = 0xC1;
    static constexpr Byte INS_CMP_INDR_Y = 0xD1;

    //CPX - Compare X Register
    static constexpr Byte INS_CPX_IM = 0xE0;
    static constexpr Byte INS_CPX_ZP = 0xE4;
    static constexpr Byte INS_CPX_ABS = 0xEC;

    //CPY - Compare Y Register
    static constexpr Byte INS_CPY_IM = 0xC0;
    static constexpr Byte INS_CPY_ZP = 0xC4;
    static constexpr Byte INS_CPY_ABS = 0xCC;

    //DEC - Decrement Memory
    static constexpr Byte INS_DEC_ZP = 0xC6;
    static constexpr Byte INS_DEC_ZP_X = 0xD6;
    static constexpr Byte INS_DEC_ABS = 0xCE;
    static constexpr Byte INS_DEC_ABS_X = 0xDE;

    //DEX - Decrement X Register
    static constexpr Byte INS_DEX_IM = 0xCA;

    //DEY - Decrement Y Register
    static constexpr Byte INS_DEY_IM = 0x88;

    //EOR - Exclusive OR
    static constexpr Byte INS_EOR_IM = 0x49;
    static constexpr Byte INS_EOR_ZP = 0x45;
    static constexpr Byte INS_EOR_ZP_X = 0x55;
    static constexpr Byte INS_EOR_ABS = 0x4D;
    static constexpr Byte INS_EOR_ABS_X = 0x5D;
    static constexpr Byte INS_EOR_ABS_Y = 0x59;
    static constexpr Byte INS_EOR_INDR_X = 0x41;
    static constexpr Byte INS_EOR_INDR_Y = 0x51;

    //INC - Increment Memory
    static constexpr Byte INS_INC_ZP = 0xE6;
    static constexpr Byte INS_INC_ZP_X = 0xF6;
    static constexpr Byte INS_INC_ABS = 0xEE;
    static constexpr Byte INS_INC_ABS_X = 0xFE;

    //INX - Increment X Register
    static constexpr Byte INS_INX_IM = 0xE8;

    //INY - Increment Y Register
    static constexpr Byte INS_INY_IM = 0xC8;

    //JMP - Jump
    static constexpr Byte INS_JMP_ABS = 0x4C;
    static constexpr Byte INS_JMP_INDR = 0x6C;

    //JSR - Jump to Subroutine
    static constexpr Byte INS_JSR_ABS = 0x20;

    // LDA - Load Accumulator
    static constexpr Byte INS_LDA_IM = 0xA9;
    static constexpr Byte INS_LDA_ZP = 0xA5;
    static constexpr Byte INS_LDA_ZP_X = 0xB5;
    static constexpr Byte INS_LDA_ABS = 0xAD;
    static constexpr Byte INS_LDA_ABS_X = 0xBD;
    static constexpr Byte INS_LDA_ABS_Y = 0xB9;
    static constexpr Byte INS_LDA_INDR_X = 0xA1;
    static constexpr Byte INS_LDA_INDR_Y = 0xB1 ;

    // LDX - Load X Register
    static constexpr Byte INS_LDX_IM = 0xA2;
    static constexpr Byte INS_LDX_ZP = 0xA6;
    static constexpr Byte INS_LDX_ZP_Y = 0xB6;
    static constexpr Byte INS_LDX_ABS = 0xAE;
    static constexpr Byte INS_LDX_ABS_Y = 0xBE;

    // LDY - Load Y Register
    static constexpr Byte INS_LDY_IM = 0xA0;
    static constexpr Byte INS_LDY_ZP = 0xA4;
    static constexpr Byte INS_LDY_ZP_X = 0xB4;
    static constexpr Byte INS_LDY_ABS = 0xAC;
    static constexpr Byte INS_LDY_ABS_X = 0xBC;

    // LSR - Logical Shift Right
    static constexpr Byte INS_LSR_ACC = 0x4A;
    static constexpr Byte INS_LSR_ZP = 0x46;
    static constexpr Byte INS_LSR_ZP_X = 0x56;
    static constexpr Byte INS_LSR_ABS = 0x4E;
    static constexpr Byte INS_LSR_ABS_X = 0x5E;

    // NOP - No Operation
    static constexpr Byte INS_NOP_IM =0xEA;

    // ORA - Logical Inclusive OR
    static constexpr Byte INS_ORA_IM =0x09;
    static constexpr Byte INS_ORA_ZP =0x05;
    static constexpr Byte INS_ORA_ZP_X =0x15;
    static constexpr Byte INS_ORA_ABS =0x0D;
    static constexpr Byte INS_ORA_ABS_X =0x1D;
    static constexpr Byte INS_ORA_ABS_Y =0x19;
    static constexpr Byte INS_ORA_INDR_X =0x01;
    static constexpr Byte INS_ORA_INDR_Y =0x11;

    //PHA - Push Accumulator
    static constexpr Byte INS_PHA_IM =0x48;

    //PHP - Push Processor Status
    static constexpr Byte INS_PHP_IM =0x08;

    // PLA - Pull Accumulator
    static constexpr Byte INS_PLA_IM =0x68;

    //PLP - Pull Processor Status
    static constexpr Byte INS_PLP_IM =0x28;

    //ROL - Rotate Left
    static constexpr Byte INS_ROL_ACC =0x2A;
    static constexpr Byte INS_ROL_ZP =0x26;
    static constexpr Byte INS_ROL_ZP_X =0x36;
    static constexpr Byte INS_ROL_ABS =0x2E;
    static constexpr Byte INS_ROL_ABS_X =0x3E;

    //ROR - Rotate Right
    static constexpr Byte INS_ROR_ACC =0x6A;
    static constexpr Byte INS_ROR_ZP =0x66;
    static constexpr Byte INS_ROR_ZP_X =0x76;
    static constexpr Byte INS_ROR_ABS =0x6E;
    static constexpr Byte INS_ROR_ABS_X =0x7E;

    //RTI Return from Interrup
    static constexpr Byte INS_RTI_IM =0x40;

    //RTS - Return from Subroutine
    static constexpr Byte INS_RTS_IM =0x60;

    //SBC Subtract with Carry
    static constexpr Byte INS_SBC_IM =0xE9;
    static constexpr Byte INS_SBC_ZP =0xE5;
    static constexpr Byte INS_SBC_ZP_X =0xF5;
    static constexpr Byte INS_SBC_ABS =0xED;
    static constexpr Byte INS_SBC_ABS_X =0xFD;
    static constexpr Byte INS_SBC_ABS_Y =0xF9;
    static constexpr Byte INS_SBC_INDR_X =0xE1;
    static constexpr Byte INS_SBC_INDR_Y =0xF1;

    //SEC - Set Carry Flag
    static constexpr Byte INS_SEC_IM =0x38;

    // SED - Set Decimal Flag
    static constexpr Byte INS_SED_IM =0xF8;

    // SEI - Set Interrupt Disable
    static constexpr Byte INS_SEI_IM =0x78;

    // STA - Store Accumulator
    static constexpr Byte INS_STA_ZP =0x85;
    static constexpr Byte INS_STA_ZP_X =0x95;
    static constexpr Byte INS_STA_ABS =0x8D;
    static constexpr Byte INS_STA_ABS_X =0x9D;
    static constexpr Byte INS_STA_ABS_Y =0x99;
    static constexpr Byte INS_STA_INDR_X =0x81;
    static constexpr Byte INS_STA_INDR_Y =0x91;

    // STX - Store X Register
    static constexpr Byte INS_STX_ZP =0x86;
    static constexpr Byte INS_STX_ZP_Y =0x96;
    static constexpr Byte INS_STX_ABS =0x8E;

    // STY - Store Y Register
    static constexpr Byte INS_STY_ZP =0x84;
    static constexpr Byte INS_STY_ZP_X =0x94;
    static constexpr Byte INS_STY_ABS =0x8C;

    //TAX - Transfer Accumulator to X
    static constexpr Byte INS_TAX_IM =0xAA;

    //TAY - Transfer Accumulator to Y
    static constexpr Byte INS_TAY_IM =0xA8;

    //TSX - Transfer Stack Pointer to X
    static constexpr Byte INS_TSX_IM =0xBA;

    //TXA - Transfer X to Accumulator
    static constexpr Byte INS_TXA_IM =0x8A;

    //TXS - Transfer X to Stack Pointer
    static constexpr Byte INS_TXS_IM =0x9A;

    //TYA - Transfer Y to Accumulator
    static constexpr Byte INS_TYA_IM =0x98;

};

}
#include "6502_headers.hpp"


namespace MOS_6502
{


struct Specification
{
    int frequency = 1790000;        // Atari 800/xl 1.79MHz 
    int freq_scaler = 1;   
    int data_bus_width = 8;         // Atari 800/xl 8-bit wide data bus
    int address_bus_wdith = 16;     //  Atari 800/xl 16-bit wide address bus
};


class Emulator
{
private:

Specification spec;             // System Specs to be used by the emulator

CPU cpu;                        // CPU Implementation to be used
Memory memory;                  // Memory Implementation to be used 
Stack stack;                    // Stack Implementation to be used


public:
    Emulator();
    ~Emulator();

    void Initialize();              // Initialize the Emulator (e.g. Setup the CPU, memory...etc)

    void LoadCartridgeROM();        // Load Cartridge ROM into system memory? (Memory Mapped IO?)

    void RunCartridge();            // Run the program on the cartridge
};

}
#include <iostream> 
#include "emulator.hpp"

void print_args(int argc, char*argv[])
{
    for (int i = 0; i < argc; i++) { std::cout << argv[i] << std::endl; }
}


int main(int argc, char *argv[])
{
    std::cout << "Welcome to the 6502 C++ Emulator" << std::endl;
    print_args(argc, argv);

    // Theorizing how the emulator would encapsulate the 6502 system (e.g probably Atari 800/800xl)
    // Emulator emulator();
    // emulator.Initialize(); 
    // emulator.LoadCartridgeROM(); 
    // emulator.RunCartridge();

    return 0; 
}
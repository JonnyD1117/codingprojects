# Atari 800 Emulator (using MOS-6502 Processor) 

The objective of this project is to implement an emulator for the Atari 800 (written in C++) that can execute roms & iso's from the original machine as well as play some of the games. 


## Emulation Requirements

In order to fully emulate the 6502-based Atari 800, one must also emulate other Integrated Circuits (ICs) that enabled the Atari 800 to have sound, graphics, and I/O....etc. 

### 6502 CPU & Random Access Memory (RAM)
The 6502 is the main IC in the Atari 800 responsible for all the computation. All of the other chips and peripherals in the Atari 800 only exist to compliment and extent the ability of the 6502 to perform certain tasks. 

NOTE: RAM is so closely tied to the operation of the CPU that it does not make sense to seperate the two. Therefore the datastructures for defining and using RAM are also defined within the library that defines the 6502 Processor. 

### OS-Read Only Memory (ROM)
The Atari 800 operating system is a very thin layer of code, and libraries (primarily written directly in 6502 Assembly) that provide the ability for a user to interact with the machine. However, the contents of system RAM are volatile and will not perist after being powered off. This means that the operating system needs some peristant storage for defining the elements of the OS that dont change... e.g. boot-utilites, I/O libraries, sys-calls...etc everything required for the operating system to run and provide a useful abstraction to the user.

Read-Only-Memory (ROM) is an IC that contains all of this information and is persistent even when the system is powered off. Once the system is powered on, the ROM chip will resides directly the 6502 processors address bus and listens for addresses within a specific memory range (0xD800 - 0xFFFF). 

This allowed the operating system to have immediate access to utilities, libraries, ..etc once booted. 

1. OS Data Base [ 0x0200 - 0x047F ]
2. User Workspace [ 0x0480 - 0x06FF ]
3.

#### Floating Point Arithmetic Package (FP)

#### Central I/O Utility (CIO) 
The Atari 800 OS provides a collection of routines that allow you to access peripheral and local devices at three difference levels. 

1. Device independent access to devices 
2. Communication via device handlers
3. Serial I/O bus Utility (SIO) 


### Cartridge Slots (A & B)



Cartridge A (Left slot)  [ 0xA000 - 0xBFFF ]
Cartridge B (Right slot) [ 0x8000 - 0x9FFF ]
### Memory Mapped I/O
In the Atari 800 the 6502 processor is capable of interacting with IO devices by mapping certain sections of memory to IO devices that sit on the address bus and listen for addresses in their specified range to be requested. Behind the scene the 6502 interacts with the IO chips as if it was a RAM chip; however, in reality, instead of reading from RAM the IO device can be used as a peripheral and provide the computer with extended functionality (e.g. to read and write to a floppy disk, sound card, graphics...etc).

#### CTIA - (Color Television Interface Adapter) 
[0xD000 - D0x01F]

Responsible for generating the video signals that will be decoded by the TV or monitor 

Effectively, a video controller chip. 

#### POKEY - (Pot Keyboard INtegrated Circuit)
[0xD200 - D0x21F]

This chip serves multiple roles for ... 

1. I/O
2. Keyboard input Handling (interpts?)
3. Sound Generation 
4. Serial Communcation



#### PIA
[0xD300 - D0x31F]
#### ANTIC
[0xD400 - D0x41F]